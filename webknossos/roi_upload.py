from pathlib import Path
import yaml
from argparse import ArgumentParser
from datetime import datetime

import zetastitcher as zs
from read_roi import read_roi_file
import tifffile
import webknossos as wk
import numpy as np

from integer_shearing_correct import IntegerShearingCorrect

# setup logging
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class ROISelectUpload:
    def __init__(self,
                 config_yml_path: Path = None,
                 ):

        self.config_yml_path = config_yml_path
        self.config = self.read_cfg(self.config_yml_path)

        self.roi_file_path = Path(self.config["roi_file_path"])
        self.stitch_file_path = Path(self.config["stitch_file_path"])
        self.out_path = Path(self.config["out_path"])
        self.physical_scale = self.config["physical_scale"]
        self.webknossos_token = self.config["webknossos_token"]
        
        self.save_corrected_tiff = self.config["save_corrected_tiff"]
        self.upload_dataset_flag = self.config["upload_dataset"]
        self.preview_inpf= self.config["preview_inpf"]

        self.shearing_delta = int(self.config["shearing_delta"])
        

        if not self.out_path.exists():
            self.out_path.mkdir(parents=True)

        self.x_min, self.x_max, self.y_min, self.y_max = self._read_roi(self.roi_file_path)

        self.vfv = zs.VirtualFusedVolume(str(self.stitch_file_path))
        self.peek = self.vfv.peek[int(self.vfv.shape[0]//2), self.y_min:self.y_max, self.x_min:self.x_max]

        self.tiff_fpath = self.stitch_file_path.parent.joinpath(Path(self._get_largest_slice_count(self.peek)).name)
        path_stem = self.tiff_fpath.stem.replace(".ome", "")
        self.zip_fpath = self.tiff_fpath.parent.parent.joinpath("zip_left/" + path_stem + '.zip')

        self.inpf = zs.InputFile(self.zip_fpath)

        inpf_shape = self.inpf.shape
        z_shape = inpf_shape[0]
        self.inpf_z_start = int((self.x_min / self.vfv.shape[2]) * z_shape)
        self.inpf_z_stop = int((self.x_max / self.vfv.shape[2]) * z_shape)
        
        self.dataset_name = self.get_dataset_name()
        
        if self.preview_inpf:
            preview_out_fpath = self.out_path.joinpath(self.dataset_name+"inpf_preview.tif")
            tifffile.imwrite(str(preview_out_fpath), self.inpf[self.inpf_z_start])

        self.isc = IntegerShearingCorrect(
            input_volume=self.inpf,
            delta=self.shearing_delta,
            start=self.inpf_z_start,
            end=self.inpf_z_stop)

        self.corrected_volume = self.isc.image_data_corrected
        self.corrected_volume_tiff_path = self.out_path.joinpath(path_stem + "_corrected.ome.tif")
        logger.info(f"Save corrected volume to {self.corrected_volume_tiff_path}")
        tifffile.imwrite(str(self.corrected_volume_tiff_path), self.corrected_volume)

        

        self.dataset_path = self.out_path.joinpath(self.dataset_name)
        self.dataset_path.mkdir(parents=True, exist_ok=True)

        self.dataset = self.get_dataset()

        if self.upload_dataset_flag:
            self.upload_dataset()

    @staticmethod
    def read_cfg(cfg_yml_path: Path):
        with cfg_yml_path.open(mode="r") as f:
            cfg = yaml.load(f, Loader=yaml.FullLoader)
        return cfg

    @staticmethod
    def _read_roi(roi_path: Path) -> tuple:
        # Load ROI
        roi = read_roi_file(str(roi_path))

        # extract first element from roi
        roi_ids = list(roi.keys())
        roi_dict = roi[roi_ids[0]]

        x_min = roi_dict['left']
        y_min = roi_dict['top']
        x_max = x_min + roi_dict['width']
        y_max = y_min + roi_dict['height']

        return x_min, x_max, y_min, y_max

    @staticmethod
    def _get_slice_pixels(slice_list: list) -> int:
        """
        Returns the number of pixels in a list of slices.
        """
        pixels = 1

        for slice_id in slice_list:
            slice_width = slice_id.stop - slice_id.start
            pixels *= slice_width

        return pixels

    @classmethod
    def _get_largest_slice_count(cls, peek: tuple) -> str:
        fnames = []
        pxs = []
        for idx, file_slices in enumerate(peek):
            fpath = file_slices[0]
            slicelist = file_slices[1]
            px = cls._get_slice_pixels(slicelist)
            fnames.append(fpath)
            pxs.append(px)

        max_idx = pxs.index(max(pxs))
        return fnames[max_idx]

    def get_volume(self):
        inpf_shape = self.inpf.shape
        z_shape = inpf_shape[0]
        self.inpf_z_start = int((self.x_min / self.vfv.shape[2]) * z_shape)
        self.inpf_z_stop = int((self.x_max / self.vfv.shape[2]) * z_shape)

        return self.inpf[self.inpf_z_start:self.inpf_z_stop]

    def get_dataset_name(self):
        timestamp = str(int(round(datetime.timestamp(datetime.now()))))
        dataset_name = str(self.tiff_fpath).split("/")[4]
        assert "LeftDet" in dataset_name, "Dataset name must contain 'LeftDet'!"
        dataset_name = dataset_name + "_" + timestamp
        return dataset_name

    def get_dataset(self):
        logger.info(f"Creating dataset: {self.dataset_name}")

        scale = map(float, self.physical_scale)
        scale = tuple(scale)
        scale = (scale[0], scale[1], scale[2])

        dataset = wk.Dataset.create(dataset_path=str(self.dataset_path),
                                    scale=scale,
                                    name=self.dataset_name)

        data = np.swapaxes(self.corrected_volume, 0, 2)
        layer = dataset.add_layer(
            layer_name="color",
            category="color",
            dtype_per_layer=self.corrected_volume.dtype
        )

        mag1 = layer.add_mag(1)
        mag1.write(
            data=data
        )
        mag1.compress()

        mag2 = layer.add_mag([2, 2, 1])
        mag2.write(
            data=data[::2, ::2, ::1]
        )
        mag2.compress()

        mag4 = layer.add_mag([4, 4, 1])
        mag4.write(
            data=data[::4, ::4, ::1]
        )
        mag4.compress()

        return dataset

    def upload_dataset(self):
        logging.info("Uploading dataset to server")
        try:
            with wk.webknossos_context(token=self.webknossos_token) as wk_ctx:
                url = self.dataset.upload()
                logger.info(f"Dataset uploaded to: {url}")
        except Exception as e:
            logger.error(f"Error uploading dataset: {e}")
            logger.error(e)
            if "already in use" in str(e):
                logger.warning("Dataset already in use. Trying with new timestamp.")
                self.dataset_name = self.get_dataset_name()
                self.dataset.name = self.dataset_name
                self.upload_dataset()


def main():
    parser = ArgumentParser()
    parser.add_argument("-c", "--cfg_yml_Path", type=str, required=True)

    args = parser.parse_args()
    cfg_yml_path = Path(args.cfg_yml_Path)

    roi_upload = ROISelectUpload(cfg_yml_path)
    print("ciao")


if __name__ == "__main__":
    main()
