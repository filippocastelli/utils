from pathlib import Path
import argparse
import yaml
import csv
import requests
import re

import numpy as np
import pandas as pd
from tqdm import tqdm
import webknossos as wk


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=Path, required=True, help="Path to config file")
    parser.add_argument("-o", "--output", type=Path, required=False, help="Output filepath")
    args = parser.parse_args()

    with args.config.open() as f:
        config = yaml.safe_load(f)

    if args.output is None:
        output_path = None
    else:
        output_path = Path(args.output)

    taskcreator = TaskCreator(config,
                              output_path=output_path)


class TaskCreator:
    def __init__(self,
                 config: dict,
                 dataset_name: str = None,
                 slice_id: int = None,
                 output_path: Path = Path("tasks.csv")):

        self.config = config
        self.output_path = output_path if output_path is not None else Path("tasks.csv")

        self.task_type_id = self.config["task_type_id"]
        self.project = self.config["project"]
        self.experience_domain = self.config["experience_domain"]
        self.min_experience = self.config["min_experience"]
        self.x, self.y, self.x = self.config["position"]
        self.rotX, self.rotY, self.rotZ = self.config["rotation"]
        # self.minX, self.minY, self.minZ = self.config["min_position"]
        self.width = self.config["width"]
        self.height = self.config["height"]
        self.depth = self.config["depth"]

        self.n_extractions = self.config["n_extractions"]
        self.allow_overlap = self.config["allow_overlap"] if "allow_overlap" in self.config else False

        self.random_seed = self.config["random_seed"] if "random_seed" in self.config else None
        self.instances_per_task = self.config["instances_per_task"]
        # self.dataset_shape = self.config["dataset_shape"]

        self.shear_cfg = self.config["shear"]
        self.shear = self.shear_cfg["enable"]

        self.webknossos_token = self.config["token"]
        self.webknossos_base_api_url = "http://webknossos.org/api/"
        self.webknossos_organization_id = self.config["organization_id"]

        self.subject = self.config["subject"]
        self.dataset_df = self.get_wk_datasets_df()

        if slice_id is None:
            self.dataset = self.config['dataset'] if dataset_name is None else dataset_name
        else:
            self.dataset_df["slice_id"] = self.dataset_df["name"].apply(lambda x: int(x.split("_")[2]))
            self.dataset = self.dataset_df[self.dataset_df.slice_id == slice_id].name.values[0]

        self.dataset_shape = self.get_dataset_shape(self.dataset)

        assert len(self.dataset_shape) == 3, "Dataset shape must be 3D"

        if "annotation_bound" in self.config:
            self.annotation_bound = self.config["annotation_bound"]
        else:
            self.annotation_bound = [0, self.dataset_shape[0],
                                     0, self.dataset_shape[1],
                                     0, self.dataset_shape[2]]

        assert len(self.annotation_bound) == 6, "annotation_bound must be a list of length 6"

        self.start_positions = self.sample_positions()

        self.create_tasks()

    def get_wk_datasets_df(self):
        session = requests.session()
        headers = {"X-Auth-Token": self.webknossos_token}
        out = session.get(self.webknossos_base_api_url + "datasets", headers=headers)
        df = pd.DataFrame(out.json())
        df = df[df.owningOrganization == self.webknossos_organization_id]

        rex = re.compile(f"^[0-9]{{8}}_{self.subject}_[0-9]+_LeftDet_[0-9]+_RightDet_[0-9]+_[0-9]+")

        def check_match(name):
            if rex.match(name):
                return True
            else:
                return False

        df = df[df.name.apply(check_match)]

        return df

    def get_dataset_shape(self, dataset_name: str):
        row = self.dataset_df[self.dataset_df.name == dataset_name].iloc[0]
        bbox_dict = row["dataSource"]["dataLayers"][0]["boundingBox"]
        return [bbox_dict["width"], bbox_dict["height"], bbox_dict["depth"]]

    def sample_positions(self) -> np.ndarray:
        """
        Sample positions for the task.
        """
        # set the seed
        if self.random_seed is not None:
            np.random.seed(self.random_seed)

        min_positions = np.array([
            self.annotation_bound[0],
            self.annotation_bound[2],
            self.annotation_bound[4]
        ]) + np.array([self.width, self.height, self.depth]) // 2
        max_positions = np.array([
            self.annotation_bound[1],
            self.annotation_bound[3],
            self.annotation_bound[5]
        ]) - np.array([self.width, self.height, self.depth]) // 2

        arr = np.ones(shape=max_positions)

        # set to zeros positions below the minimum
        arr[0:min_positions[0]] = 0
        arr[:, 0:min_positions[1]] = 0
        arr[:, :, 0:min_positions[2]] = 0

        centers = []
        for i in tqdm(range(self.n_extractions)):
            # nonzero_pos = np.argwhere(arr == 1)
            flat_nonzero = np.flatnonzero(arr)

            if len(flat_nonzero) == 0:
                print("No positions left to sample, skipping...")
                break
            ravel_idx = np.random.choice(flat_nonzero)
            center = np.unravel_index(ravel_idx, max_positions)
            centers.append(center)
            # idx = np.random.randint(0, len(nonzero_pos))
            # pos = nonzero_pos[idx]
            # centers.append(pos)

            if self.allow_overlap:
                arr[center[0] - self.width // 2:center[0] + self.width // 2,
                    center[1] - self.height // 2:center[1] + self.height // 2,
                    center[2] - self.depth // 2:center[2] + self.depth // 2] = 0
            else:
                arr[center[0] - self.width:center[0]+self.width,
                    center[1] - self.height:center[1]+self.height,
                    center[2] - self.depth:center[2]+self.depth] = 0

        centers = np.array(centers)
        # centers = np.random.randint(low=min_positions, high=max_positions, size=(self.n_extractions, 3))
        start_positions = centers - np.array([self.width, self.height, self.depth]) // 2

        if self.shear:
            shear_delta = self.shear_cfg["delta"]
            direction = self.shear_cfg["direction"]
            assert direction in ["x", "y"], "direction must be one of 'x', 'y'"

            sheared_shape = self.dataset_shape.copy()
            max_shear = abs(shear_delta) * sheared_shape[2]
            if direction == "x":
                for pos in start_positions:
                    pos[0] = pos[0] + max_shear - shear_delta * pos[2]
            elif direction == "y":
                for pos in start_positions:
                    pos[1] = pos[1] + max_shear - shear_delta * pos[2]

        return start_positions

    # create a CSV file where every line is a task in the format: dataSet, taskTypeId, experienceDomain,
    # minExperience, x, y, z, rotX, rotY, rotZ, instances, minX, minY, minZ, width, height, depth, project
    def create_tasks(self):
        csv_lines = []
        for i, start_position in enumerate(self.start_positions):
            csv_line = [self.dataset, self.task_type_id, self.experience_domain, self.min_experience,
                        start_position[0], start_position[1], start_position[2],
                        self.rotX, self.rotY, self.rotZ, self.instances_per_task,
                        start_position[0], start_position[1], start_position[2],
                        self.width, self.height, self.depth, self.project]
            print(", ".join(map(str, csv_line)))
            csv_lines.append(csv_line)

        # write to file
        with open(self.output_path, "a") as f:
            writer = csv.writer(f)
            writer.writerows(csv_lines)


if __name__ == '__main__':
    main()
