#!/bin/bash

# to save output to file execute
# ./upload_slices.sh 2>&1 | tee -a upload_slices.log

# activating zetastitcher virtual environment
source /home/phil/anaconda3/bin/activate zetastitcher

echo "Python executable: ";
which python;

# defining slice numbers
SLICES=(20 34 24 36 43 5 16 31 46 41);

# iterating over slice numbers
for SLICE in "${SLICES[@]}"; do
  echo "Uploading slice $SLICE...";
  command="python roi_upload.py -c slice_upload_configs/slice$SLICE.yml";
  echo "executing $command...";
  eval $command;
  done;





