from pathlib import Path
from time import gmtime, strftime

import numpy as np
import webknossos as wk
from webknossos import webknossos_context, Project, Task, AnnotationState, Mag
from tqdm import tqdm
import tifffile

auth_token = "EVJL157aAXCRA1yxbobkPg"
org_id = "cdbbcad7d293338e"

download_path = Path("/mnt/NASoneScratch/castelli/roi_upload_out")
download_path.mkdir(parents=True, exist_ok=True)

data_out = Path("/mnt/NASoneScratch/castelli/webknossos_downloads2")
data_out.mkdir(exist_ok=True, parents=True)

completed_path = data_out / "completed"
completed_path.mkdir(exist_ok=True, parents=True)
not_completed_path = data_out / "not_completed"
not_completed_path.mkdir(exist_ok=True, parents=True)


def is_completed(task: Task):
    annotation_infos = list(task.get_annotation_infos())
    if len(annotation_infos) == 0:
        return False

    for info in annotation_infos:
        if info.state == AnnotationState.FINISHED:
            return True

def get_dataset_info(task: Task):

    infos = task.get_annotation_infos()
    if  len(infos) > 1:
        print("More than one annotation info found for task {}".format(task.name))
        print("Using first one")
    
    dataset_name = task.dataset_name
    datestr, subject, slice_n, _, left_freq, _, right_freq, timestamp = dataset_name.split("_")
    slice_n = int(slice_n)
    left_freq = int(left_freq)
    right_freq = int(right_freq)
    timestamp = int(timestamp)
    
    dataset_info = {
        "dataset_name": dataset_name,
        "datestr": datestr,
        "subject": subject,
        "slice_n": slice_n,
        "left_freq": left_freq,
        "right_freq": right_freq,
        "timestamp": timestamp,
        "dataset_name": dataset_name,
        "task_name": task.name,
    }

    return dataset_info
    

with webknossos_context(token=auth_token) as wkc:
    proj = Project.get_by_name("SOMA_ANNOTATION_NIH")
    tasks = proj.get_tasks()

    completed_tasks = []
    completed_task_ids = []
    finished_annotations = []
    incomplete_annotations = []
    incomplete_tasks = []
    duplicate_annotation_infos = []
    for idx, task in enumerate(tqdm(tasks)):
        annotation_infos = list(task.get_annotation_infos())
        if len(annotation_infos) > 1:
            duplicate_annotation_infos.append(task.task_id)
        for info in annotation_infos:
            if info.state == AnnotationState.FINISHED:
                completed_tasks.append(task)
                completed_task_ids.append(task.task_id)
                finished_annotation = info.download_annotation()
                finished_annotations.append(finished_annotation)

                dataset_info = get_dataset_info(task)
                dataset_name = dataset_info["dataset_name"]

                # dataset = wk.Dataset.download(dataset_name)
                # time_str = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
                # new_ds_name = dataset_name + f"_segmented_{time_str}"

                ds = wk.Dataset.download(dataset_name,
                                         path=str(download_path.joinpath(dataset_name)))

                if "volume_layer" in ds.layers:
                    ds.delete_layer("volume_layer")
                try:
                    volume_annotation = finished_annotation.export_volume_layer_to_dataset(ds)
                except Exception as e:
                    print(e)
                    incomplete_tasks.append(task.task_id)
                    continue

                # else:
                #    volume_annotation = ds.layers["volume_layer"]
                bounding_box = finished_annotation.task_bounding_box
                volume_annotation.bounding_box = bounding_box

                mag = Mag(1)
                mag_view = ds.layers["color"].mags[mag]

                frame_data = mag_view.read(absolute_bounding_box=bounding_box)
                annotation_data = volume_annotation.mags[mag].read()

                frame_data = np.squeeze(frame_data).transpose()
                annotation_data = np.squeeze(annotation_data).transpose()
                # if annotation_data.max() > 0:
                if annotation_data.max() == 0:
                    incomplete_tasks.append(task.task_id)
                    incomplete_annotations.append(info.id)
                    base_path = not_completed_path
                else:
                    base_path = completed_path
                base_name = f"sub-{dataset_info['subject']}_ses-SPIM_sample-BrocaArea_stain-NeuN_chunk-{str(dataset_info['slice_n'])}_{str(idx)}"

                frame_out_path = base_path.joinpath(task.task_id + "_frame.tif")
                annotation_out_path = base_path.joinpath(task.task_id + "_annotation.tif")

                tifffile.imwrite(str(frame_out_path), frame_data)
                tifffile.imwrite(str(annotation_out_path), annotation_data)


def dump_list(elem_list: list, fname):
    out_path = data_out.joinpath(fname)
    with out_path.open(mode="w") as outf:
        for elem in elem_list:
            outf.write(elem + "\n")


dump_list(incomplete_tasks, "incomplete_tasks.txt")
dump_list(incomplete_annotations, "incomplete_annotations.txt")
dump_list(completed_task_ids, "complete_task_ids.txt")
dump_list(duplicate_annotation_infos, "duplicate.txt")
