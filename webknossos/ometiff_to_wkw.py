from argparse import ArgumentParser
import zipfile
from datetime import datetime
from pathlib import Path
import shutil
import os
import logging
from typing import Union, Tuple

import numpy as np
import webknossos as wk
import pyometiff
import yaml

logging.basicConfig(level=logging.INFO)


def main():
    parser = ArgumentParser(description="Convert OMETIFFs to WKW")

    parser.add_argument("-s", "--source",
                        dest="source_stack_path",
                        help="source stack path",
                        type=str)

    parser.add_argument("-o", "--output",
                        dest="output_path",
                        help="output folder path",
                        action="store",
                        default="",
                        type=str)

    parser.add_argument("--scale",
                        dest="scale",
                        help="physical scale",
                        action="store",
                        type=str,
                        default=None)

    parser.add_argument("-t", "--token",
                        dest="token",
                        help="webknossos upload token",
                        action="store",
                        type=str,
                        )

    args = parser.parse_args()
    source_path = Path(args.source_stack_path)

    if args.output_path != "":
        output_path = Path(args.output_path)
    else:
        output_path = None

    if args.scale is not None:
        scale_str_split = args.scale.split(" ")
        scale = [float(scale_string) for scale_string in scale_str_split]
    else:
        scale = None

    OMETIFF2WKW(
        stack_path=source_path,
        out_path=output_path,
        scale=scale,
        token=args.token
    )


class OMETIFF2WKW:
    """
    Class for converting OMETIFFs to WKW.
    """

    def __init__(self,
                 stack_path: Path,
                 out_path: Path = None,
                 scale: Union[list, tuple, None] = None,
                 token: str = None):

        self.stack_path = stack_path
        self.out_path = out_path if out_path is not None else stack_path.parent
        self.out_path.mkdir(exist_ok=True, parents=True)

        self.token = token
        if self.token is None:
            # try to read token from environment variable WEBKNOSSOS_TOKEN
            try:
                logging.info("trying to read token from environment variable WEBKNOSSOS_TOKEN")
                self.token = os.environ.get("WEBKNOSSOS_TOKEN")
            except KeyError:
                logging.info("no token found, I won't be uploading to webknossos")

        self.scale = scale

        # compute dataset name
        path_stem = self.stack_path.stem
        if "." in path_stem:
            path_stem = path_stem.split(".")[0]

        self.dataset_name = path_stem

        # check if the stack exists
        logging.info(f"Checking if {self.stack_path} exists")
        assert self.stack_path.is_file(), f"stack {str(stack_path)} does not exist"

        # set output path
        self.dataset_output_path = self.out_path.joinpath(self.dataset_name)

        # check if the dataset already exists
        if self.dataset_output_path.exists():
            logging.warning(f"Dataset {self.dataset_output_path} already exists. Overwriting old dataset.")
            shutil.rmtree(str(self.dataset_output_path))

        self._data, self._meta = self._read_data()

        if scale is not None:
            self._meta = {
                "PhysicalSizeX": scale[0],
                "PhysicalSizeY": scale[1],
                "PhysicalSizeZ": scale[2]
            }

        self._dataset = self._create_dataset()

        if self.token is not None:
            self._upload_dataset()

        self.compress_folder(self.dataset_output_path)

    def _read_data(self) -> Tuple[np.ndarray, dict]:
        """
        Reads the data from the stack and returns it as a numpy array and a dictionary with meta data.
        """
        logging.info(f"Reading data from {self.stack_path}")
        ome_reader = pyometiff.OMETIFFReader(self.stack_path)
        arr, meta, metaxml = ome_reader.read()

        # adjusting dims
        # input shape is (z, y, x), we need (c, x, y, z)
        # swap z and x
        arr = np.swapaxes(arr, 0, 2)
        # insert a channel dimension
        # arr = np.expand_dims(arr, axis=0)
        # the dimension is now (c, x, y, z), return

        # if ignoring channel we have (x,y,z)
        return arr, meta

    def _create_dataset(self) -> wk.Dataset:
        """
        Creates a WK dataset and returns the dataset object
        """
        logging.info(f"Creating dataset {self.dataset_output_path}")
        scale = (self._meta["PhysicalSizeX"], self._meta["PhysicalSizeY"], self._meta["PhysicalSizeZ"])
        dataset = wk.Dataset.create(dataset_path=str(self.dataset_output_path),
                                    scale=scale,
                                    name=self.dataset_name)
        layer = dataset.add_layer(
            layer_name="color_layer",
            category="color",
            dtype_per_layer=self._data.dtype
        )
        mag1 = layer.add_mag(1)
        mag1.write(
            data=self._data
        )
        mag1.compress()

        mag2 = layer.add_mag([2, 2, 1])
        # downsample the data to 2-2-1
        mag2.write(
            data=self._data[::2, ::2, ::1]
        )
        mag2.compress()

        # downsample the data to 4-4-1
        mag4 = layer.add_mag([4, 4, 1])
        mag4.write(
            data=self._data[::4, ::4, ::1]
        )
        mag4.compress()

        return dataset

    @staticmethod
    def get_timestr() -> str:
        """
        Returns a string with the current time in the format YYYY-MM-DD_HH-MM-SS
        """
        timestr = str(int(round(datetime.timestamp(datetime.now()))))
        # timestr = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
        return timestr

    @staticmethod
    def compress_folder(dir_path: Path) -> None:
        """
        Compresses a folder and all its subfolders
        """
        logging.info(f"Compressing {dir_path}")
        with zipfile.ZipFile(str(dir_path) + ".zip", mode="w") as zf:
            for dirname, subdirs, files in os.walk(str(dir_path)):
                dirname_path = Path(dirname)
                dirname_relative = dirname_path.relative_to(dir_path)
                dirname_relative_str = str(dirname_relative) if str(dirname_relative) != "." else ""
                zf.write(dirname_path, arcname=dirname_relative_str)
                for filename in files:
                    zf.write(os.path.join(dirname, filename), arcname=os.path.join(dirname_relative_str, filename))

    def _upload_dataset(self) -> None:
        """
        Uploads the dataset to the WK server
        """
        logging.info(f"Uploading dataset {self.dataset_output_path}")
        try:
            with wk.webknossos_context(token=self.token) as wkc:
                url = self._dataset.upload()
                print(f"Dataset uploaded to {url}")
        except Exception as e:
            logging.error(f"Error uploading dataset {self.dataset_output_path}")
            logging.error(e)
            if "already in use" in str(e):
                logging.warning(f"Dataset {self.dataset_output_path} already exists. Retrying with different name.")
                self.dataset_name = f"{self.dataset_name}_{self.get_timestr()}"
                self._dataset.name = self.dataset_name
                self._upload_dataset()


if __name__ == "__main__":
    main()
