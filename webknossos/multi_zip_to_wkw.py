from pathlib import Path
import logging
import zipfile
import os
from time import strftime, gmtime
import argparse
import shutil
from datetime import datetime

import wkw
import yaml
import numpy as np

import zetastitcher
from pyometiff import OMETIFFWriter
import webknossos as wk

logging.basicConfig(level=logging.INFO)


def main():
    parser = argparse.ArgumentParser(description='Convert a multi-zipped dataset to a webknossos dataset.')

    parser.add_argument('-c', '--config', type=str, required=True, help='Path to the config file.')
    parser.add_argument('-o', '--output', type=str, required=True, help='Path to the output directory.')
    parser.add_argument('-u', '--upload_config', type=str, required=True, help='Path to the upload config file.')
    parser.add_argument('-s', '--start', type=int, required=True, help='Start index of the dataset.')
    parser.add_argument('-e', '--end', type=int, required=True, help='End index of the dataset.')
    parser.add_argument('--offline', action="store_true", default=False, help='Disable upload  .')

    args = parser.parse_args()
    config_path = Path(args.config)
    output_path = Path(args.output)
    upload_config_path = Path(args.upload_config) if args.upload_config != "" else None
    start = args.start
    end = args.end

    MultiZipToWKW(
        cfg_fpath=config_path,
        output_dir=output_path,
        start=start,
        end=end,
        upload_config_fpath=upload_config_path,
        offline=args.offline
    )


class MultiZipToWKW:
    """
    Convert a JP2 zip to an OME-TIFF stack
    """

    def __init__(self,
                 cfg_fpath: Path,
                 output_dir: Path,
                 compression: str = 'deflate',
                 start: int = 0,
                 end: int = -1,
                 upload_config_fpath: Path = Path("upload_config.yml"),
                 offline: bool = False):

        self.cfg_fpath = cfg_fpath
        self.output_dir = output_dir

        self.start = start
        self.end = end

        self.compression = compression

        self.cfg = self.parse_cfg(self.cfg_fpath)
        self.channel_dict = self.cfg["channels"]

        self.output_path = self.output_dir.joinpath(self.cfg["dataset_name"] + ".ome.tiff")
        self.dataset_path = self.output_dir.joinpath(self.cfg["dataset_name"]+ "_" + self.get_timestr())

        if self.dataset_path.exists():
            logging.warning(f"Dataset {self.dataset_path} already exists. Overwriting old dataset.")
            shutil.rmtree(str(self.dataset_path))

        self.dataset_path.mkdir(exist_ok=True, parents=True)

        # check if start end end are valid
        if start < 0:
            self.start = 0
        if end < start:
            raise ValueError("end must be greater than start")
        self.start = start
        self.end = end
        self.ometiff_config = self.get_ometiff_config()
        self.data = self.load_data()

        self.write_ometiff()

        self.upload_config_fpath = upload_config_fpath
        self.upload_config = self.parse_cfg(self.upload_config_fpath)

        self.token = self.upload_config["token"]
        self.dataset_name = self.cfg["dataset_name"]
        self.dataset_name = f"{self.dataset_name}_{self.get_timestr()}"

        self.dataset = self.create_wkw_dataset()
        self.offline = offline

        if not self.offline:
            self.upload_dataset()

        self.compress_folder(self.dataset_path)

    def load_data(self) -> np.ndarray:
        """
        Load the channels from the config file
        """
        data = []
        for channel_name, channel_path_str in self.channel_dict.items():
            channel_fpath = Path(channel_path_str)
            channel_inputfile = zetastitcher.InputFile(channel_fpath)

            channel_shape = channel_inputfile.shape

            if self.end < channel_shape[0]:
                channel_data = channel_inputfile[self.start:self.end]
                channel_data = np.reshape(channel_data, (self.end - self.start, 1, *channel_data.shape[1:]))
            else:
                raise ValueError("end must be smaller than the number of slices in the channel")

            data.append(channel_data)

        return np.stack(data, axis=2)

    @staticmethod
    def parse_cfg(yml_path: Path) -> dict:
        """
        Parse the config file and return a dict
        """
        with open(yml_path, 'r') as f:
            cfg = yaml.safe_load(f)
        return cfg

    def get_ometiff_config(self) -> dict:
        """
        Get the OME-TIFF config as a dict
        """
        resolution = self.cfg["resolution"]
        return {
            "PhysicalSizeX": resolution[2],
            "PhysicalSizeXUnit": "nm",
            "PhysicalSizeY": resolution[1],
            "PhysicalSizeYUnit": "nm",
            "PhysicalSizeZ": resolution[0],
            "PhysicalSizeZUnit": "nm",
            "Channels": self._get_channel_config_dict(),
        }

    def _get_channel_config_dict(self):
        """
        Get the channel config dict
        """

        return {str(key): {
            "Name": str(key),
            "SamplesPerPixel": 1,
            "ExcitationWavelength": int(key),
            "ExcitationWavelengthUnit": "nm"}
            for key in self.channel_dict.keys()}

    def write_ometiff(self):
        """
        Write the OME-TIFF stack
        """
        ometiff_writer = OMETIFFWriter(
            fpath=self.output_path,
            dimension_order="ZTCYX",
            array=self.data,
            metadata=self.ometiff_config,
            compression=self.compression,
        )
        ometiff_writer.write()

    def create_wkw_dataset(self) -> wk.Dataset:
        data = self.data
        # data shape is (z, 1, c, y, x), we need (c, x, y, z)

        data = np.squeeze(data, axis=1)  # squeeze the time dimension, (z, 1, c, y, x) -> (z, c, y, x)
        data = np.moveaxis(data, 1, 0)  # move the channel dimension to the first dimension, (z, c, y, x) -> (c, z,
        # y, x)
        data = np.swapaxes(data, 1, -1)  # swap the y and x dimensions, (c, z, y, x) -> (c, x, y, z)
        scale = self.cfg["resolution"]
        wkw_dataset = wk.Dataset.create(
            dataset_path=str(self.dataset_path),
            scale=scale,
            name=self.dataset_name,
        )

        for i, channel_name in enumerate(self.channel_dict.keys()):
            channel_name = f"color_{channel_name}"
            self.add_channel_layer(dataset=wkw_dataset,
                                   channel_name=str(channel_name),
                                   channel_data=data[i])

        return wkw_dataset

    @staticmethod
    def add_channel_layer(dataset: wk.Dataset,
                          channel_name: str,
                          channel_data: np.ndarray,
                          ) -> wk.Layer:
        # channel_data = np.expand_dims(channel_data, axis=0)  # add an empty channel dimension
        color_layer = dataset.add_layer(layer_name=channel_name,
                                        category="color",
                                        dtype_per_layer=channel_data.dtype)
        mag1 = color_layer.add_mag(1)
        mag1.write(data=channel_data)
        mag1.compress()

        mag2 = color_layer.add_mag([2, 2, 1])
        mag2.write(data=channel_data[::2, ::2, ::1])
        mag2.compress()

        mag4 = color_layer.add_mag([4, 4, 1])
        mag4.write(data=channel_data[::4, ::4, ::1])
        mag4.compress()

        return color_layer

    @staticmethod
    def compress_folder(dir_path: Path) -> None:
        """
        Compresses a folder and all its subfolders
        """
        logging.info(f"Compressing {dir_path}")
        with zipfile.ZipFile(str(dir_path) + ".zip", mode="w") as zf:
            for dirname, subdirs, files in os.walk(str(dir_path)):
                dirname_path = Path(dirname)
                dirname_relative = dirname_path.relative_to(dir_path)
                dirname_relative_str = str(dirname_relative) if str(dirname_relative) != "." else ""
                zf.write(dirname_path, arcname=dirname_relative_str)
                for filename in files:
                    zf.write(os.path.join(dirname, filename), arcname=os.path.join(dirname_relative_str, filename))

    @staticmethod
    def get_timestr() -> str:
        """
        Returns a string with the current time in the format YYYY-MM-DD_HH-MM-SS
        """
        timestr = str(int(round(datetime.timestamp(datetime.now()))))
        # timestr = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
        return timestr

    def upload_dataset(self) -> None:
        """
        Uploads the dataset to the WK server
        """
        logging.info(f"Uploading dataset {self.dataset_path}")
        try:
            with wk.webknossos_context(token=self.token) as wkc:
                url = self.dataset.upload()
                print(f"Dataset uploaded to {url}")
        except Exception as e:
            logging.error(f"Error uploading dataset {self.dataset_path}")
            logging.error(e)
            if "already in use" in str(e):
                logging.warning(f"Dataset {self.dataset_path} already exists. Retrying with different name.")
                self.dataset_name = f"{self.dataset_name}_{self.get_timestr()}"
                self.dataset.name = self.dataset_name
                self.upload_dataset()


if __name__ == "__main__":
    main()
