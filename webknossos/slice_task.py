import sys
sys.path.append("/home/phil/repos/utils/webknossos")
import csv
from pathlib import Path
import yaml

from bulk_task import TaskCreator

slice_lims_path = Path("task_slice_lims.csv")
config_path = Path("bulk_task_cfg.yml")

with config_path.open("r") as f:
    config = yaml.load(f, Loader=yaml.FullLoader)
with slice_lims_path.open(mode="r") as f:
    reader = csv.DictReader(f)
    lines = list(reader)

for line in lines:
    print(line)
    output_path = Path("slice_"+line["slice_n"]+".csv")
    task_creator = TaskCreator(
        config=config,
        slice_id=int(line["slice_n"]),
        output_path=output_path)


print("debug")