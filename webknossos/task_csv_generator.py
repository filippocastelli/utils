from pathlib import Path
from typing import Union
import argparse
import yaml
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--cfg",
                        dest="config_yml",
                        help="config yml path",
                        type=str,
                        action="store")

    parser.add_argument("-i", "--instances",
                        action="store",
                        type=int,
                        default=1,
                        dest="instances",
                        help="instances")

class WebKnossosTaskCSVGenerator:
    def __init__(self,
                 cfg_yml_path: Union[Path, str],
                 instances: int = 1,
                 global_bbox: Union[list, tuple] = None
                 ):
        self.cfg_yml_path = Path(cfg_yml_path)
        self.instances = instances
        self.global_bbox = global_bbox

    @staticmethod
    def get_pivots(instances: int,
                   global_bbox: Union[list, tuple],
                   ):
        pass


    @staticmethod
    def _read_cfg(cfg_path):
        with cfg_path.open(mode="r") as cfg_file:
            cfg = yaml.load(cfg_file)
        return cfg


if __name__ == "__main__":
    main()