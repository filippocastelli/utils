from argparse import ArgumentParser, RawTextHelpFormatter
from pathlib import Path
import shutil
import logging
import yaml
import datetime
from collections import OrderedDict

from pyometiff import OMETIFFWriter

import numpy as np
import tifffile
from tqdm import tqdm

from zetastitcher import VirtualFusedVolume

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

zetastitcher_logger = logging.getLogger("zetastitcher")
zetastitcher_logger.setLevel(logging.WARNING)


def setup_yaml():
    """add represented for OrderedDicts to pyYAML
  see https://stackoverflow.com/a/8661021 """
    represent_dict_order = lambda self, data: self.represent_mapping('tag:yaml.org,2002:map', data.items())
    yaml.add_representer(OrderedDict, represent_dict_order)


setup_yaml()

EXAMPLE_CONFIG_DICT = {
    "crop_shape": [200, 200, 200],
    "n_random_crops": 10,
    "offsets": {
        "channel_0": [0, 0, 0],
        "channel_1": [1, 2, 3]
    },
    "stitch_file_paths": {
        "channel_0": "path/to/channel0.stitch",
        "channel_1": "path/to/channel1.stitch",
    },
    "bounds": {
        "low": [0, 0, 0],
        "high": [-1, -1, -1]
    }
}


def main():
    description_txt = "Extract GT slices from a stitched volume \n\
        the configuration YAML should be formatted as \n\n\n\
        \t\t- conf.yml - \n\
        \tcrop_shape: [crop_z, crop_y, crop_x] \n\
        \tn_random_crops: n_crops \n\
        \tpivot_list: \n\
        \t\t - [z_0, y_0, x_0] \n\
        \t\t - [z_1, y_1, x_1] \n\n\
        \tstitch_file_paths: \n\
        \t\t - channel_0: path_to_stitch_yml0 \n\
        \t\t - channel_1: path_to_stitch_yml1 \n\
        \toffsets: \n\
        \t\t - channel_0: [0,0,0] \n\
        \t\t - channel_1: [o_z, o_y, o_x] \n\
        \tbounds: \n\
        \t\t - low: [0,0,0] \n\
        \t\t - high: [-1, -1, -1] \n\
        \twhere \n\n\
        \t stitch_file_paths: paths to zetastitcher stitch.yml stitch file \n\
        \t crop_shape: shape of the crop selection \n\
        \t n_random_crops: if random is true, extracts n_random_crops crops \n\
        \t pivot_list: list of ZYX format coordinates in stitched space (optional)\n\
        \t\t if pivot_list is present it overrides the random_behaviour \n\
        \t bounds: low/high boundaries in which to extract patches (optional)"

    parser = ArgumentParser(
        description=description_txt,
        epilog="Author: Filippo Maria Castelli <castelli@lens.unifi.it>",
        formatter_class=RawTextHelpFormatter)

    parser.add_argument(
        "--conf",
        type=str,
        action="store",
        dest="conf_yml_path",
        help="Configuration yml path")

    parser.add_argument(
        "--out",
        type=str,
        action="store",
        dest="out_path",
        help="Out path")

    parser.add_argument(
        "--gen_cfg",
        action="store_true",
        dest="gen_cfg",
        help="Generate a sample config file")

    args = parser.parse_args()

    out_path = Path(args.out_path) if args.out_path is not None else Path.cwd().joinpath("out")
    conf_yml_path = Path(args.conf_yml_path) if args.conf_yml_path is not None else None

    if args.gen_cfg:
        sample_yml_path = Path.cwd().joinpath("sample_cfg.yml")
        logging.info("Writing a sample configuration_file to {}".format(str(sample_yml_path)))
        _save_yaml(EXAMPLE_CONFIG_DICT, sample_yml_path)
        exit()

    conf_dict = _load_yaml(conf_yml_path)

    crop_shape = conf_dict["crop_shape"]
    # choose_random = conf_dict["random"]
    n_random_crops = conf_dict["n_random_crops"]
    stitch_file_paths = conf_dict["stitch_file_paths"]
    offsets = conf_dict["offsets"]
    bounds = conf_dict["bounds"]
    pivot_list = conf_dict["pivot_list"] if "pivot_list" in conf_dict else None

    ometiff_cfg = conf_dict["ometiff_cfg"]

    _ = MultiChannelVolumeExtractor(
        stitch_file_paths=stitch_file_paths,
        offsets=offsets,
        crop_shape=crop_shape,
        n_random_crops=n_random_crops,
        pivot_list=pivot_list,
        out_path=out_path,
        bounds=bounds,
        ometiff_cfg=ometiff_cfg)


class MultiChannelVolumeExtractor:

    def __init__(self,
                 stitch_file_paths,
                 offsets,
                 out_path,
                 bounds,
                 pivot_list=None,
                 crop_shape=(200, 200, 200),
                 n_random_crops=10,
                 ometiff_cfg=None
                 ):

        self.stitch_file_paths = stitch_file_paths
        self.channels = list(self.stitch_file_paths.keys())
        self.offsets = self._get_offset_dict(offsets)
        self.crop_shape = crop_shape
        self.n_random_crops = n_random_crops
        self.out_path = out_path
        self.ometiff_cfg = ometiff_cfg

        self.bounds = bounds

        # derived attributes

        self.extract_random = True if pivot_list is None else False

        self.vfv_dict = self._get_vfv_dict()
        self.pivot_list = self.get_pivot_list(pivot_list)

        self._backup_stitchfiles()
        self.extract_volumes(self.pivot_list)

    def _backup_stitchfiles(self):
        """makes backups of stitch_files"""
        self.backup_stitch_dir = self.out_path.joinpath("stitchfile_backups")
        self.backup_stitch_dir.mkdir(exist_ok=True, parents=True)

        logging.info("Making backups of original stitch files...")
        for channel, fpath in self.stitch_file_paths.items():
            out_fpath = self.backup_stitch_dir.joinpath(str(channel) + "_stitch_file.yml")
            shutil.copy(str(fpath), str(out_fpath))

    def _get_offset_dict(self, offset_dict):
        """run sanity checks on offsets then make a complete """
        assert set(offset_dict.keys()).issubset(
            set(self.stitch_file_paths.keys())), "There are items in offsets which do not match any stitch file in " \
                                                 "stitch_file_paths "
        default_offset = [0, 0, 0]
        return {key: offset_dict[key] if key in self.channels else default_offset for key in self.channels}

    def _get_vfv_dict(self):
        """get a {channel: VirtualFusedVolume} dict"""

        logging.info("Loading virtual volumes...")
        vfv_dict = {}
        vol_shapes = []
        for channel, fpath in self.stitch_file_paths.items():
            vfv = VirtualFusedVolume(str(fpath))
            vol_shapes.append(vfv.shape)
            vfv_dict[channel] = vfv

        # volume shape sanity check
        shapes_ok = [shape == vol_shapes[0] for shape in vol_shapes]
        assert all(shapes_ok), "volumes have different shapes"

        return vfv_dict

    def get_pivot_list(self, parsed_pivot_list=None):
        """get the list of pivots"""
        # pivots have to be defined here
        # in both cases, pivot coming out of a list or pivots being randomly
        # generated, pivots have to be tested on all vfvs

        if parsed_pivot_list is not None:
            pivot_list = parsed_pivot_list
        else:
            # use first channel as a stitch reference for pivots
            pivot_list = self._get_random_pivot_list(self.n_random_crops)

        return pivot_list

    def _get_random_pivot_list(self, n_random_pivots):
        """generate a random valid pivot list"""

        if self.bounds is not None:
            bound_min = np.array(self.bounds["low"])

            bounds_high = []
            for bound_idx, bound in enumerate(self.bounds["high"]):
                if bound > 0:
                    bounds_high.append(bound)
                else:
                    bounds_high.append(self.vfv_dict[self.channels[0]].shape[bound_idx])

            bound_max = np.array(bounds_high) - np.array(self.crop_shape)
        else:
            bound_min = np.array([0, 0, 0])
            bound_max = np.array(self.vfv_dict[self.channels[0]].shape) - np.array(self.crop_shape)

        pivots = []
        pivots_str = []

        logging.info("Extracting {} random pivots...".format(n_random_pivots))
        for idx in tqdm(range(n_random_pivots)):

            pass_condition = False
            pivot = self._gen_random_pivot(bound_min, bound_max)
            try_counter = 0
            while not pass_condition:
                # logger.debug(f"try count: {try_counter}")
                peek_channels = self._peek_check(pivot, return_peek=True)
                peek_lengths = np.array([len(peek) for peek in peek_channels.values()])
                pivot_str_list = np.char.mod("%d", pivot)
                pivot_str = ",".join(pivot_str_list)

                if pivot_str in pivots_str:
                    logger.debug("pivot duplicate...")
                    logger.debug("retry random extraction...")
                    pivot = self._gen_random_pivot(bound_min, bound_max)
                    pass_condition = False
                    try_counter = 0

                if try_counter > 5:
                    logger.debug("retry random extraction...")
                    pivot = self._gen_random_pivot(bound_min, bound_max)
                    pass_condition = False
                    try_counter = 0

                if all(peek_lengths == 1):
                    peek_valid_shapes = [self._peek_is_valid_shape(peek) for peek in peek_channels.values()]
                    if all(peek_valid_shapes):
                        pivots.append(pivot)
                        pivots_str.append(pivot_str)
                        pass_condition = True
                        try_counter = 0
                    else:
                        pivot = self._gen_random_pivot(bound_min, bound_max)
                        pass_condition = False
                        try_counter = 0

                elif all(peek_lengths == 2):
                    # move on one channel
                    pivot = self._move_pivot(pivot, self.channels[0], peek_channels[self.channels[0]])
                    try_counter += 1
                    pass_condition = False
                else:
                    pivot = self._gen_random_pivot(bound_min, bound_max)
                    pass_condition = False
                    try_counter = 0

        return pivots

    def _peek_check(self, pivot, return_peek=False):
        """return a {channel: True/False} dict with single-tile conditions
            can return a {channel: peek} dict if return_peek=True"""
        return_dict = {}
        for channel in self.channels:
            return_dict[channel] = self._peek_check_single_channel(pivot, channel, return_peek=return_peek)

        return return_dict

    def _peek_check_single_channel(self, pivot, channel, return_peek=False):
        """check if peek returns one single tile, can return peek if return_peek=True"""
        slices = self._pivot_to_slices(pivot, self.offsets[channel])
        peek = self.vfv_dict[channel].peek[slices[0], slices[1], slices[2]]

        if not return_peek:
            return len(peek) == 1
        else:
            return peek

    def _move_pivot(self, pivot, channel, peek):
        """try to move a pivot in the direction that's closest to an interface"""
        diff_lower = self._zyx_slices_to_diffs(peek[0][1])
        diff_upper = self._zyx_slices_to_diffs(peek[1][1])

        diffs = np.vstack([diff_lower, diff_upper])

        min_diff_loc = np.unravel_index(diffs.argmin(), diffs.shape)

        axis = min_diff_loc[1]
        direction = +1 if min_diff_loc[0] == 0 else -1

        delta = np.array([0, 0, 0])
        delta[axis] = direction * self.crop_shape[axis]

        return pivot + delta

    @staticmethod
    def _gen_random_pivot(bound_min, bound_max):
        """generate a random pivot inside bounds, is static"""
        rand_z = np.random.randint(bound_min[0], bound_max[0])
        rand_y = np.random.randint(bound_min[1], bound_max[1])
        rand_x = np.random.randint(bound_min[2], bound_max[2])

        return np.array([rand_z, rand_y, rand_x])

    def _pivot_to_slices(self, pivot, offset=[0, 0, 0]):
        """convert a pivot to (z,y,x) slices"""
        offset_pivot = np.array(pivot) + np.array(offset)
        z_slice = slice(offset_pivot[0], offset_pivot[0] + self.crop_shape[0], 1)
        y_slice = slice(offset_pivot[1], offset_pivot[1] + self.crop_shape[1], 1)
        x_slice = slice(offset_pivot[2], offset_pivot[2] + self.crop_shape[2], 1)

        return (z_slice, y_slice, x_slice)

    def _peek_is_valid_shape(self, peek):
        """ check if peek produces a valid crop shape"""
        if len(peek) > 1:
            return False

        zyx_slices = peek[0][1]
        diff_array = self._zyx_slices_to_diffs(zyx_slices)

        return (diff_array == np.array(self.crop_shape)).all()

    @staticmethod
    def _zyx_slices_to_diffs(zyx_slices):
        return np.array([slice_.stop - slice_.start for slice_ in zyx_slices])

    def extract_volumes(self, pivot_list):

        stitch_file_paths = {key: str(item) for key, item in self.stitch_file_paths.items()}
        offsets = {key: np.array(item).tolist() for key, item in self.offsets.items()}
        bounds = {key: np.array(item).tolist() for key, item in self.bounds.items()}

        descriptor_yml_dict = {}

        global_settings_dict = {
            "stitch_file_paths": stitch_file_paths,
            "offsets": offsets,
            "bounds": bounds,
            "out_path": str(self.out_path),
            "crop_shape": np.array(self.crop_shape).tolist(),
        }

        descriptor_yml_dict["global_config"] = global_settings_dict

        logging.info("Extracting crops from virtual volumes...")
        for idx, pivot in enumerate(tqdm(pivot_list)):

            idx_str = format(idx, "05d")
            crop_name = "crop_{}".format(idx_str)
            crop_path_dir = self.out_path.joinpath(crop_name)
            crop_path_dir.mkdir(exist_ok=True, parents=True)

            pivot_yml_dict = {"output_path": str(crop_path_dir),
                              "crop_shape": self.crop_shape,
                              "stitch_pivot": pivot.tolist(),
                              "bounds": self.bounds}
            volume_dict = {}

            for channel in self.channels:
                offset = self.offsets[channel]
                zyx_slices = self._pivot_to_slices(pivot, offset)
                offset_pivot = np.array(pivot) + np.array(offset)

                volume_crop = self.vfv_dict[channel][zyx_slices[0], zyx_slices[1], zyx_slices[2]]
                volume_dict[channel] = volume_crop
                assert list(volume_crop.shape) == self.crop_shape, "wrong shape of crop {}/{}".format(pivot, channel)

                peek = self.vfv_dict[channel].peek[zyx_slices[0], zyx_slices[1], zyx_slices[2]]
                assert len(peek) == 1

                crop_path_tiff = crop_path_dir.joinpath("crop_{}_{}.tiff".format(channel, idx_str))
                tifffile.imsave(file=crop_path_tiff, data=volume_crop)
                acquisition_path, acquisition_slices = peek[0]
                acquisition_coords = np.array([slice_.start for slice_ in acquisition_slices]).tolist()

                channel_yml_dict = {
                    "stitch_coords": pivot.tolist(),
                    "stitch_offset": np.array(offset).tolist(),
                    "stitch_coords_offset": offset_pivot.tolist(),
                    "stitch_file_path": str(self.stitch_file_paths[channel]),
                    "acquisition_path": str(acquisition_path),
                    "acquisition_coords": acquisition_coords,
                    "output_path": str(crop_path_tiff),

                }

                pivot_yml_dict[channel] = channel_yml_dict

            vol_list = list(volume_dict.values())
            vol_array = np.stack(vol_list, axis=0)

            # channel first
            # vol_array = np.moveaxis(vol_array, 0, -1)
            out_vol_path = crop_path_dir.joinpath("crop_{}.ome.tiff".format(idx_str))
            # tifffile.imwrite(str(out_vol_path), vol_array)

            if self.ometiff_cfg is not None:
                writer = OMETIFFWriter(fpath=out_vol_path,
                                       array=vol_array,
                                       metadata=self.ometiff_cfg,
                                       dimension_order=self.ometiff_cfg["DimensionOrder"])
                writer.write()
            else:
                writer = OMETIFFWriter(fpath=out_vol_path,
                                       array=vol_array,
                                       metadata={},
                                       dimension_order="CZYX")
                writer.write()
            descriptor_yml_dict[crop_name] = pivot_yml_dict

            pivot_yml_path = crop_path_dir.joinpath(crop_name + ".yml")
            _save_yaml(pivot_yml_dict, pivot_yml_path)

        descriptor_yml_path = self.out_path.joinpath("crops.yml")
        _save_yaml(descriptor_yml_dict, descriptor_yml_path)


def _load_yaml(yml_path):
    """ load a yml file and return a dict"""
    with yml_path.open(mode="r") as rfile:
        try:
            yml_dict = yaml.safe_load(rfile)
        except yaml.YAMLError as exc:
            print(exc)
            raise ValueError(exc)

    return yml_dict


def _save_yaml(data, yml_path):
    """save a dict-like to yml"""
    with yml_path.open(mode="w") as dumpfile:
        yaml.dump(data, dumpfile,
                  default_flow_style=None,
                  line_break="\n")


if __name__ == "__main__":
    main()
