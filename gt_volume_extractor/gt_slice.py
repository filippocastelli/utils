from pathlib import Path
from argparse import ArgumentParser
import logging


from gt_patch_extractor_multi import GTPatchExtractor


def main():
    parser = ArgumentParser(description="Extracts gray and white matter samples from slice")

    parser.add_argument("-d", "--slice_dir", type=str, required=True, help="Directory containing the slice")
    parser.add_argument("--shape", type=int, nargs=3, help="Crop shape", default=[82, 132, 132])
    parser.add_argument("--sample_bounds", type=int, nargs=6,
                        help="Sample bounds [low_z, low_y, low_x, high_z, high_y, high_x",
                        default=[80, 0, 0, 180, 12258, 12612])
    parser.add_argument("--padding", type=int, help="number of padding pixels", default=16)
    parser.add_argument("--load_mode", type=str, help="Load mode", default="zetastitcher")
    parser.add_argument("--n_gray_samples", type=int, help="Number of gray matter samples", default=50)
    parser.add_argument("--n_white_samples", type=int, help="Number of white matter samples", default=50)
    parser.add_argument("--sampling_mode", type=str, help="Sampling mode", default="center")

    args = parser.parse_args()

    # get the directories
    slice_dir = Path(args.slice_dir)

    output_dir = slice_dir.joinpath("out")
    assert not output_dir.exists(), "Output directory already exists"
    output_dir.mkdir()

    gray_out = output_dir.joinpath("gray")
    gray_out.mkdir()
    white_out = output_dir.joinpath("white")
    white_out.mkdir()

    # get the slice
    slice_path = slice_dir.joinpath("fused.tif")
    assert slice_path.exists(), "Slice does not exist"
    white_matter_mask_path = slice_dir.joinpath("white_matter_mask.tif")
    assert white_matter_mask_path.exists(), "White matter mask does not exist"
    gray_matter_mask_path = slice_dir.joinpath("gray_matter_mask.tif")
    assert gray_matter_mask_path.exists(), "Gray matter mask does not exist"

    # get the sample bounds
    sample_bounds = args.sample_bounds
    low_bound = sample_bounds[:3]
    high_bound = sample_bounds[3:]
    sample_bounds_tuple = (low_bound, high_bound)

    # get the number of samples
    n_white_samples = args.n_white_samples
    n_gray_samples = args.n_gray_samples

    # get the sampling mode
    sample_mode = args.sampling_mode
    assert sample_mode in ["center", "pivot"], "Invalid sampling mode"

    # get the crop shape
    crop_shape = args.shape

    # get the padding
    padding = args.padding

    # get the load mode
    load_mode = args.load_mode
    assert load_mode in ["zetastitcher", "whole_stack", "zarr"], "Invalid load mode"

    logging.log(logging.INFO, "Extracting white matter samples")
    # white matter patch extractor
    white_matter_extractor = GTPatchExtractor(
        input_file_path=slice_path,
        mask_file_path=white_matter_mask_path,
        output_path=white_out,
        sample_bounds=sample_bounds_tuple,
        crop_shape=crop_shape,
        n_extractions=n_white_samples,
        padding=padding,
        stack_load_mode=load_mode,
        sample_mode=sample_mode)

    logging.log(logging.INFO, "Extracting gray matter samples")
    # gray matter patch extractor
    gray_matter_extractor = GTPatchExtractor(
        input_file_path=slice_path,
        mask_file_path=gray_matter_mask_path,
        output_path=gray_out,
        sample_bounds=sample_bounds_tuple,
        crop_shape=crop_shape,
        n_extractions=n_gray_samples,
        padding=padding,
        stack_load_mode=load_mode,
        sample_mode=sample_mode)


if __name__ == "__main__":
    main()
