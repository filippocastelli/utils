from pathlib import Path
from argparse import ArgumentParser

from skimage import io
import numpy as np
from tqdm import tqdm


def main():
    parser = ArgumentParser()

    parser.add_argument(
        "-d", "--directory",
        dest="input_directory",
        type=str,
        default="/mnt/NASone3/NIH/Misure_I46/test_slice_300-500um/ground_truth/downscaled_3_3um/GROUND_TRUTH",
        action="store",
        help="input directory")

    args = parser.parse_args()
    in_path = Path(args.input_directory)

    crop_dirs = list(in_path.glob("*"))
    crop_dirs = [dir_path for dir_path in crop_dirs if dir_path.is_dir() and "crop_" in str(dir_path.name)]

    for crop_dir in tqdm(crop_dirs):
        crop_tiffs = list(crop_dir.glob("*.tiff"))
        crop_tiffs = [fpath for fpath in crop_tiffs if "ome" not in fpath.name]
        for crop_tiff in tqdm(crop_tiffs):
            divide_imgs(crop_tiff)


def divide_imgs(img_path: Path):
    img = io.imread(str(img_path))
    img_shape = np.array(img.shape)
    half_shape = img_shape // 2

    slice_y_lower = slice(0, half_shape[1])
    slice_x_lower = slice(0, half_shape[2])

    slice_y_higher = slice(half_shape[1], None)
    slice_x_higher = slice(half_shape[2], None)

    img_a = img[:, slice_y_lower, slice_x_lower]
    img_b = img[:, slice_y_lower, slice_x_higher]
    img_c = img[:, slice_y_higher, slice_x_lower]
    img_d = img[:, slice_y_higher, slice_x_higher]

    # img_a = img[0:half_shape[0], 0:half_shape[1]]
    # img_b = img[0:half_shape[0], half_shape[1]:]
    # img_c = img[half_shape[0], half_shape[1]:]
    # img_d = img[slice_y_higher, slice_x_higher]

    out_path = img_path.parent.joinpath(img_path.stem)
    out_path.mkdir(exist_ok=True)

    images = {
        "A": img_a,
        "B": img_b,
        "C": img_c,
        "D": img_d
    }

    for key, img in images.items():
        out_img_path = out_path.joinpath(img_path.stem + "_" + key + img_path.suffix)
        io.imsave(str(out_img_path), img, check_contrast=False)


if __name__ == "__main__":
    main()
