from pathlib import Path
from argparse import ArgumentParser
from typing import Tuple, List, Union
import yaml
import logging
from datetime import datetime

import tifffile
import numpy as np
from tqdm import tqdm
import zarr

from zetastitcher import InputFile

# set up logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def read_yml_config(yml_path: Path) -> dict:
    """
    Reads a yaml config file and returns a dictionary.
    """
    with yml_path.open(mode="r") as infile:
        yml_dict = yaml.safe_load(infile)
    return yml_dict


def main():
    parser = ArgumentParser()
    parser.add_argument("-c",
                        "--config",
                        type=str,
                        required=True,
                        help="Path to the config file.")
    parser.add_argument("-o",
                        "--output",
                        type=str,
                        required=True,
                        help="Path to the output directory.")
    args = parser.parse_args()
    config_path = Path(args.config)
    output_path = Path(args.output)

    config_dict = read_yml_config(config_path)
    output_path.mkdir(parents=True, exist_ok=True)

    GTPatchExtractor(
        input_file_path=Path(config_dict["input_file_path"]),
        mask_file_path=Path(config_dict["mask_file_path"]),
        output_path=output_path,
        sample_bounds=config_dict["sample_bounds"],
        crop_shape=config_dict["crop_shape"],
        n_extractions=config_dict["n_extractions"],
        padding=config_dict["padding"],
        stack_load_mode=config_dict["stack_load_mode"],
        sample_mode=config_dict["sample_mode"]
    )


class GTPatchExtractor:

    def __init__(self,
                 input_file_path: Path,
                 mask_file_path: Union[Path, None],
                 output_path: Path,
                 sample_bounds: Union[None, tuple],
                 crop_shape: tuple,
                 n_extractions: int,
                 padding: int,
                 stack_load_mode: str = "whole_stack",
                 sample_mode: str = "center"):

        self.input_file_path = input_file_path
        self.mask_file_path = mask_file_path
        self.output_path = output_path
        self.sample_bounds = sample_bounds
        self.crop_shape = np.array(crop_shape)
        self.n_extractions = n_extractions
        self.padding = padding

        self.stack_load_mode = stack_load_mode
        assert self.stack_load_mode in ["whole_stack", "zarr", "zetastitcher"], "Invalid stack load mode."

        self.sample_mode = sample_mode
        assert self.sample_mode in ["center", "pivot"], "Invalid sample mode."

        assert all(self.crop_shape % 2 == 0), "The crop shape must be even in all dimensions."

        self.inpf = InputFile(str(self.input_file_path))
        self.inpf_shape = self.inpf.shape

        self.binary_mask, self.z_bounds = self._load_binary_bounds(mask_path=self.mask_file_path)

        if self.stack_load_mode == "whole_stack":
            self.stack = self.inpf.whole()

        self.patch_bounds = self._get_patch_bounds(
            n_extractions=self.n_extractions)

        self.save_crops()
        self.make_summary_yml()

    def _load_binary_bounds(self, mask_path: Union[Path, None]) -> (np.ndarray, (int, int)):
        if mask_path is not None:
            mask_arr = tifffile.imread(str(mask_path))
            assert mask_arr.shape == self.inpf_shape[1:], "Mask shape does not match input file shape."
        else:
            mask_arr = np.ones_like(self.inpf_shape[1:])
        # binarize the 8bit mask
        mask_arr = mask_arr > 0
        shape_array = np.array(self.crop_shape)

        if self.sample_bounds is None:

            if self.sample_mode == "pivot":
                # pivot extraction
                lower_bound = np.array((0, 0, 0))
                higher_bound = np.array(self.inpf_shape) - shape_array

            elif self.sample_mode == "center":
                # center extraction
                lower_bound = shape_array // 2
                higher_bound = np.array(self.inpf_shape) - (shape_array // 2)
            else:
                raise ValueError("Invalid sample mode.")
        else:

            if self.sample_mode == "pivot":
                # pivot extraction
                lower_bound = np.array(self.sample_bounds[0])
                higher_bound = np.array(self.sample_bounds[1]) - shape_array
            elif self.sample_mode == "center":
                # center extraction
                lower_bound = np.array(self.sample_bounds[0]) + (shape_array // 2)
                higher_bound = np.array(self.sample_bounds[1]) - (shape_array // 2)
            else:
                raise ValueError("Invalid sample mode.")

        assert all(higher_bound > 0), "The higher bound is smaller than the lower bound."

        # set the mask to false outside the sample bounds
        mask_arr[0:lower_bound[1], :] = False
        mask_arr[higher_bound[1]:, :] = False

        mask_arr[:, 0:lower_bound[2]] = False
        mask_arr[:, higher_bound[2]:] = False

        return mask_arr, (lower_bound[0], higher_bound[0])

    def _get_patch_bounds(self,
                          n_extractions: int,
                          ) -> List[Tuple[slice, slice, slice]]:
        """
        Returns a list of slices that can be used to extract patches.
        """
        patch_slices = []
        shape_array = np.array(self.crop_shape)

        for idx in range(n_extractions):

            extract_z = np.random.randint(self.z_bounds[0], self.z_bounds[1])
            y, x = np.where(self.binary_mask)
            idx = np.random.randint(0, len(x))
            extract_x = int(x[idx])
            extract_y = int(y[idx])

            if self.sample_mode == "pivot":
                patch_slices.append((slice(extract_z, extract_z + shape_array[0], 1),
                                     slice(extract_y, extract_y + shape_array[1], 1),
                                     slice(extract_x, extract_x + shape_array[2], 1)))
            elif self.sample_mode == "center":
                patch_slices.append((slice(extract_z - shape_array[0] // 2, extract_z + shape_array[0] // 2, 1),
                                     slice(extract_y - shape_array[1] // 2, extract_y + shape_array[1] // 2, 1),
                                     slice(extract_x - shape_array[2] // 2, extract_x + shape_array[2] // 2, 1)))
            else:
                raise ValueError("Invalid sample mode.")

        return patch_slices

    def save_crops(self):
        """
        Saves the patches to the output directory.
        """
        for idx, patch_slice in enumerate(tqdm(self.patch_bounds)):
            if self.stack_load_mode == "zarr":
                with tifffile.imread(str(self.input_file_path), aszarr=True) as store:
                    za = zarr.open(store, mode="r")
                    # patch = za[patch_slice]
                    patch = za[patch_slice[0], patch_slice[1], patch_slice[2]]
            elif self.stack_load_mode == "zetastitcher":
                patch = self.inpf[patch_slice[0], patch_slice[1], patch_slice[2]]
            elif self.stack_load_mode == "whole_stack":
                patch = self.stack[patch_slice[0], patch_slice[1], patch_slice[2]]
            else:
                raise ValueError("Invalid stack load mode.")

            patch_pivot = (patch_slice[0].start,
                           patch_slice[1].start,
                           patch_slice[2].start)

            assert patch.shape == tuple(self.crop_shape), "The patch shape is not the same as the crop shape."

            patch_pivot_str = "_".join(map(str, patch_pivot))
            patch_dim_str = "_".join(map(str, self.crop_shape))

            patch_output_path = self.output_path.joinpath(str(idx))
            patch_output_path.mkdir(parents=True, exist_ok=True)

            tifffile.imsave(str(patch_output_path.joinpath(f"pos_{patch_pivot_str}_dim_{patch_dim_str}.tif")), patch)

            if self.padding is not None:
                eroded_patch = patch[self.padding:-self.padding, self.padding:-self.padding, self.padding:-self.padding]
                eroded_patch_shape = eroded_patch.shape
                patch_dim_str = "_".join(map(str, eroded_patch_shape))
                tifffile.imsave(str(patch_output_path.joinpath(f"pos_{patch_pivot_str}_dim_{patch_dim_str}.tif")),
                                eroded_patch)

            self.make_patch_yml(patch_pivot=patch_pivot, patch_output_path=patch_output_path)

    def make_patch_yml(self,
                       patch_pivot: tuple,
                       patch_output_path: Path):
        """
        Creates a yml file that contains the patch information.
        """
        with open(str(patch_output_path.joinpath("patch_info.yml")), "w") as f:
            yaml.dump(
                {"patch_pivot": self._prettify_list(patch_pivot),
                 "patch_shape": self._prettify_list(list(self.crop_shape)),
                 "padding": self.padding if self.padding is not None else 0,
                 "sample_lower_bound": self._prettify_list(self.sample_bounds[0]),
                 "sample_upper_bound": self._prettify_list(self.sample_bounds[1]),
                 "stack_load_mode": self.stack_load_mode,
                 "creation_date": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                 "input_file_path": str(self.input_file_path),
                 "mask_file_path": str(self.mask_file_path)},
                f,
                default_flow_style=False)

    @staticmethod
    def _prettify_list(input_list: Union[list, tuple]) -> list:
        """
        Prettifies a list of integers.
        """
        return [str(i) for i in input_list]

    def make_summary_yml(self):
        """
        Creates a summary yml file that contains the summary information.
        """
        with open(str(self.output_path.joinpath("summary.yml")), "w") as f:
            pivot_list = [[patch[0].start, patch[1].start, patch[2].start] for patch in self.patch_bounds]
            pivot_list_pretty = self._prettify_list(pivot_list)
            yaml.dump(
                {"sample_lower_bound": self._prettify_list(self.sample_bounds[0]),
                 "sample_upper_bound": self._prettify_list(self.sample_bounds[1]),
                 "stack_load_mode": self.stack_load_mode,
                 "creation_date": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                 "input_file_path": str(self.input_file_path),
                 "mask_file_path": str(self.mask_file_path) if self.mask_file_path is not None else None,
                 "pivots": pivot_list_pretty,
                 "crop_shape": self._prettify_list(list(self.crop_shape))},
                f,
                default_flow_style=False)


if __name__ == "__main__":
    main()
