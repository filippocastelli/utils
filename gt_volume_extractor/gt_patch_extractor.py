from pathlib import Path
from argparse import ArgumentParser

import yaml
import tifffile
import numpy as np
from tqdm import tqdm

from zetastitcher import VirtualFusedVolume


def read_yml_config(yml_path: Path) -> dict:
    """
    Reads a yaml config file and returns a dictionary.
    """
    with yml_path.open(mode="r") as infile:
        yml_dict = yaml.safe_load(infile)
    return yml_dict


def main():
    parser = ArgumentParser()
    parser.add_argument(
        "-c", "--cfg",
        type=str,
        dest="config_path",
        help="cfg path",
        action="store",
        default=None
    )

    parser.add_argument(
        "-o", "--out",
        type=str,
        dest="out_path",
        help="out path",
        action="store",
        default=None
    )
    args = parser.parse_args()
    config_path = Path(args.config_path)
    out_path = Path(args.out_path)

    config_dict = read_yml_config(config_path)

    vpe = VolumePatchExtractor(
        stitch_file_path=config_dict["stitch_file_path"],
        out_path=out_path,
        bounds=config_dict["bounds"] if "bounds" in config_dict else None,
        crop_shapes=config_dict["crop_shapes"],
        n_pivots=config_dict["n_pivots"]
    )


class VolumePatchExtractor:

    def __init__(self,
                 stitch_file_path: Path,
                 out_path: Path,
                 bounds: None,
                 crop_shapes: tuple,
                 n_pivots: int):

        self.stitch_file_path = stitch_file_path
        self.out_path = out_path
        self.bounds = bounds
        self.crop_shapes = crop_shapes
        self.n_pivots = n_pivots

        self.vfv = VirtualFusedVolume(str(self.stitch_file_path))

        self.pivot_list = self.get_pivot_list(crop_shapes=self.crop_shapes,
                                              n_pivots=self.n_pivots)

        self.get_volumes(pivot_list=self.pivot_list)

    def get_pivot_list(self,
                       crop_shapes: tuple,
                       n_pivots: int) -> list:

        shapes_array = np.array(crop_shapes)
        max_shape = np.max(shapes_array, axis=0)

        if self.bounds is None:
            lower_bound = np.array((0, 0, 0))
            higher_bound = np.array(self.vfv.shape) - max_shape
        else:
            lower_bound = np.array(self.bounds[0])
            higher_bound = np.array(self.bounds[1]) - max_shape

        assert all(higher_bound > 0), "can't place crops in selected bounds"

        pivot_list = []
        for idx in range(n_pivots):
            rand_z = np.random.randint(low=lower_bound[0], high=higher_bound[0])
            rand_y = np.random.randint(low=lower_bound[1], high=higher_bound[1])
            rand_x = np.random.randint(low=lower_bound[2], high=higher_bound[2])

            pivot_list.append((rand_z, rand_y, rand_x))

        return pivot_list

    def get_volumes(self, pivot_list: list):

        for idx, pivot in enumerate(tqdm(pivot_list)):
            pivot_str = "_".join(map(str, pivot))
            pivot_out_path = self.out_path.joinpath("pivot_" + pivot_str)
            pivot_out_path.mkdir(exist_ok=True, parents=True)

            for crop_shape in tqdm(self.crop_shapes):
                substack = self.vfv[
                           pivot[0]:pivot[0] + crop_shape[0],
                           pivot[1]:pivot[1] + crop_shape[1],
                           pivot[2]:pivot[2] + crop_shape[2]]

                crop_shape_str = "_".join(map(str, crop_shape))
                crop_shape_out_path = pivot_out_path.joinpath(crop_shape_str + ".tiff")
                tifffile.imwrite(str(crop_shape_out_path), data=substack)


if __name__ == "__main__":
    main()