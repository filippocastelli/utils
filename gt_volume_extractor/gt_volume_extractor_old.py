from argparse import ArgumentParser, RawTextHelpFormatter
from pathlib import Path
import shutil
import logging
import yaml
import datetime
from collections import OrderedDict

import numpy as np
import tifffile
from tqdm import tqdm

from zetastitcher import VirtualFusedVolume

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def setup_yaml():
  """add represented for OrderedDicts to pyYAML
  see https://stackoverflow.com/a/8661021 """
  represent_dict_order = lambda self, data:  self.represent_mapping('tag:yaml.org,2002:map', data.items())
  yaml.add_representer(OrderedDict, represent_dict_order)
   
setup_yaml()


def main():
    description_txt = "Extract GT slices from a stitched volume \n\
        the configuration YAML should be formatted as \n\n\n\
        \t\t- conf.yml - \n\
        \tstitch_file_path: path_to_stitch.yml \n\
        \tcrop_shape: [crop_z, crop_y, crop_x] \n\
        \trandom: false/true \n\
        \tn_random_crops: n_crops \n\
        \tpositions: \n\
        \t\t - [z_0, y_0, x_0] \n\
        \t\t - [z_1, y_1, x_1] \n\n\
        \twhere \n\n\
        \t stitch_file_path: path to zetastitcher stitch.yml stitch file \n\
        \t crop_shape: shape of the crop selection \n\
        \t random: enables/disables random croppping mode \n\
        \t n_random_crops: if random is true, extracts n_random_crops crops \n\
        \t positions: list of ZYX format coordinates in stitched space \n"
        
    parser = ArgumentParser(
        description=description_txt,
        epilog="Author: Filippo Maria Castelli <castelli@lens.unifi.it>",
        formatter_class=RawTextHelpFormatter)
    
    parser.add_argument(
        "--conf",
        type=str,
        action="store",
        dest="conf_yml_path",
        help="Configuration yml path")
    
    parser.add_argument(
        "--out",
        type=str,
        action="store",
        dest="out_path",
        help="Out path")
    
    # other args
    args = parser.parse_args()
    
    out_path = Path(args.out_path) if args.out_path is not None else Path.cwd().joinpath("out")
    conf_yml_path = Path(args.conf_yml_path)  if args.conf_yml_path is not None else None
    
    conf_dict = _load_yaml(conf_yml_path)
    
    vc = VolumeCropper(conf_dict, out_path)
    
def _load_yaml(yml_path):
    """ load a yml file and return a dict"""
    with yml_path.open(mode="r") as rfile:
        try:
            yml_dict = yaml.safe_load(rfile)
        except yaml.YAMLError as exc:
            print(exc)
            raise ValueError(exc)
            
    return yml_dict

class VolumeCropper:
    def __init__(self,
                 config,
                 out_path):
        
        self.config = config
        self.out_path = Path(out_path)
        self.out_path.mkdir(parents=True, exist_ok=True)
        self.crop_shape = config["crop_shape"]
        self.stitch_file_path = Path(config["stitch_file_path"])
        
        self.stitch_file_paths_dict = {channel: fpath for channel, fpath in config["stitch_files_paths"]}
        self.offsets = config["offsets"]
        
        self.channels = list(self.stitch_file_paths_dict.keys())

        self.random_selection = config["random"]
        assert config["random"] in  [True, False], "invalid mode {}".format(config["random"])
        self.n_random_crops = config["n_random_crops"]
        
        self.vfv_dict = {}
        self._get_virtual_volumes()
        # self.vfv = VirtualFusedVolume(str(self.stitch_file_path))
        self.vol_shape = self.vfv.shape
        self.positions = self._get_pos_list()
        
        # save crops
        self.descr_dict = {}
        self.counter = 0
        self.make_crops()
        
        # save yml descriptor
        self.yml_descriptor_path = self.out_path.joinpath("crops.yml")
        self._save_yaml(OrderedDict(self.descr_dict), self.yml_descriptor_path)
        
        # backup stitch file
        self.stitch_copy_path = self.out_path.joinpath("stitch.yml")
        shutil.copy(str(self.stitch_file_path), str(self.stitch_copy_path))
        
        
    def _get_pos_list(self):
        if not self.random_selection:
            pos_list = self.config["positions"]
            assert len(pos_list) > 1, "no predefined positions, enable random mode?"
            
            return pos_list
        else:
            logging.info("Calculating positions...")
            return self._get_random_pivot_list()

    def _get_virtual_volumes(self):
        for channel, fpath in self.stitch_file_paths_dict.items():
            self.vfv_dict[channel] = VirtualFusedVolume(str(fpath))
            
            
            
    @staticmethod
    def _get_random_pivot(bound_min, bound_max):
        rand_z = np.random.randint(bound_min[0], bound_max[0])
        rand_y = np.random.randint(bound_min[1], bound_max[1])
        rand_x = np.random.randint(bound_min[2], bound_max[2])
        
        return np.array([rand_z, rand_y, rand_x])
    
    def _get_slices(self, pivot, offset=[0,0,0]):
        
        offset_pivot = np.array(pivot) + np.array(offset)
        z_slice = slice(offset_pivot[0] , offset_pivot[0] + self.crop_shape[0], 1)
        y_slice = slice(offset_pivot[1], offset_pivot[1] + self.crop_shape[1], 1)
        x_slice = slice(offset_pivot[2], offset_pivot[2] + self.crop_shape[2], 1)
        
        return (z_slice, y_slice, x_slice)
        
    def _get_slices_dict(self, pivot):
        slices_dict = {}
        
        for channel in self.channels:
            offset = self.offsets[channel] if channel in self.offsets else [0,0,0]
            channel_slices = self._get_slices(pivot, offset)
            
            slices_dict[channel] = channel_slices
            
        return slices_dict
            
        
        
    def _get_random_pivot_list(self):
        
        bound_min = np.array([0,0,0])
        bound_max = np.array(self.vol_shape) - np.array(self.crop_shape)
        
        pivots = []
        
        for idx in range(self.n_random_crops):
            pass_condition = False
            pivot = self._get_random_pivot(bound_min, bound_max)
            counter = 0
            while not pass_condition:
                
                slices = self._get_slices(pivot)
                peek = self.vfv.peek[slices[0], slices[1], slices[2]]
                
                if len(peek) == 1:
                    #only way to get out of the loop is here
                    # if sizes match then pivot_condition = True
                    if self._is_valid_shape(pivot):
                        pivots.append(pivot)
                        pass_condition = True
                        counter = 0
                    else:
                        pivot = self._get_random_pivot(bound_min, bound_max)
                        pass_condition = False
                        counter = 0
                elif len(peek) == 2:
                    pivot = self._move_pivot(pivot)
                    pass_condition = False
                    counter+=1
                    if counter > 5:
                        pivot = self._get_random_pivot(bound_min, bound_max)
                        pass_condition = False
                        counter = 0
                elif len(peek) > 2:
                    pivot = self._get_random_pivot(bound_min, bound_max)
                    pass_condition = False
        return pivots
    
    
    def _get_peek_slices(self, pivot):
        slices = self._get_slices(pivot)
        peek = self.vfv.peek[slices[0], slices[1], slices[2]]
        return [entry[1] for entry in peek]
    
    
    def _get_peek(self, pivot):
        slices = self._get_slices(pivot)
        peek = self.vfv.peek[slices[0], slices[1], slices[2]]
        return peek
    
    
    def _is_valid_shape(self, pivot):
        peek_slices = self._get_peek_slices(pivot)
        diff_array = np.array(self._slice_list_to_diffs(peek_slices))
        diff_array_sum = np.sum(diff_array, axis=0)
        
        return (diff_array_sum == np.array(self.crop_shape)).all()
    
    def _move_pivot(self, pivot):
        # peek_slices = self._get_peek_slices(pivot)
        # peek_start_stop = [self._peek_slices_to_startstop_list(peek_slice) for peek_slice in peek_slices]
        # peek_start_stop_npy = np.array(peek_start_stop)

        peek_slices = self._get_peek_slices(pivot)
        diff_array = np.array(self._slice_list_to_diffs(peek_slices))
        min_diff_loc = np.unravel_index(diff_array.argmin(), diff_array.shape)
        
        axis = min_diff_loc[1]
        direction = +1 if min_diff_loc[0] == 0 else -1
        
        delta = np.array([0,0,0])
        delta[axis] = direction * self.crop_shape[axis]
        
        return pivot + delta
    
    @classmethod
    def _peek_slices_to_startstop_list(cls, peek_slice):
        return [cls._slice_to_list(sslice) for sslice in peek_slice]
        
    @staticmethod
    def _list_equal(first_list, second_list):
        for idx, elem in enumerate(first_list):
            if elem != second_list[idx]:
                return False
        return True
    
    @staticmethod
    def _slice_to_list(sslice):
        return [sslice.start, sslice.stop]
    
    @staticmethod
    def _slice_list_to_diffs(peek_slices):
        diff_list = []
        for peek_slice in peek_slices:
            diff_list.append([sslice.stop - sslice.start for sslice in peek_slice])
        return diff_list
            
    
    def make_crops(self):
        for crop_pivot in tqdm(self.positions):
            crop = self._get_volume_crop(crop_pivot)
            self._save_crop(crop, crop_pivot)
            
    def _get_volume_crop(self, pivot):
        slices = self._get_slices(pivot)
        volume_crop = self.vfv[slices[0],slices[1],slices[2]]
        self._check_volume_shape(volume_crop.shape)
        
        return volume_crop
    def _check_volume_shape(self, shape):
        is_correct_shape = (np.array(self.crop_shape) == np.array(shape)).all()
        
        if not is_correct_shape:
            raise ValueError("incorrect volume shape, shouldn't happen tho")
        
    def _save_crop(self, crop, pivot):
        crop_name = "crop_{}".format(format(self.counter, "05d"))
        self.counter += 1
        crop_path_dir = self.out_path.joinpath(crop_name)
        crop_path_dir.mkdir(exist_ok=True, parents=True)
        crop_path_tiff = crop_path_dir.joinpath("crop.tiff")
        tifffile.imsave(file=crop_path_tiff, data=crop)
        
        acquisition_filename, slices = self._get_peek(pivot)[0]
        start_stop_list = self._peek_slices_to_startstop_list(slices)
        
        acquisition_coords = [item[0] for item in start_stop_list]
        
        yml_descriptor_dict = {
            "stitch_file_path": str(self.stitch_file_path),
            "acquisition_path": acquisition_filename, 
            "stitch_coords": pivot.tolist(),
            "acquisition_coords": np.array(acquisition_coords).tolist(), 
            "crop_shape": np.array(self.crop_shape).tolist(),
            "output_path": str(crop_path_dir),
            "timestamp": str(datetime.datetime.now())}
        
        yml_descriptor_path = crop_path_dir.joinpath("crop.yml")
        
        self.descr_dict[crop_name] = yml_descriptor_dict
        self._save_yaml(OrderedDict(yml_descriptor_dict), yml_descriptor_path)
        
    
    @staticmethod
    def _save_yaml(data, yml_path):
        """save a dict-like to yml"""
        with yml_path.open(mode="w") as dumpfile:
            yaml.dump(data, dumpfile,
                      default_flow_style=None,
                      line_break="\n")
        
    
        
        
        
if __name__ == "__main__":
    main()
