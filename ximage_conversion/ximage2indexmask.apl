name: "ximage2indexmask"

stages {
  name: "src"
  type: SOURCE_IMAGE
  source_param {
    path: "$input_path"
    ximage_meta: SHAPES_FLATTEN
    classes_num: 256
  }
}

stages {
  name: "class_filter"
  type: CLASS_FILTER
  input: "src"
  class_filter_param {
    default_policy: CLASS_REJECT
    class_include: "$classes"
    value_threshold: 0.0
  }
}

stages {
  name: "index_masks"
  type: INDEX_MASK
  input: "class_filter"
  index_mask_param {
    background: 255
  }
}

