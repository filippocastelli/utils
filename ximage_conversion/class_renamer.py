import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# from uuid import UUID, uuid4
from pathlib import Path
from argparse import ArgumentParser
import sys

import numpy as np

sys.path.append('/home/phil/repos/ximage')
from ximage_core import XImageMeta


SUPPORTED_EXTENSIONS = ["png", "tif", "tiff", "jpg", "jpeg"]
            
class ClassChanger:
    
    def __init__(self,
                 stack_path,
                 from_class,
                 to_class):
        
        self.stack_path = Path(stack_path)
        self.frame_paths = self._find_files(self.stack_path)
        
        self.from_class = from_class
        self.to_class = to_class
    
    @staticmethod
    def _find_files(search_path):
        return sorted([fname for fname in search_path.glob("*.*") if (fname.is_file() and fname.suffix.split(".")[1].lower() in SUPPORTED_EXTENSIONS)])
    
    def change_classes(self):
        for idx, frame_fpath in enumerate(self.frame_paths):
            self._change_class(frame_fpath, self.from_class, self.to_class)
        
    @staticmethod
    def _change_class(frame_path, from_class, to_class):
        img_meta  = XImageMeta.read(frame_path)
        
        items = img_meta.items
        
        for item_idx, item in enumerate(items):
            for blob_idx, blob in enumerate(item.blobs):
                blob_id = blob.get_classid()
                if blob_id == from_class:
                    logging.info("Changing class of item {} from {} to {}".format(item.uuid, str(from_class), str(to_class)))
                    new_value_array = np.zeros_like(blob.values)
                    new_value_array[to_class] = 1.
                    img_meta.items[item_idx].blobs[blob_idx].values = new_value_array
        
        img_meta.write(frame_path)
                    

def main():
    parser = ArgumentParser(description="Change class for all items in a givben class in stack",
                            epilog="Autthor: Filippo Maria Castelli <castelli@lens.unifi.it>")
    parser.add_argument(
        "-d",
        "--input_dir",
        action="store",
        type=str,
        dest="img_dir_path",
        default="/home/phil/stack",
        help="Specify stack location",
        )
    
    parser.add_argument(
        "--from",
        action="store",
        type=int,
        dest="from_class",
        default=None,
        help="id of class you want to change")
    
    parser.add_argument(
        "--to",
        action="store",
        type=int,
        dest="to_class",
        default=None,
        help="id of class you want to change to")
    
    args = parser.parse_args()
    stack_path = Path(args.img_dir_path)
    
    from_class = args.from_class
    to_class = args.to_class
    
    cc = ClassChanger(stack_path, from_class, to_class)
    cc.change_classes()

if __name__ == "__main__":
    main()

