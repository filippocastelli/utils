import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# from uuid import UUID, uuid4
from pathlib import Path
from argparse import ArgumentParser
import sys
import yaml
from uuid import uuid4, UUID
sys.path.append('/home/phil/repos/ximage')
from ximage_core import XImageMeta

SUPPORTED_EXTENSIONS = ["png", "tif", "tiff", "jpg", "jpeg"]

class UUIDDivider:
    
    def __init__(self,
                 stack_path,
                 conf_path):
        
        self.stack_path = Path(stack_path)
        self.stack_fpaths = self._find_files(self.stack_path)
        self.conf_path = Path(conf_path)
        self._parse_cfg()
        
    @staticmethod
    def _load_yml(yml_path):
        """ load a yml file and return a dict"""
        with yml_path.open(mode="r") as rfile:
            try:
                yml_dict = yaml.safe_load(rfile)
            except yaml.YAMLError as exc:
                print(exc)
                raise ValueError(exc)
                
        return yml_dict
    
    @staticmethod
    def _find_files(search_path):
        return sorted([fname for fname in search_path.glob("*.*") if (fname.is_file() and fname.suffix.split(".")[1].lower() in SUPPORTED_EXTENSIONS)])
    
    def _parse_cfg(self):
        self.conf_dict = self._load_yml(self.conf_path)
        self.uuid_idx = {UUID(uuid_str): item for uuid_str, item in self.conf_dict.items()}
        self.uuids = list(self.uuid_idx.keys())
        self.uuid_newid = {old_uuid: uuid4() for old_uuid in self.uuids}
        
    def divide_uuids(self):
        for frame_idx, fpath in enumerate(self.stack_fpaths):
            
            meta = XImageMeta.read(fpath)
            items = meta.items
            
            for item_idx, item in enumerate(items):
                item_uuid = item.uuid
                if item_uuid in self.uuids:
                    if frame_idx >= self.uuid_idx[item_uuid]:
                        logging.info("frame {}/{}, changing UUID {} to {}".format(frame_idx, len(self.stack_fpaths), item_uuid, self.uuid_newid[item_uuid]))
                        meta.items[item_idx].uuid = self.uuid_newid[item_uuid]
                        
            meta.write(fpath)
                
                
    @staticmethod
    def _set_meta_uuid(meta, item_idx, new_uuid):
        meta.items[item_idx].uuid = new_uuid
        
def main():
    parser = ArgumentParser(description="Replace UUIDs",
                            epilog="Autthor: Filippo Maria Castelli <castelli@lens.unifi.it>")
    parser.add_argument(
        "-d",
        "--input_dir",
        action="store",
        type=str,
        dest="img_dir_path",
        default="/home/phil/unpack_ximages_prop",
        help="Specify stack location",
        )
    
    parser.add_argument(
        "--yml",
        "-c",
        action="store",
        type=str,
        dest="yml_path",
        default="uuid_conf.yml",
        help="conf file")
    
    
    args = parser.parse_args()
    
    conf_path  = Path(args.yml_path)
    stack_path = Path(args.img_dir_path)
    
    div = UUIDDivider(stack_path, conf_path)
    div.divide_uuids()

if __name__ == "__main__":
    main()
    