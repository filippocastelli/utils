# =============================================================================
# XIMAGE2INDEXMASK
#
#
# Filippo Maria Castelli
# castelli@lens.unifi.it
# =============================================================================
import sys
import time
sys.path.append('/home/phil/repos/ximage')
from argparse import ArgumentParser
import logging
from pathlib import Path
from multiprocessing import Pool, cpu_count
from itertools import product

from skimage import io as skio
import numpy as np
from tqdm import tqdm
import tifffile

from ximage_errors import XImageEmptyXMPError
from ximage_utils import ximage_export

logger = logging.getLogger()
logger.setLevel(logging.INFO)

SUPPORTED_EXT = ["png", "tif", "tiff", "jpg", "jpeg"]
SUPPORTED_EXT_PT = ["."+ ext for ext in SUPPORTED_EXT]

SUPPORTED_MODES = ["single", "multi"]

NO_CLASS = "no_class"

def ximage2mask(fpath, out_path=None, mode="single", invert=False):
    """
    Convert a ximage segmentation to an indexmask using ximage_export()
    Supports "multi" and "single" modes
    
    "single" creates a segmentation image with
    255 - background
    i - segmentation_class number
    
    "multi" creates a dictionary of segmentation images in the format
    {seg_class : mask}
    mask values are 
    255 - background
    0 - foreground
    
    foreground and background in "multi" mode are inverted if invert opt is used
    
    Parameters
    ----------
    fpath : pathlib Path
        input filepath.
    out_path : pathlib Path, optional
        output filepath, if specified saves the results to it. The default is None.
    mode : str, optional
        indexmask mode. The default is "single".
    invert : bool, optional
        invert black/white label format. Default is False.

    Raises
    ------
    NotImplementedError
        if unsupported mode is requested.

    Returns
    -------
    mask : np.ndarray | dict
        array for single mode, dict for multi.

    """
    try:
        mask = ximage_export(fpath, mode=mode, invert=invert)
    except XImageEmptyXMPError:
        img = skio.imread(fpath, plugin="pil")
        background_color = 0 if invert else 255
        mask = np.full(img.shape[:2], background_color, dtype=np.uint8)
        
        if mode == "multi":
            mask = {NO_CLASS: mask}
        
    if out_path:
        if mode == "single":
            skio.imsave(out_path, mask, plugin="pil", check_contrast=False)
        elif mode == "multi":
            for key, item in mask.items():
                filename = out_path.stem
                extension = out_path.suffix                
                out_name = "{}_{}{}".format(filename, key, extension)
                parent = out_path.parent
                out_fpath = parent.joinpath(out_name)
                
                skio.imsave(out_fpath, item, plugin="pil", check_contrast=False)
        else:
            raise NotImplementedError("segmentation mode {} is not implemented".format(mode))
            
    return mask


def ximage2stack(dir_path, multithread=True, out_path=None, mode="single", invert=False):
    """
    map ximage2mask() to every file in a specified directory
    """
    files_to_convert = sorted([file for file in dir_path.glob("*.*") if file.suffix in SUPPORTED_EXT_PT])
    
    
    if len(files_to_convert) == 0:
        logger.error("No image files found")
    
    if multithread:
        print("MULTITHREAD MODE")
        multi_start = time.time()
        pool = Pool(processes=cpu_count())
        ximage2mask_args = list(product(files_to_convert, [out_path], [mode], [invert]))
        masks = pool.starmap(ximage2mask, ximage2mask_args)
        if out_path:
            pool.map()
        multi_end = time.time()
        print("{} threads: {:.3f}s".format(cpu_count(), multi_end-multi_start))
    else:
        print("Single thread mode...")
        cycle_start = time.time()
        masks = []
        for fpath in tqdm(files_to_convert):
            out_fpath = out_path.joinpath(get_sanitized_fname(fpath, "mask")) if out_path else None
            mask = ximage2mask(fpath, out_fpath, mode=mode, invert=invert)
            masks.append(mask)
        cycle_end = time.time()
        print("Single thread: {:.3f}s".format(cycle_end-cycle_start))
        
    return np.array(masks)


def get_sanitized_fname(fpath, add_fix=None, ignore_extension=False):
    """
    get a filename without dots in it

    Parameters
    ----------
    fpath : pathlib Path
        input filepath.
    add_fix : str, optional
        insert a custom string in the filename. The default is None.
    ignore_extension : bool, optional
        Omit file extension

    Returns
    -------
    filename : str
        filename without dots.

    """
    # parent = fpath.parent
    fname = fpath.name
    
    fname_split = fname.split(".")
    
    if len(fname_split) > 2:
        fname_elems = fname_split[:-1]
        fname = "_".join(fname_elems)
        
    extension = fname_split[-1]
    
    if add_fix is not None:
        fname = "{}_{}".format(fname, add_fix)
    
    if ignore_extension:
        return fname
    else:
        return ".".join([fname, extension])

def get_unique_classes(mask_array, remove_null=False):
    """
    get the unique segmentation class names in a list of mask dicts

    Parameters
    ----------
    mask_array : array/list
        array or list of masks in dict format {seg_class : mask}.
    remove_null : bool, optional
        If true removes NO_CLASS from the set. The default is False.

    Returns
    -------
    class_list
        list of unique segmentation class names.

    """
    classes_set = set()
    
    for mask_dict in mask_array:
        classes_set = classes_set.union(set(mask_dict.keys()))
        
    if remove_null:
        classes_set.remove(NO_CLASS)
    
    return list(classes_set)

def masklist2dict(mask_list, invert=False):
    """
    Convert a list of single mask dictionaries to a dictionary of stacked masks.
    
    Parameters
    ----------
    mask_list : list
        mask list in [{seg_class : mask}] format.
    invert : bool, optional
        Invert black/white

    Returns
    -------
    out_dict : dict
        mask stack dictionary in {seg_class : stack} format.

    """
    unique_classes = get_unique_classes(mask_list, remove_null=False)
    shape = mask_list[0][list(mask_list[0].keys())[0]].shape[:2]
    background_color = 0 if invert else 255
    empty_img = np.full(shape, background_color, dtype=np.uint8)
    stacks_per_class = {seg_class: [] for seg_class in unique_classes}      
         
    for mask_dict in mask_list:
        if NO_CLASS in mask_dict.keys():
            for key, item in stacks_per_class.items():
                item.append(empty_img)
        else:
            for key, item in stacks_per_class.items():
                item.append(mask_dict[key])
                
    out_dict = {key : np.array(item) for key, item in stacks_per_class.items()}
    return out_dict

def main():
    parser = ArgumentParser()
    
    parser.add_argument(
        "-d",
        type=str,
        default="/mnt/NASone3/castelli/test.png",
        action="store",
        dest="input_path",
        help="Input file path")
    
    parser.add_argument(
        "-o",
        dest="output_path",
        action="store",
        default=None,
        help="output directory path")
    
    parser.add_argument(
        "-m","--mp",
        dest="multiprocessing",
        action="store_true",
        help="use multiprocessing")
    
    parser.add_argument(
        "-s","--stack",
        dest="stack",
        action="store_true",
        help="save as TIFF stack")
    
    parser.add_argument(
        "--mode",
        dest="mode",
        type=str,
        action="store",
        default="single",
        help="Indexmask mode, single or multi")
    
    parser.add_argument(
        "--save_slices",
        dest="save_slices",
        action="store_true",
        default=False,
        help="Save single slices")
	
    parser.add_argument(
        "-i", "--invert",
        dest="invert",
        action="store_true",
        help="invert indexmask colorscheme")
    
    args = parser.parse_args()
    input_fpath = Path(args.input_path)
    
    mode = args.mode
    invert = args.invert

    if mode not in SUPPORTED_MODES:
        logger.error("invalid mode {}".format(mode))
        
    out_path = Path(args.output_path) if args.output_path is not None else input_fpath.joinpath("masks")
    out_path.mkdir(exist_ok=True, parents=True)
    
    # if directory scan every item and apply ximage2indexmask
    if input_fpath.is_dir():
        slices_path = out_path if args.save_slices else None
        masks = ximage2stack(input_fpath, args.multiprocessing, mode=mode, out_path=slices_path, invert=invert)
        
        # if output stack is requested create stacks
        if args.stack:
            # only in TIFF format
            if type(masks[0]) is dict: # "multi" segmentation mode has been used
            # mask results are in dictionary format
                mask_dict = masklist2dict(masks)
                for seg_class, stack in mask_dict.items():
                    out_stack_path = out_path.joinpath("{}_{}.tif".format(out_path.name, seg_class))
                    tifffile.imwrite(out_stack_path, stack)
            else: # "single" segmentation mode has been used, results are a list
                mask_stack = np.array(masks)
                out_stack_path = out_path.joinpath(out_path.name + ".tif")
                tifffile.imwrite(out_stack_path, mask_stack)
            
    # if fpath points to a single file just apply x
    elif input_fpath.is_file():
        if out_path.is_dir():
            out_name = get_sanitized_fname(input_fpath, add_fix="mask")
            out_fpath = out_path.joinpath(out_name)
        else:
            out_fpath = out_path
        ximage2mask(input_fpath,out_path=out_fpath)
    else:
        logger.error("No input file was provided")

    
if __name__ == "__main__":
    main()

