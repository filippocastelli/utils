import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# from uuid import UUID, uuid4
from pathlib import Path
from argparse import ArgumentParser
import sys

from shapely.geometry import Polygon
from shapely.errors import TopologicalError
sys.path.append('/home/phil/repos/ximage')
from ximage_core import XImageMeta

SUPPORTED_EXTENSIONS = ["png", "tif", "tiff", "jpg", "jpeg"]
            
class UUIDStackPropagator:
    
    def __init__(self,
                 stack_path):
        
        self.stack_path = Path(stack_path)
        self.frame_paths = self._find_files(self.stack_path)
    
    @staticmethod
    def _find_files(search_path):
        return sorted([fname for fname in search_path.glob("*.*") if (fname.is_file() and fname.suffix.split(".")[1].lower() in SUPPORTED_EXTENSIONS)])
    
    def propagate(self):
        for idx, upper_frame_fpath in enumerate(self.frame_paths):
            if idx != 0:
                lower_frame_fpath = self.frame_paths[idx-1]
                self._propagate_frame(upper_frame_fpath, lower_frame_fpath)
    
    
    @staticmethod
    def _lower_overlap(pol, lower_items):
        for item in lower_items:
            for blob in item.blobs:
                lower_pol = Polygon(blob.points)
                try:
                    intersects = pol.intersects(lower_pol)
                except TopologicalError:
                    intersects = False
                if intersects:
                    return item.uuid
        return None
    
    @staticmethod
    def _set_meta_uuid(meta, item_idx, new_uuid):
        meta.items[item_idx].uuid = new_uuid
        
    @classmethod
    def _propagate_frame(cls, upper_fpath, lower_fpath):
        upper_meta = XImageMeta.read(upper_fpath)
        lower_meta = XImageMeta.read(lower_fpath)
        
        upper_items = upper_meta.items
        lower_items = lower_meta.items
        
        
        for idx, upper_item in enumerate(upper_items):
            upper_uuid = upper_item.uuid
            
            for blob in upper_item.blobs:
                pol = Polygon(blob.points)
                intersecting_lower_uuid = cls._lower_overlap(pol, lower_items)
                
                if intersecting_lower_uuid is not None:
                    logger.debug("{} UUID from {} intersects UUID from {}".format(str(upper_uuid), str(upper_fpath), str(intersecting_lower_uuid), str(lower_fpath)))
                    cls._set_meta_uuid(upper_meta, idx, intersecting_lower_uuid)
        
        logging.info("Propagating UUIDs from {} to {}".format(str(lower_fpath), str(upper_fpath)))
        upper_meta.write(upper_fpath)

def main():
    parser = ArgumentParser(description="Propagate coherent UUIDs from bottom to top in a ximage-annotated stack",
                            epilog="Autthor: Filippo Maria Castelli <castelli@lens.unifi.it>")
    parser.add_argument(
        "-d",
        "--input_dir",
        action="store",
        type=str,
        dest="img_dir_path",
        default="/home/phil/unpack_ximages_prop",
        help="Specify stack location",
        )
    
    args = parser.parse_args()
    
    stack_path = Path(args.img_dir_path)
    
    prop = UUIDStackPropagator(stack_path)
    prop.propagate()

if __name__ == "__main__":
    main()
    