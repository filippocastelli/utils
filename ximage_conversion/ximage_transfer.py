import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# from uuid import UUID, uuid4
from pathlib import Path
from argparse import ArgumentParser
import sys

sys.path.append('/home/phil/repos/ximage')
from ximage_utils import ximage_extract, ximage_inject, ximage_export
from ximage_core import XImageMeta

import numpy as np
import cv2

import skimage

SUPPORTED_EXTENSIONS = ["png", "tif", "tiff", "jpg", "jpeg"]
            
class TagTransferer:
    def __init__(self,
                 input_stack_path,
                 output_stack_path,
                 export=False):
        
        
        self.input_stack_path = Path(input_stack_path)
        self.output_stack_path = Path(output_stack_path)
        self.input_frame_paths = self._find_files(self.input_stack_path)
        self.output_frame_paths = self._find_files(self.output_stack_path)
        
        self.out_xml_dir = self.output_stack_path.joinpath("meta")
        self.out_xml_dir.mkdir(exist_ok=True, parents=True)
        
        self.export = export
        
        if self.export:
            self.original_masks_path = self.output_stack_path.joinpath("original_masks")
            self.original_masks_path.mkdir(exist_ok=True, parents=True)
            
            self.transferred_masks_path = self.output_stack_path.joinpath("transferred_masks")
            self.transferred_masks_path.mkdir(exist_ok=True, parents=True)

        
    @staticmethod
    def _find_files(search_path):
        return sorted([fname for fname in search_path.glob("*.*") if (fname.is_file() and fname.suffix.split(".")[1].lower() in SUPPORTED_EXTENSIONS)])
    
    def transfer_xml(self):
        for idx, input_frame_path in enumerate(self.input_frame_paths):
            xml_data = ximage_extract(input_frame_path)
            xml_fpath = self.out_xml_dir.joinpath(input_frame_path.stem + ".xml")
            self._dump_xml(xml_data, xml_fpath)
            logging.info("Transferring xml metadata from {} to {}".format(str(input_frame_path), str(xml_fpath)))
            ximage_inject(meta_path=xml_fpath, out_path=self.output_frame_paths[idx])
            
            if self.export:
                self._export_idxmask(input_frame_path, self.original_masks_path)
                self._export_idxmask(self.output_frame_paths[idx], self.transferred_masks_path)
    
    @staticmethod
    def _export_idxmask(frame_path, out_path):
        mask_fpath = out_path.joinpath(frame_path.stem + ".png")
        
        mask = ximage_export(frame_path)
        cv2.imwrite(str(mask_fpath), mask)
        
    @staticmethod
    def _dump_xml(xml_data, xml_path):
        with xml_path.open(mode="w") as dumpfile:
            dumpfile.write(xml_data)
    
class XimageOffset:
    
    def __init__(self,
                 input_stack,
                 offset,
                 export=False):
        self.input_stack_path = Path(input_stack)
        self.export = export
        
        self.offset = offset
        self.frame_paths = TagTransferer._find_files(self.input_stack_path)
        
        if self.export:
            self.offset_mask_path = self.input_stack_path.joinpath("offset_masks")
            self.offset_mask_path.mkdir(exist_ok=True, parents=True)
    
    
    def apply_offset(self):
        for idx, frame_path in enumerate(self.frame_paths):
            
            self._apply_offset_frame(frame_path, self.offset)
            if self.export:
                TagTransferer._export_idxmask(frame_path, self.offset_mask_path)
    
    @classmethod
    def _apply_offset_frame(cls, frame_path, offset):
        
        frame_meta = XImageMeta.read(frame_path)
        
        items = frame_meta.items
        logging.info("Applying offsets in {}".format(str(frame_path)))
        for item_idx, item in enumerate(items):
            item_uuid = item.uuid
            logging.info("Offsetting item {} by {} px".format(str(item_uuid), str(offset)))
            for blob_idx, blob in enumerate(item.blobs):
                points = blob.points
                new_points = points + offset
                frame_meta.items[item_idx].blobs[blob_idx].points = new_points
                
                
        frame_meta.write(frame_path)
        
        
def main():
    parser = ArgumentParser(description="Change class for all items in a givben class in stack",
                            epilog="Autthor: Filippo Maria Castelli <castelli@lens.unifi.it>")
    parser.add_argument(
        "-d",
        "--input_dir",
        action="store",
        type=str,
        dest="in_stack_path",
        default="/home/phil/in_stack",
        help="Specify input stack location",
        )
    
    parser.add_argument(
        "-o",
        "--output_dir",
        action="store",
        type=str,
        dest="out_stack_path",
        default="/home/phil/out_stack",
        help="Specify output stack location",
        )
    
    parser.add_argument(
        "--offset",
        action="store",
        type=int,
        nargs="+",
        dest="offset",
        default=[0,0],
        help="x,y offset",
        )
    parser.add_argument(
        "--export",
        action="store_true",
        default=False,
        dest="export",
        help="export masks, for debugging purposes",
        )
    
    parser.add_argument("--list", nargs="+", default=["a", "b"])
    
    args = parser.parse_args()
    in_stack_path = Path(args.in_stack_path)
    out_stack_path = Path(args.out_stack_path)
    
    offset = np.array(args.offset)
    
    export = args.export
    
    tt = TagTransferer(in_stack_path, out_stack_path, export=export)
    tt.transfer_xml()
    
    xo = XimageOffset(out_stack_path, offset, export=export)
    xo.apply_offset()

if __name__ == "__main__":
    main()

