# =============================================================================
# DATA IMPORT
#
# automate data import from Bioretics
#
# Filippo Maria Castelli
# castelli@lens.unifi.it
# =============================================================================

from pathlib import Path
from argparse import ArgumentParser
import subprocess
import logging

import tifffile
import numpy as np
from skimage import io as skio

from img_sort import img_sort
from bulk_rename import bulk_rename
from img_to_stack import img_to_stack

def main():
    
    parser = ArgumentParser()
    parser.add_argument(
        "-d",
        "--input_dir",
        action="store",
        type=str,
        dest="ximage_input_path",
        default="/mnt/NASone3/castelli/2pe_ximages",
        help="Specify images location",
        )
    
    parser.add_argument(
        "-o",
        "--output",
        action="store",
        type=str,
        dest="out_path_str",
        default="/mnt/NASone3/castelli/2pe_ximages/dataset",
        help="Specify output directory",
        )
    
    parser.add_argument(
        "--apl",
        action="store",
        type=str,
        dest="apl_path_str",
        default="/mnt/NASone3/castelli/shared_code/utils/ximage2indexmask.apl",
        help="Conversion .apl path",
        )
    
    parser.add_argument(
        "--classes",
        action="store",
        type=str,
        dest="classes",
        default="1",
        help="Classes for index mask",
        )
    
    parser.add_argument(
        "--aliquis",
        action="store",
        type=str,
        default="/home/castelli/aliquis/aliquis/",
        dest="aliquis_path_str",
        help="Aliquis install directory",
        )
    
    parser.add_argument(
        "--ximage_export",
        action="store_true",
        dest="ximage_export",
        help="Use ximage export instead of aliquis custom apl",
        )
    args = parser.parse_args()
    
    # Setting logger level
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    
    # IO Path definitions
    ximage_path = Path(args.ximage_input_path)
    output_path = Path(args.out_path_str)
    apl_path = Path(args.apl_path_str)
    aliquis_path = Path(args.aliquis_path_str)
    
    aliquis_activate = aliquis_path.joinpath("bin/activate")
    aliquispl_run_path = aliquis_path.joinpath("bin/aliquispl_run")
    aliquis_activate_cmd = "source {}".format(str(aliquis_activate))
    
    tmp_path = output_path.joinpath("tmp")
    tmp_path.mkdir(exist_ok=True, parents=True)
    
    classes, classes_int = _basic_sanitize_stringlist(args.classes)
    
    # Sorting imgs in train/val/test
    train_path, val_path, test_path = img_sort(
        input_path=ximage_path,
        output_path=tmp_path)
    
    unordered_paths = (train_path, val_path, test_path)
    
    # Renaming images
    frame_paths = [path.joinpath("frames") for path in unordered_paths]
    mask_paths = [path.joinpath("masks") for path in unordered_paths]
    
    for i, u_path in enumerate(unordered_paths):
        bulk_rename(in_dir=u_path, out_dir=frame_paths[i])
    
    # EXTRACT MASKS
        
    section_names = ("train", "val", "test")
    
    for i, frame_path in enumerate(frame_paths):
        #Defining output paths
        out_frames_path = output_path.joinpath("{}_frames.tif".format(section_names[i]))
        out_masks_path = output_path.joinpath("{}_masks.tif".format(section_names[i]))
        
        # Create masks
        mask_paths[i].mkdir(exist_ok=True, parents=True)
        
        frames = frame_path.glob("*.png")
        
        if args.ximage_export:
            for img_path in frames:
                logging.info("ximage converting {}".format(str(img_path)))
                fname = img_path.name
                mask_path = mask_paths[i].joinpath(fname)
                export_cmd = "ximage export {} {}".format(str(img_path), str(mask_path))
                cmd = "; ".join((aliquis_activate_cmd, export_cmd))
                subprocess_cmd(cmd)
        else:
            logging.info("Running Aliquis on {} masks".format(section_names[i]))
            aliquispl_run_cmd = "{} -o {} {} input_path={}/* classes={}".format(str(aliquispl_run_path), str(mask_paths[i]), str(apl_path), str(frame_path), classes)
            cmd = "; ".join((aliquis_activate_cmd, aliquispl_run_cmd))
            subprocess_cmd(cmd)
            
            #Fill in missing outputs
            masks = mask_paths[i].glob("*.png")
            mask_names = [path.name.split("_")[1] for path in masks]
            
            missing_output_frames = [frame for frame in frames if frame.name.split(".")[0] not in mask_names]
            
            for frame in missing_output_frames:
                missing_mask_name = "{}_{}_00000001.png".format(apl_path.name.split(".")[0], frame.name.split(".")[0])
                missing_mask_path = mask_paths[i].joinpath(missing_mask_name)
                img = skio.imread(frame, plugin="pil")
                tifffile.imwrite(missing_mask_path, (255*np.ones_like(img[:,:,0])).astype(np.uint8))
                
            #missing_output_frames = ["{}_{}_00000001.png".format(name, apl_path.name.split(".")[0]) for name in frame_names if name not in mask_names]
            
        # Stack frames and masks
        out_frames_path = output_path.joinpath("{}_frames.tif".format(section_names[i]))
        out_masks_path = output_path.joinpath("{}_masks.tif".format(section_names[i]))
        
        logging.info("Stacking outputs")
        frame_stack = img_to_stack(img_path=frame_path, out_path=out_frames_path, img_format=".png", save_stack=False)
        save_classes(frame_stack, out_frames_path, classes_int=None)
        mask_stack = img_to_stack(img_path=mask_paths[i], out_path=out_masks_path, img_format=".png", save_stack=False)
        save_classes(mask_stack, out_masks_path, classes_int)

  
def _basic_sanitize_stringlist(stringlist):
    stringlist = stringlist.split(",")
    intlist = [int(class_id_str) for class_id_str in stringlist]
    stringlist = [str(class_id_int) for class_id_int in intlist]
    return ",".join(stringlist), intlist

def subprocess_cmd(command):
    process = subprocess.Popen(command,stdout=subprocess.PIPE, shell=True, executable="/bin/bash")
    proc_stdout = process.communicate()[0].strip()
    print(proc_stdout)
    
def save_classes(stack, out_path, classes_int=None):
    p_dir = out_path.parents[0]
    
    if classes_int is not None:
        for img_class in classes_int:
            img_name = out_path.name.split(".")[0]
            img_name = img_name + "_class{}.tif".format(str(img_class))
            img_path = p_dir.joinpath(img_name)
            
            stack = np.where(stack == img_class, 255, 0).astype(np.uint8)
            tifffile.imwrite(img_path, stack)
    else:
        tifffile.imwrite(out_path, stack)
        
        
if __name__ == "__main__":
    main()

