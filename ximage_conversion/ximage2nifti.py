# =============================================================================
# XIMAGE2NIFTI
#
#
# Filippo Maria Castelli
# castelli@lens.unifi.it
# =============================================================================

import sys
sys.path.append('/home/phil/repos/ximage')
from argparse import ArgumentParser
import logging
from pathlib import Path
import numpy as np
import nibabel as nib
import tifffile

from ximage2indexmask import ximage2stack, masklist2dict, ximage2mask, get_sanitized_fname

logger = logging.getLogger()
logger.setLevel(logging.INFO)

SUPPORTED_EXT = ["png", "tif", "tiff", "jpg", "jpeg"]
SUPPORTED_EXT_PT = ["."+ ext for ext in SUPPORTED_EXT]

SUPPORTED_MODES = ["single", "multi"]

NIFTI_RES_UNITS = {
    "unknown" : 0,
    "m"     :   1,
    "mm"    :   2,
    "um"    :   3,
    "s"     :   8,
    "ms"    :   16,
    "us"    :   24,
    "hz"    :   32,
    "ppm"    :   40,
    "rads"    :   48,
    }

def save_nii_stack(stack, out_path, x_pix=10, y_pix=10, z_pix=0.0099, res_units = "mm"):
    """
    Save a numpy stack as a NIFTI-1 file

    Parameters
    ----------
    stack : np.ndarray
        Input stack.
    out_path : pathlib Path
        Output file path.
    x_pix : float, optional
        Pixel size in x, in res_units. The default is 10.
    y_pix : float, optional
        Pixel size in y, in res_units. The default is 10.
    z_pix : float, optional
        Pixel size in z, in res_units. The default is 10.
    res_units : str, optional
        Unitsize. The default is "mm".

    Raises
    ------
    ValueError
        Raise if unsupported unitsize is used, See NIFTI_RES_UNITS.
    """
    
    if res_units in NIFTI_RES_UNITS:
        res_units_code = NIFTI_RES_UNITS[res_units]
    else:
        raise ValueError("unsupported {} units, choose one in {}".format(res_units,
                                                                         list(NIFTI_RES_UNITS.keys())))
    # from z,y,x to x,y,z
    if len(stack.shape) > 2:
        stack = np.swapaxes(stack, 0, 2)
        
    nifti = nib.Nifti1Image(stack, None)
    
    nifti.header['pixdim'][1] = x_pix
    nifti.header['pixdim'][2] = y_pix
    nifti.header['pixdim'][3] = z_pix
    
    if stack.dtype == 'uint8':
        # 2 is the NIFTI code for unsigned char, see https://nifti.nimh.nih.gov/nifti-1/documentation/nifti1fields/
        nifti.header['datatype'] = 2
        nifti.header['bitpix'] = 8
    if stack.dtype == 'uint16':
        # 512 is the NIFTI code for unsigned int16, see https://nifti.nimh.nih.gov/nifti-1/documentation/nifti1fields/
        nifti.header['datatype'] = 512
        nifti.header['bitpix'] = 16
    # 2 is the NIFTI code for millimeters, see https://nifti.nimh.nih.gov/nifti-1/documentation/nifti1fields/
    nifti.header['xyzt_units'] = res_units_code
    
    nib.save(nifti, out_path)
    
    
def main():
    parser = ArgumentParser()
    
    parser.add_argument("-d", type=str, default="/mnt/NASone3/castelli/test.png", action="store", dest="input_path", help="Input file path")
    parser.add_argument("-o", dest="output_path", action="store", default=None, help="output directory path")
    parser.add_argument("-m","--mp", dest="multiprocessing", action="store_true", help="use multiprocessing")
    parser.add_argument("-x", "--xres", type=float, dest="x_resolution", default=10, help="Voxel resolution along x (in chosen units, def mm)")
    parser.add_argument("-y", "--yres", type=float, dest="y_resolution", default=10, help="Voxel resolution along y (in chosen units, def mm)")
    parser.add_argument("-z", "--zres", type=float, dest="z_resolution", default=10, help="Voxel resolution along z (in chosen units, def mm)")
    parser.add_argument("-u", "--units", type=str, dest="res_units", default="mm", help="Voxel resolution units")
    parser.add_argument("-i", "--invert", dest="invert", action="store_true", default=False, help="Invert black-white label.")
    
    args = parser.parse_args()

    input_fpath = Path(args.input_path)
    
    if not args.output_path:
        if input_fpath.is_dir():
            out_path = input_fpath.joinpath("masks")
        else:
            out_path = input_fpath.parent.joinpath("masks")
    
    out_path.mkdir(exist_ok=True, parents=True)
    
    # scan directory
    if input_fpath.is_dir():
        print("Extracting masks...")
        mask_list = ximage2stack(input_fpath, args.multiprocessing, mode="multi", invert=args.invert)
        mask_dict = masklist2dict(mask_list, invert=args.invert)
        
        # create a nifti for each element in dictionary
        print("Saving NIFTI...")
        for seg_class, stack in mask_dict.items():
            out_stack_path = out_path.joinpath("{}_{}.nii.gz".format(out_path.name, seg_class))
            save_nii_stack(stack=stack,
                           out_path=out_stack_path,
                           x_pix=args.x_resolution,
                           y_pix=args.y_resolution,
                           z_pix=args.z_resolution)

            
    # if fpath points to a single file just apply ximage2indxmaks to that file only
    elif input_fpath.is_file():
        
        mask_dict = ximage2mask(input_fpath, mode="multi")
        s_fname = get_sanitized_fname(input_fpath, add_fix="mask", ignore_extension=True)
        
        for seg_class, mask in mask_dict.items():
            
            out_fpath = out_path.joinpath("{}_{}.nii.gz".format(s_fname, seg_class))
            save_nii_stack(stack=mask,
                           out_path=out_fpath,
                           x_pix=args.x_resolution,
                           y_pix=args.y_resolution,
                           z_pix=args.z_resolution)
            
        # create nifti
    else:
        logger.error("No input file was provided")

    
if __name__ == "__main__":
    main()
