# =============================================================================
# SPLIT_INDEXMASK
#
#
# Filippo Maria Castelli
# castelli@lens.unifi.it
# =============================================================================

from argparse import ArgumentParser
from pathlib import Path
from skimage import io as skio
import tifffile
import numpy as np
from functools import partial

from multiprocessing import Pool, cpu_count
from tqdm import tqdm
import gc

SUPPORTED_EXT = ["tif", "tiff"]

def main():
    parser = ArgumentParser()
    
    parser.add_argument(
        "-d",
        type=str,
        default="/home/phil/Scrivania/test",
        action="store",
        dest="input_path",
        help="Input file path")
    
    parser.add_argument(
        "-m",
        dest="multithread",
        action="store_true",
        help="enable multithread")
    
    parser.add_argument(
        "-t",
        dest="threads",
        type=int,
        default=None,
        action="store",
        help="number of threads")
    
    parser.add_argument(
        "-v",
        dest="values",
        type=int,
        nargs='+',
        help="index mask values")
    
    parser.add_argument(
        "-p",
        dest="pos",
        type=int,
        default=255,
        help="output mask positive value")
        
    args = parser.parse_args()
    masks_path = Path(args.input_path)
    multithread = args.multithread
    split_indexmasks(masks_path,
                     positive=args.pos,
                     multi=multithread,
                     threads=args.threads,
                     values=args.values)
    

def split_indexmasks(mask_dir_path, values=None, positive=255, multi=False, threads=None):
    
    stack_fpaths = [path for path in mask_dir_path.glob("*.*") if _get_extension(path) in SUPPORTED_EXT]
    
    if values is None:    
        values = _get_uniques(stack_fpaths[0])

    partial_callable = partial(_extract_binary_masks, values=values, positive=positive, negative=0)

    print("extracting...")
    if not multi:
        for stack_fpath in tqdm(stack_fpaths):
            partial_callable(stack_fpath)
    else:
        print("starting pool...")
        n_workers = threads if threads is not None else cpu_count()
        with Pool(n_workers) as pool:
            n_steps = len(stack_fpaths)
            with tqdm(total=n_steps) as pbar:
                for i, _ in enumerate(pool.map(partial_callable, stack_fpaths)):
                    pbar.update()
            
    
    
def _extract_binary_masks(indexmask_fpath, values, positive=255, negative=0):
    print("extracting {}...".format(str(indexmask_fpath)))
    indexmask_img = skio.imread(str(indexmask_fpath))
    output_dir = indexmask_fpath.parent.joinpath(indexmask_fpath.stem)
    output_dir.mkdir(exist_ok=True)
    
    for value in values:
        binary_mask = _binary_mask(indexmask_img, value, positive, negative)
        output_path = output_dir.joinpath("{}.tif".format(str(value)))
        tifffile.imwrite(str(output_path), binary_mask)
        
        gc.collect()

def _get_uniques(fpath):
    vol = skio.imread(str(fpath))
    return np.unique(vol)

def _get_extension(path):
    return path.suffix.split(".")[-1]

def _binary_mask(array, filter_value, positive=255, negative=0):
    
    cpy = array.copy()
    mask = cpy == filter_value
    
    cpy[mask] = positive
    cpy[~mask] = negative
    
    return cpy
    # np.where uses internal conversion to float64 which causes memory errors
    # return np.where(array==filter_value, positive, negative)
    

    
if __name__ == "__main__":
    main()
