# =============================================================================
# BULK RENAME
#
# renaming lots of files
#
# Filippo Maria Castelli
# castelli@lens.unifi.it
# =============================================================================

from pathlib import Path
from argparse import ArgumentParser
import shutil

def main():
    
    parser = ArgumentParser()
    parser.add_argument(
        "-d",
        "--input_dir",
        action="store",
        type=str,
        dest="img_dir_path",
        default="/mnt/NASone3/castelli/2pe_ximages",
        help="Specify images location",
        )
    
    parser.add_argument(
        "-o",
        "--output",
        action="store",
        type=str,
        dest="out_path_str",
        default="/mnt/NASone3/castelli/2pe_ximages/sorted",
        help="Specify output directory",
        )
    
    parser.add_argument(
        "-s",
        "--start",
        action="store",
        type=int,
        dest="start",
        default=None,
        help="start counting from "
        )

    args = parser.parse_args()
    
    img_dir_path = Path(args.img_dir_path)
    output_path = Path(args.out_path_str)
    start = args.start

    bulk_rename(img_dir_path, output_path, start=start)
    

def bulk_rename(in_dir, out_dir, start=None):
    """Copy and rename images in alphabetical order"""
    
    images = sorted([fname for fname in in_dir.glob("*.*") if fname.is_file()])
    
    out_dir.mkdir(exist_ok=True, parents=True)
    
    for i, fpath in enumerate(images):
        if start is not None:
            i = i+start
        fname = format(i, '08d')+".png"
        to_path = out_dir.joinpath(fname)
        shutil.copy(str(fpath),str(to_path))

    
if __name__ == "__main__":
    main()

