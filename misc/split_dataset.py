from pathlib import Path
from argparse import ArgumentParser
from typing import List
from shutil import copy

from sklearn.model_selection import train_test_split
import numpy as np


def main():
    parser = ArgumentParser()
    parser.add_argument("-d", "--dataset",
                        action="store",
                        type=str,
                        dest="dataset_path",
                        help="dataset path")

    parser.add_argument("-o", "--output",
                        action="store",
                        type=str,
                        dest="output_path",
                        help="output path")

    parser.add_argument("--test",
                        type=float,
                        default=0.3,
                        dest="test_ratio",
                        help="test ratio",
                        action="store")

    parser.add_argument("--train",
                        type=float,
                        default=0.7,
                        dest="train_ratio",
                        help="train ratio",
                        action="store")

    parser.add_argument("--val",
                        type=float,
                        default=0.1,
                        dest="val_ratio",
                        help="validation ratio",
                        action="store")

    args = parser.parse_args()
    dataset_path = Path(args.dataset_path)
    output_path = Path(args.output_path)
    train_ratio = args.train_ratio
    test_ratio = args.test_ratio
    val_ratio = args.val_ratio

    ds = DatasetSplitter(
        dataset_path=dataset_path,
        output_path=output_path,
        train_ratio=train_ratio,
        test_ratio=test_ratio,
        val_ratio=val_ratio
    )


class DatasetSplitter:
    def __init__(self,
                 dataset_path: Path,
                 output_path: Path,
                 train_ratio: float = 0.7,
                 test_ratio: float = 0.2,
                 val_ratio: float = 0.1,
                 img_extension: str = ".png",
                 bbox_file_extension: str = None,
                 ):
        self.dataset_path = dataset_path
        self.output_path = output_path
        self.output_path.mkdir(exist_ok=True, parents=True)
        self.train_ratio = train_ratio
        self.test_ratio = test_ratio
        self.val_ratio = val_ratio
        self.extension = img_extension
        self.bbox_file_extension = bbox_file_extension

        self.bbox = True if self.bbox_file_extension else False

        self.frames_path = self.dataset_path.joinpath("frames")
        self.masks_path = self.dataset_path.joinpath("masks")
        self.subdir_names = ["train", "test", "val"]

        self.frames_fpath_list = sorted(list(self.frames_path.glob("*"+self.extension)))
        self.masks_fpath_list = sorted(list(self.masks_path.glob("*"+self.extension)))

        assert len(self.frames_fpath_list) == len(self.masks_fpath_list), ""
        if self.bbox:
            self.bbox_fpath_list = [fpath.parent.joinpath(fpath.name + self.bbox_file_extension) for fpath in
                                    self.masks_fpath_list]
        else:
            self.bbox_fpath_list = None

        self.split_data_dict = self.split_data(frames_list=self.frames_fpath_list,
                                               masks_list=self.masks_fpath_list,
                                               bbox_list=self.bbox_fpath_list,
                                               ratios=(self.train_ratio, self.test_ratio, self.val_ratio)
                                               )
        self.export_split(self.split_data_dict, self.output_path)

    @staticmethod
    def split_data(frames_list: list,
                   masks_list: list,
                   bbox_list: list = None,
                   ratios: tuple = (0.7, 0.2, 0.1)) -> dict:

        if bbox_list is not None:
            dataset = (frames_list, masks_list, bbox_list)
        else:
            dataset = (frames_list, masks_list)
        ratio_norm = np.sum(ratios)
        ratios = np.array(ratios) / ratio_norm

        split_train_test = train_test_split(*dataset, test_size=ratios[1] + ratios[2], train_size=ratios[0])

        if bbox_list is not None:
            frames_train, frames_test, masks_train, masks_test, bbox_train, bbox_test = split_train_test
            train_out = (frames_train, masks_train, bbox_train)
            test_val_dataset = (frames_test, masks_test, bbox_test)
        else:
            frames_train, frames_test, masks_train, masks_test = split_train_test
            train_out = (frames_train, masks_train)
            test_val_dataset = (frames_test, masks_test)

        split_test_val = train_test_split(*test_val_dataset, test_size=ratios[2], train_size=ratios[1])

        if bbox_list is not None:
            frames_test, frames_val, masks_test, masks_val, bbox_test, bbox_val = split_test_val
            test_out = (frames_test, masks_test, bbox_test)
            val_out = (frames_val, masks_val, bbox_val)
        else:
            frames_test, frames_val, masks_test, masks_val = split_test_val
            test_out = (frames_test, masks_test)
            val_out = (frames_val, masks_val)

        return {
            "train": train_out,
            "test": test_out,
            "val": val_out
        }

    @staticmethod
    def copy_file_list(fpaths: List[Path],
                       out_dir_fpath: Path):
        for fpath in fpaths:
            out_fpath = out_dir_fpath.joinpath(fpath.name)
            copy(str(fpath), str(out_fpath))

    @classmethod
    def export_split(cls,
                     dataset_dict: dict,
                     output_path: Path):
        sections = ["train", "test", "val"]
        for section in sections:
            subdir_path = output_path.joinpath(section)
            subdir_path.mkdir(exist_ok=True, parents=True)
            frames_path = subdir_path.joinpath("frames")
            frames_path.mkdir(exist_ok=True, parents=True)
            masks_path = subdir_path.joinpath("masks")
            masks_path.mkdir(exist_ok=True, parents=True)

            dataset_section = dataset_dict[section]

            if len(dataset_section) == 2:
                frames_fpaths, masks_fpaths = dataset_section
            elif len(dataset_section) == 3:
                frames_fpaths, masks_fpaths, bbox_fpaths = dataset_section
                cls.copy_file_list(bbox_fpaths, masks_path)
            else:
                raise ValueError("invalid dataset split len")
            cls.copy_file_list(frames_fpaths, frames_path)
            cls.copy_file_list(masks_fpaths, masks_path)


if __name__ == "__main__":
    main()
