import itertools

import numpy as np
np.set_printoptions(suppress=True)
import skimage.external.tifffile as tiff
from tqdm import tqdm
from skimage import io as skio
from skimage.color import rgb2gray
import skimage
import matplotlib.pyplot as plt

class BitEncoder16:

    def __init__(self, n=512, w=2**16):
        self.n = n
        self.w = w
        self.lut = self._gen_lut()

    def _gen_lut(self):
        """return the LUT over all w values"""

        n = self.n
        w = self.w
        p = n / w

        d = np.arange(w)

        # Linear component
        L = (d + 0.5) / w

        # Piecewise Triangulars
        # consts
        Lp_2 = (L / (p / 2)) % 2
        Lp_4 = ((L - (p / 4)) / (p / 2)) % 2

        # Ha
        Ha = Lp_2
        ie = Ha > 1
        Ha[ie] = 2 - Ha[ie]

        # Hb
        Hb = Lp_4
        ie = Hb > 1
        Hb[ie] = 2 - Hb[ie]

        lut = np.c_[L, Ha, Hb] * 255

        return np.floor(lut).astype(np.uint8)
    
    def _gen_inv_lut(self, vectorized = True):
        L_range = np.arange(256)
        Ha_range = L_range
        Hb_range = L_range
        
        # Qui puoi anche usare meshgri (anche più veloce), è solo che non mi tornavano le dimensioni
        big_fat_arr = np.array(list(itertools.product(L_range, Ha_range, Hb_range)))
        # big_fat_arr = np.array(np.meshgrid(L_range, L_range, L_range))
        inv_lut = self.decode(big_fat_arr)
        inv_lut = inv_lut.reshape((256, 256, 256))
        
        return inv_lut
            
    def decode(self, encoded):
        """
        Decode one or multiple values using the LUT

        Parameters
        ----------
        encoded : nndarray
            encoded array.
        Ha : ndarray
            Ma-encoded channel.
        Hb : ndarray
            Mb-encoded channel.
        """

        encoded = encoded / 255

        L = encoded[0]
        Ha = encoded[1]
        Hb = encoded[2]

        n = self.n
        w = self.w
        p = n / w

        # m(L*)
        m = np.floor(4 * (L / p) - 0.5) % 4

        # L0(L*)
        L_0 = L - ((L - p / 8) % p) + (p / 4) * m - p / 8

        # delta(L*, Ha*, Hb*)
        delta = np.zeros(encoded.shape[1:])

        # piecewise definitions
        # m = 0
        idx = np.where(m == 0)
        delta[idx] = Ha[idx]
        # m = 1
        idx = np.where(m == 1)
        delta[idx] = Hb[idx]
        # m = 2
        idx = np.where(m == 2)
        delta[idx] = 1 - Ha[idx]
        # m = 3
        idx = np.where(m == 3)
        delta[idx] = 1 - Hb[idx]
        
        delta *= (p / 2)

        # d(L*, Ha*, Hb*)
        d = w * (L_0 + delta)
        return np.floor(d).astype(np.uint16)

    def encode(self, d):
        """Encode one or multiple values using the LUT"""
        return self.lut[d]


# img = tiff.imread("test_stack.tif")

encoder = BitEncoder16()
lut = encoder.lut

panda = skio.imread("panda.jpg")
gray_panda = rgb2gray(panda)
gray_panda = skimage.img_as_int(gray_panda)
skio.imshow(gray_panda)

encoded_panda = encoder.encode(gray_panda)
moved_axis_panda = np.moveaxis(encoded_panda, -1, 0)

redecoded_panda = encoder.decode(moved_axis_panda)

skio.imshow(redecoded_panda)


plt.subplot(1, 3, 1)
plt.title('16bit panda')
plt.imshow(gray_panda)

plt.subplot(1, 3, 2)
plt.title('24bit encoded panda')
plt.imshow(encoded_panda)

plt.subplot(1, 3, 3)
plt.title('16bit back_encoded panda')
plt.imshow(redecoded_panda)


plt.show()

