from pathlib import Path
from argparse import ArgumentParser

from shutil import copy
import os

SUPPORTED_EXT = ["png", "tif", "tiff"]
def main():
    parser = ArgumentParser(
        description="Rename all files in folder to substitute . to -")
    parser.add_argument(
        "-d",
        type=str,
        default="/home/phil/test_img",
        action="store",
        dest="results_path",
        help="results_path")
    
    args = parser.parse_args()
    results_path = Path(args.results_path)
    
    rename_substitute(results_path)
    
    
def _get_extension(fpath):
    return fpath.suffix.split(".")[-1]
    
def _copy_to_dest(fpath, target_dir_path):
    output_path = target_dir_path.joinpath(fpath.name)
    copy(str(fpath), str(output_path))

def rename_substitute(path):
    
    files_to_rename = [fpath for fpath in  path.glob("*") if fpath.is_file() and _get_extension(fpath) in SUPPORTED_EXT]
    
    for fpath in files_to_rename:
        new_path = _remove_dots(fpath)
        os.rename(fpath, new_path)
    

def _remove_dots(fpath):
    
    file_stem = fpath.stem
    file_suffix = fpath.suffix
    
    file_stem_replace = file_stem.replace(".", "-")
    return fpath.parent.joinpath(file_stem_replace + file_suffix)
    

if __name__ == "__main__":
    main()