from pathlib import Path

import tifffile
import numpy as np

import matplotlib.pyplot as plt

maps_dir = Path("/mnt/ssd1/2pe_paper/maps")
samples = ["pm", "pd", "f3", "xu"]
# metrics_list = ["count", "density", "volume"]
metrics_list = ["count"]
def _save_vol(key, path, out_path):
    vol = np.load(path)
    
    vol = np.transpose(vol, axes=(2, 0, 1))
    # vol = np.rot90(vol, k=-1, axes=(1,2))
    tiffpath = out_path.joinpath(key+".tif")
    print("saving {}".format(str(tiffpath)))
    tifffile.imwrite(str(tiffpath), vol)


#%%
for sample in samples:
    sample_dir = maps_dir.joinpath(sample)
    npy_dir = sample_dir.joinpath("npy")
    tiff_dir = sample_dir.joinpath("tiffs")
    tiff_dir.mkdir(exist_ok=True, parents=True)
    
    for mapitem in metrics_list:    
        volume_path = npy_dir.joinpath("{}_{}.npy".format(sample, mapitem))
        _save_vol(mapitem, volume_path, tiff_dir)
#%%

maps_dir = Path("/mnt/ssd1/2pe_paper/new_maps/maps")
npy_files = maps_dir.glob("*.npy")
for fpath in npy_files:
    _save_vol(key=fpath.stem, path=fpath, out_path=maps_dir)