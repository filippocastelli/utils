from pathlib import Path
import numpy as np
from matplotlib import pylab as plt
from matplotlib import ticker

plt.ioff()
DEF_FONTSIZE = 38
LEGEND_FONTSIZE = 25


plt.rc("xtick", labelsize=DEF_FONTSIZE)
plt.rc("ytick", labelsize=DEF_FONTSIZE)


from tqdm import tqdm
import csv

profiles_dir = Path("/mnt/ssd1/2pe_paper/profiles")
out_path = Path("/mnt/ssd1/2pe_paper/plots")

samples = ["pm", "pd", "f3"]
measures = ["volume", "density", "count"]
profile_trim = -1

z_trim = {"pm": (16, 36), "pd": (19, 39), "f3": (31, 51)}


pd_ids = [
    "0049-0062-0257",
    "0049-0147-0283",
    "0049-0187-0298",
    "0049-0210-0317",
    "0049-0078-0258",
    "0049-0130-0279",
    "0049-0163-0292",
    "0049-0202-0307",
    "0049-0175-0294",
    "0049-0111-0254",
]


f3_ids = [
    "0089-0361-0483",
    "0061-0432-0508",
    "0089-0193-0359",
    "0068-0554-0535",
    "0068-0397-0492",
    "0082-0201-0341",
    "0082-0232-0449",
    "0082-0589-0537",
    "0058-0222-0328",
    "0058-0617-0538",
]

pm_ids = [
    "0064-0389-0228",
    "0064-0362-0187",
    "0064-0346-0181",
    "0064-0320-0169",
    "0064-0298-0163",
    "0064-0254-0173",
    "0064-0212-0209",
    "0064-0190-0250",
    "0064-0184-0285",
    "0018-0162-0367",
]

id_dict = {
    "pm": pm_ids,
    "f3": f3_ids,
    "pd": pd_ids
    }

#%% OLD COUNT METHOD
# y_tick_dict = {
#     "pm": {
#         "volume": (0, 6000, 12000),
#         "count": (0, 15, 30),
#         "density": (0, 0.15, 0.30)
#         },
#     "pd": {
#         "volume": (0, 5000, 10000),
#         "count": (0, 20, 40),
#         "density": (0, 0.1, 0.2),
#         },
#     "f3":{
#         "volume" : (0, 2500, 5000),
#         "density": (0, 0.1, 0.2),
#         "count": (0, 25, 50)
#     }
#     }
#%% NEW COUNT METHOD
y_tick_dict = {
    "pm": {
        "volume": (0, 6000, 12000),
        "count": (0, 30, 60),
        "density": (0, 0.15, 0.30)
        },
    "pd": {
        "volume": (0, 5000, 10000),
        "count": (0, 25, 50),
        "density": (0, 0.1, 0.2),
        },
    "f3":{
        "volume" : (0, 2500, 5000),
        "density": (0, 0.1, 0.2),
        "count": (0, 60, 120),
    }
    }
#%%
class ProfilePlotter:
    def __init__(
        self,
        profiles_dir,
        samples=["pm", "pd", "f3"],
        measures=["volume", "density", "count"],
        id_order_dict = None,
        profile_trim=-1,
        z_trim={},
        figsize=(20, 5),
        plot_mean=True,
        plot_std=True,
        plot_title=False,
        out_path=None,
        debug_mode=True,
        y_tick_dict=None
    ):

        self.profiles_dir = Path(profiles_dir)
        self.samples = samples
        self.measures = measures
        self.id_order_dict = id_order_dict

        self.out_path = None if out_path is None else Path(out_path)

        self.profile_trim = profile_trim
        self.z_trim = z_trim
        self.samples_dirs = {
            sample: profiles_dir.joinpath("{}_profiles".format(sample))
            for sample in samples
        }

        self.sample_dict = {
            sample: self._load_profiles(sample) for sample in self.samples
        }
        
        
        self.plot_mean = plot_mean
        self.plot_std = plot_std
        self.plot_title = plot_title

        self.debug = debug_mode

        self.measure_label_dict = {
            "volume": r"Volume ($\mu m^3$)",
            "density": "Volumetric density",
            "count": r"Cell count (neurons/voxel)",
        }

        self.figsize = figsize
        
        self.y_tick_dict = y_tick_dict

        # self.plot_profile(sample="pm",
        #                   sample_id="0018-0162-0367",
        #                   measure="volume",
        #                   figsize=self.figsize,
        #                   save=True)

    def _trim_profiles(self, sample, profile_list):
        z_trim_sample = self.z_trim[sample]
        trimmed_profiles = [
            profile[: self.profile_trim, z_trim_sample[0] : z_trim_sample[-1]]
            for profile in profile_list
        ]
        lengths = [len(profile) for profile in trimmed_profiles]
        # assert len(np.unique(lengths)) == 1, "trimming didnt work bro"
        return trimmed_profiles

    def _load_profiles(self, sample):
        sample_dict = {}
        for meas in measures:
            sample_dir = self.samples_dirs[sample].joinpath(meas)
            npy_paths = list(sample_dir.glob("*.npy"))
            npy_profiles = [np.load(fpath).T for fpath in npy_paths]
            ids = [fpath.stem for fpath in npy_paths]
            npy_profiles = self._trim_profiles(sample, npy_profiles)

            sample_dict[meas] = self._get_sample_id_dict(ids, npy_profiles)

        return sample_dict

    @staticmethod
    def _get_sample_id_dict(id_list, value_list):
        sample_dict = {}
        for idx, sample_id in enumerate(id_list):
            sample_dict[sample_id] = value_list[idx]
        return sample_dict

    @staticmethod
    def _init_fig(figsize):
        fig = plt.figure(figsize=figsize)
        ax = fig.gca()
        return fig, ax

    def _get_profile_stats(self, sample, measure, sample_id, save_to=None):
        z_profiles = self.sample_dict[sample][measure][sample_id]

        lengths = [len(profile) for profile in z_profiles]
        if len(np.unique(lengths)) > 1:
            print("ciao")
        mean = np.mean(z_profiles, axis=1)
        std = np.std(z_profiles, axis=1)

        csv_dict = {
            "z_profiles": z_profiles,
            "mean": mean,
            "std": std,
        }

        if save_to:
            self._write_csv(csv_dict, sample_id, save_to)

        return csv_dict

    def _write_csv(self, csv_dict, sample_id, out_path):
        csv_copy = csv_dict.copy()
        z_profiles = csv_copy.pop("z_profiles")

        zprofiles_csv_path = out_path.joinpath(sample_id + "_zprofiles.csv")
        additional_profiles_csv_path = out_path.joinpath(
            sample_id + "_additional_profiles.csv"
        )

        with zprofiles_csv_path.open(mode="w") as csv_out:
            writer = csv.writer(csv_out)
            firstrow = ("z_profiles",)
            writer.writerow(firstrow)
            writer.writerows(z_profiles)

        with additional_profiles_csv_path.open(mode="w") as csv_out:
            writer = csv.writer(csv_out)
            keys = list(csv_copy.keys())
            writer.writerow(keys)
            for key in keys:
                row = csv_copy[key]
                writer.writerow(row)

    def plot_profile(
        self,
        sample,
        measure,
        sample_id,
        fig = None,
        ax = None,
        figsize=(20, 10),
        color=None,
        plot_mean=True,
        plot_std=True,
        save=False,
        plot_title=False,
        legend=True,
        xlim=None,
        ylim=None,
        last_tick=False
    ):
        out_dir = self.out_path.joinpath("{}/{}".format(sample, measure))
        out_dir.mkdir(parents=True, exist_ok=True)
        
        if not (fig or ax):
            is_subplot = False
            fig, ax = self._init_fig(figsize=figsize)
        else:
            is_subplot = True

            
        z_profiles = self.sample_dict[sample][measure][sample_id]

        csv_dict = self._get_profile_stats(sample, measure, sample_id, out_dir)

        z_profiles = csv_dict["z_profiles"]
        mean_profile = csv_dict["mean"]
        std_profile = csv_dict["std"]

        n_x, n_z = z_profiles.shape

        X = np.arange(start=0, stop=n_x)

        if plot_mean:
            color = "lightgray"

        # Z PROFILES
        for idx, profile in enumerate(z_profiles.T):
            label = "_nolegend_" if not (idx == len(z_profiles.T) - 1) else "z profile"
            z_plot = ax.plot(profile, color=color, label=label, zorder=-32)

        # # STD
        if plot_std:
            # std_errorbars = ax.errorbar(x=X, y=mean_profile, yerr=std_profile, zorder=-30, color="darkgray")
            lower_std = ax.fill_between(
                x=X,
                y1=mean_profile + std_profile,
                zorder=-15,
                y2=mean_profile - std_profile,
                alpha=0.2,
                color="gold",
                label=r"$\sigma$",
            )
        # MEAN
        if plot_mean:
            mean_plot = ax.plot(mean_profile, color="black", label="mean")
            
            if legend:
                ax.legend(fontsize=LEGEND_FONTSIZE,
                          loc="upper right")
        
        title = "{}_{}".format(sample_id, measure)
        if plot_mean:
            title = title + "_mean"

        if plot_title and not is_subplot:
            ax.set_title(title)
        
        scale = 10
        ticks = ticker.FuncFormatter(lambda x, pos: "{0:g}".format(x * scale))
        
        if xlim:
            ax.set_xlim(xlim)
        if ylim:
            ax.set_ylim(ylim)
        
        if last_tick:
            ax.set_xticks(
                list(ax.get_xticks()) + [n_x,]
            )
            
        
        if self.y_tick_dict:
            y_ticks = self.y_tick_dict[sample][measure]
            
            y_low = y_ticks[0]
            y_high = y_ticks[-1]
            
            ax.set_ylim((y_low, y_high))
            ax.yaxis.set_ticks(y_ticks)
        # y_start, y_stop = ax.get_ylim()
        # ax.yaxis.set_ticks(np.linspace(start=y_start, stop=y_stop, num=3).astype(int))
        ax.xaxis.set_major_formatter(ticks)
        
        if not is_subplot:
            # ax.set_xlabel("cortical depth (x100um)", fontsize=15)
            ax.set_xlabel(r"Cortical depth ($\mu m$)", fontsize=DEF_FONTSIZE)
            ax.set_ylabel(self.measure_label_dict[measure], fontsize=DEF_FONTSIZE)
        # plt.tight_layout()
        if save and self.out_path:
            out_fpath = out_dir.joinpath("{}.png".format(title))
            fig.savefig(out_fpath)

    def plot_multi(self):
        
        self.out_multi = self.out_path.joinpath("multi")
        self.out_multi.mkdir(exist_ok=True, parents=True)
            
        if self.debug == True:
            #plt.ion()
            sample = "f3"
            out_sample = self.out_multi.joinpath(sample)
            out_sample.mkdir(exist_ok = True, parents=True)
            measure = "count"
            self.column_plot(sample, measure, out_path=out_sample)
            
        else:
            for sample in tqdm(self.samples):
                out_sample = self.out_multi.joinpath(sample)
                out_sample.mkdir(exist_ok=True, parents=True)
                for measure in tqdm(self.measures):
                    self.column_plot(sample, measure, out_sample)
    
    
    
    def column_plot(self, sample, measure, out_path=None):
        
        fig_x, fig_y = self.figsize
        ordered_ids = self.id_order_dict[sample]
        figsize_multi = self.figsize
        fig, axs = plt.subplots(len(ordered_ids), figsize=figsize_multi, sharex=True)
        
        xlim, ylim = self._get_column_lims(sample, measure)
        for idx, sample_id in enumerate(ordered_ids):
            
            legend = idx == 0
            self.plot_profile(sample=sample,
                              measure=measure,
                              sample_id=sample_id,
                              fig=fig,
                              ax=axs[idx],
                              save=False,
                              plot_title=False,
                              legend=legend,
                              xlim=xlim,
                              ylim=None)
            
            plt.subplots_adjust(bottom=0.05,
                                top=0.99,
                                left=0.145,
                                right=0.95,
                                hspace=None,
                                wspace=None)
            # plt.tight_layout()
            
            shared_label_fontsize = int(1.2*DEF_FONTSIZE)
            # fig.suptitle(measure,fontsize=2*DEF_FONTSIZE)
            fig.text(0.5, 0.01, r"cortical depth ($\mu m$)", ha='center',
                     fontsize=shared_label_fontsize)
            fig.text(0.005, 0.5, self.measure_label_dict[measure],
                     va='center',
                     rotation='vertical',
                     fontsize=shared_label_fontsize)
            out_fpath = out_path.joinpath(measure+".png")
            fig.savefig(out_fpath)
            
    def _get_column_lims(self, sample, measure):
        
        ordered_ids = self.id_order_dict[sample]
        profiles_dict = self.sample_dict[sample][measure]
        
        mins = []
        maxs = []
        
        lengths = []
        for sample_id in ordered_ids:
            z_profiles = profiles_dict[sample_id].T
            
            mins.append(np.min(z_profiles))
            maxs.append(np.max(z_profiles))
            
            lengths.append(len(z_profiles[0]))
        ylim = (np.min(mins), np.max(maxs))
        xlim = (0, np.max(lengths))
        return xlim, ylim
            
    def plot_profiles(self):

        if self.debug:
            plt.ion()
            sample = self.samples[0]
            measure = self.measures[1]

            id_dict = self.sample_dict[sample][measure]
            sample_id = list(id_dict.keys())[0]
            self.plot_profile(
                sample=sample,
                measure=measure,
                sample_id=sample_id,
                figsize=self.figsize,
                color=None,
                plot_mean=True,
                plot_std=True,
                plot_title=self.plot_title,
                save=True,
            )
        else:
            for sample in tqdm(self.samples):
                for measure in tqdm(self.measures):

                    id_dict = self.sample_dict[sample][measure]
                    for sample_id in tqdm(list(id_dict.keys())):
                        self.plot_profile(
                            sample=sample,
                            measure=measure,
                            sample_id=sample_id,
                            figsize=self.figsize,
                            color=None,
                            plot_mean=True,
                            plot_std=self.plot_std,
                            plot_title=self.plot_title,
                            save=True,
                        )

                        self.plot_profile(
                            sample=sample,
                            measure=measure,
                            sample_id=sample_id,
                            figsize=self.figsize,
                            color=None,
                            plot_mean=False,
                            plot_std=False,
                            plot_title=self.plot_title,
                            save=True,
                        )
                            
                            

pp = ProfilePlotter(
    profiles_dir=profiles_dir,
    samples=samples,
    measures=measures,
    profile_trim=profile_trim,
    z_trim=z_trim,
    out_path=out_path,
    plot_title=False,
    debug_mode=False,
    id_order_dict=id_dict,
    figsize=(20,35),
    y_tick_dict=y_tick_dict,
)

#%%

pp.plot_multi()
# pp.plot_profiles()
# for sample, sample_profiles ,in profile_dict.items():
