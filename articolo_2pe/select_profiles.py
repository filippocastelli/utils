from pathlib import Path
import csv
from shutil import copyfile

csv_path = Path("/mnt/ssd1/2pe_paper/profiles/coords_corrette_f3.csv")
csv_rows = []
with csv_path.open(mode = "r") as csvfile:
    csvreader = csv.reader(csvfile, delimiter=",")
    
    for row in csvreader:
        csv_rows.append(row)

roi_ids = [row[0] for row in csv_rows if row[0] != "name"]
#%%

profiles_path = Path("/mnt/ssd1/2pe_paper/profiles/f3_profiles_orig")
out_path = Path("/mnt/ssd1/2pe_paper/profiles/f3_profiles")
out_path.mkdir(exist_ok=True, parents=True)

sub_names = ["count", "density", "volume"]

subdirs = [profiles_path.joinpath(sub_name) for sub_name in sub_names]

# png_paths = []
# npy_paths = []

# for idx, subdir in enumerate(subdirs):    
#     png_paths += list(subdir.glob("*.png"))
#     npy_paths += list(subdir.glob("*.npy"))
    
# #%%
# pres = []
# for idx, png_path in enumerate(png_paths):
#     if png_path.stem in roi_ids:
#         pres.append(png_path.stem)
        
# #%%
# for idx, png_path in enumerate(png_paths):
    
#     if png_path.stem not in roi_ids:
#         os.remove(png_path)
#         os.remove(npy_paths[idx])
        
        
#%%   
npy_dict = {}
for subdir in subdirs:
    file_list = list(subdir.glob("*.npy"))
    # npy_dict[subdir.stem] = file_list
    
    all_file_ids = [fpath.stem for fpath in file_list]
    assert set(roi_ids).issubset(set(all_file_ids)), "missing files"
    
    fpaths = [fpath for fpath in file_list if fpath.stem in roi_ids]
    
    out_subdir = out_path.joinpath(subdir.name)
    out_subdir.mkdir(exist_ok=True, parents=True)
    for fpath in fpaths:
        dest = out_path.joinpath(subdir.name).joinpath(fpath.name)
        copyfile(fpath, dest)
    
    