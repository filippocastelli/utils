from argparse import ArgumentParser
from pathlib import Path
import logging

import numpy as np
from zetastitcher import InputFile
import tifffile

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def main():
    parser = ArgumentParser(description='Correct integer shearing in a ZetaStitcher input file.')
    parser.add_argument('-s', '--source', type=str, required=True, help='Input file')
    parser.add_argument('-o', '--output', type=str, required=False, help='Output directory')
    parser.add_argument('-d', '--direction', type=str, required=False, help='Direction of shearing', choices=['x', 'y'])
    parser.add_argument('-delta', '--delta', type=int, required=False, help='Integer shearing factor')
    parser.add_argument('--start', type=int, required=False, help='Start index')
    parser.add_argument('--end', type=int, required=False, help='End index')
    parser.add_argument('-i', '--inverse', action='store_true', help='Inverse shearing')

    args = parser.parse_args()

    if args.direction is None:
        raise ValueError('Direction of shearing must be specified')
    if args.delta is None:
        raise ValueError('Integer shearing factor must be specified')

    source_path = Path(args.source)
    if not source_path.exists():
        raise FileNotFoundError(f'Source file {source_path} does not exist')

    inverse = args.inverse if args.inverse is not None else False

    output_suffix = '_corrected' if not inverse else '_inverse_corrected'

    if args.output is None:
        output_path = source_path.parent.joinpath(source_path.stem + output_suffix + '.tif')
    else:
        output_path = Path(args.output).joinpath(source_path.stem + output_suffix + '.tif')

    isc = IntegerShearingCorrect(
        source_path=source_path,
        output_path=output_path,
        direction=args.direction,
        delta=args.delta,
        inverse=inverse,
    )


class IntegerShearingCorrect:
    def __init__(self,
                 source_path: Path,
                 output_path: Path,
                 direction: str = 'x',
                 delta: int = 1,
                 inverse: bool = False,
                 start: int = None,
                 end: int = None):

        self.source_path = source_path
        self.output_path = output_path

        self.start = start if start is not None else 0
        self.end = end if end is not None else -1

        self.direction = direction
        if self.direction not in ['x', 'y']:
            raise ValueError('Direction of shearing must be specified')

        self.inverse = inverse
        if self.inverse:
            logging.warning('start and end indices are not used for inverse correction')
            self.start = 0
            self.end = -1

        self.delta = delta
        assert type(self.delta) is int, 'Integer shearing factor must be an integer'

        self.input_file = InputFile(str(source_path))

        if self.start == 0 and self.end == -1:
            self.image_shape = self.input_file.shape
        else:
            self.image_shape = (self.end - self.start,) + self.input_file.shape[1:]

        self.corrected_shape = self._get_corrected_shape()
        self.image_data_corrected = None

        self.image_data_corrected = np.zeros(self.corrected_shape, dtype=np.uint16)

        if self.inverse:
            self.inverse_correct()
        else:
            self.forward_correct()

        self.save()

    def _get_corrected_shape(self):
        # the input image shape is (z, y, x)
        inverse_factor = -1 if self.inverse else 1
        if self.direction == 'x':
            corrected_shape = (self.image_shape[0],
                               self.image_shape[1],
                               self.image_shape[2] + inverse_factor * self.image_shape[0] * np.abs(self.delta))

        elif self.direction == 'y':
            corrected_shape = (self.image_shape[0],
                               self.image_shape[1] + inverse_factor * self.image_shape[0] * np.abs(self.delta),
                               self.image_shape[2])
        else:
            raise ValueError('Direction of shearing must be specified')

        return corrected_shape

    def inverse_correct(self):
        for z in range(self.image_shape[0]):
            if self.direction == "x":
                if self.delta > 0:
                    x_start = z * self.delta
                    x_end = x_start + self.corrected_shape[2]
                else:
                    x_end = self.image_shape[2] + z * self.delta
                    x_start = x_end - self.corrected_shape[2]
                self.image_data_corrected[z, :, :] = self.input_file[z, :, x_start:x_end]
            elif self.direction == "y":
                if self.delta > 0:
                    y_start = z * self.delta
                    y_end = y_start + self.corrected_shape[1]
                else:
                    y_end = self.image_shape[1] + z * self.delta
                    y_start = y_end - self.corrected_shape[1]
                self.image_data_corrected[z, :, :] = self.input_file[z, y_start:y_end, :]
            else:
                raise ValueError('Direction of shearing must be specified')

    def forward_correct(self):
        """
        Get corrected shape of image data
        """
        for z in range(self.image_shape[0]):
            if self.direction == 'x':
                if self.delta > 0:
                    x_start = z * self.delta
                    x_end = x_start + self.image_shape[2]
                else:
                    x_end = self.corrected_shape[2] + z * self.delta
                    x_start = x_end - self.image_shape[2]

                self.image_data_corrected[z, :, x_start:x_end] = self.input_file[self.start + z, :, :]

            elif self.direction == 'y':
                if self.delta > 0:
                    y_start = z * self.delta
                    y_end = y_start + self.image_shape[1]
                else:
                    y_end = self.corrected_shape[1] + z * self.delta
                    y_start = y_end - self.image_shape[1]

                self.image_data_corrected[z, y_start:y_end, :] = self.input_file[self.start + z, :, :]
            else:
                raise ValueError('Direction of shearing must be specified')

    def save(self):
        """
        Save corrected image data
        """
        tifffile.imsave(str(self.output_path), self.image_data_corrected)


if __name__ == "__main__":
    main()
