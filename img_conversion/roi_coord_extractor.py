from argparse import ArgumentParser
from pathlib import Path
from read_roi import read_roi_zip


def _init_outfile(out_path):
    with out_path.open(mode="w", encoding="utf-8") as outfile:
        fields = ["name","x1", "y1","x2","y2"]
        outfile.write(",".join(fields))
        outfile.write("\n")
        
def extract_lines(zip_path, append_to_fpath=None):
    roi = read_roi_zip(zip_path)
    
    if append_to_fpath:
        out_path = append_to_fpath
        if not out_path.is_file():
            _init_outfile(out_path)
    else:
        out_path = zip_path.parent.joinpath(zip_path.stem + ".txt")
        _init_outfile(out_path)
        
    for key, roi_item in roi.items():
        
        if roi_item["type"] == "line":
            x1 = int(roi_item["x1"])
            y1 = int(roi_item["y1"])
            x2 = int(roi_item["x2"])
            y2 = int(roi_item["y2"])
            
            fields = [key, x1, y1, x2, y2]
            
        with out_path.open(mode="a", encoding="utf-8") as outfile:
            outfile.write(",".join(map(str, fields)))
            outfile.write("\n")
            
def main(dir_path, out_path):
    zip_paths = sorted(list(dir_path.glob("*.zip")))
    
    for zip_path in zip_paths:
        extract_lines(zip_path, out_path)
    
if __name__ == "__main__":
    parser = ArgumentParser()
    
    
    parser.add_argument("-d" "--d", 
                        action="store",
                        type=str,
                        dest="dir_path",
                        help="ROI dir path"
                        )
    
    parser.add_argument("-o" "--out", 
                        action="store",
                        type=str,
                        dest="out_path",
                        help="OUT path"
                        )
    
    
    args, unknown = parser.parse_known_args()
    
    dir_path = Path(args.dir_path)
    out_path = Path(args.out_path)

    main(dir_path, out_path)