# =============================================================================
# UNPACK TIFF STACK
#
#
# Filippo Maria Castelli
# castelli@lens.unifi.it
# =============================================================================

from argparse import ArgumentParser
from pathlib import Path
from skimage import io as skio
from tqdm import tqdm
import numpy as np
import logging
import tifffile

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def main():
    parser = ArgumentParser()

    parser.add_argument(
        "-d",
        type=str,
        default="/mnt/NASone3/castelli/test.png",
        action="store",
        dest="input_path",
        help="Input file path")

    parser.add_argument(
        "-o",
        dest="output_path",
        action="store",
        default="",
        help="output directory path")

    parser.add_argument(
        "-a",
        dest="add_channel",
        action="store_true",
        default=False,
        help="add empty channel")
    args = parser.parse_args()

    input_fpath = Path(args.input_path)
    output_path = Path(args.output_path) if args.output_path != "" else None

    add_empty_channel = args.add_channel

    tu = TiffStackUnpacker(
        input_fpath=input_fpath,
        output_path=output_path,
        add_empty_channel=add_empty_channel
    )


class TiffStackUnpacker:
    def __init__(self,
                 input_fpath: Path,
                 output_path: Path = None,
                 add_empty_channel: bool = False):

        self.input_fpath = input_fpath
        if output_path is None:
            self.output_path = self.input_fpath.parent.joinpath(self.input_fpath.stem)
        else:
            self.output_path = output_path

        self.output_path.mkdir(exist_ok=True)

        self.add_empty_channel = add_empty_channel

        self.unpack_stack()

    def unpack_stack(self):
        img_stack = tifffile.imread(str(self.input_fpath))
        if self.add_empty_channel:
            # img_stack = np.moveaxis(img_stack, 1, -1)
            img_stack = np.pad(img_stack, ((0, 0), (0, 1), (0, 0), (0, 0)), mode="constant")
        for idx, img in enumerate(tqdm(img_stack)):
            filename = format(idx, '05d')
            output_fpath = self.output_path.joinpath(f"{filename}.tif")
            tifffile.imsave(str(output_fpath), data=img)


if __name__ == "__main__":
    main()
