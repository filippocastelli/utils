from pathlib import Path
from argparse import ArgumentParser

from tqdm import tqdm
from img_to_stack import img_to_stack

from multiprocessing import Pool, cpu_count
from functools import partial

from tqdm import tqdm

def main():
    parser = ArgumentParser(
        description="Convert series of images in subdirectories to stacks using a subdirectory pattern")
    parser.add_argument(
        "-d",
        type=str,
        default="/mnt/ssd1/experiments/results",
        action="store",
        dest="results_path",
        help="results_path")

    parser.add_argument(
        "-p",
        dest="pattern",
        action="store",
        type=str,
        default="test_heatmaps",
        help="pattern for stack-containing directory names")
    
    parser.add_argument(
        "-m",
        dest="multithread",
        action="store_true",
        help="enable multithread")
    
    parser.add_argument(
        "-t",
        dest="threads",
        action="store",
        type=int,
        default=None,
        help="number of threads")
    
    args = parser.parse_args()
    results_path = Path(args.results_path)
    
    subdirs = [path for path in results_path.rglob("*") if path.is_dir()]
    stack_subdirs = [path for path in subdirs if args.pattern in path.name]
    
    if args.multithread:
        print("starting multiprocessing...")
        mappable_partial = partial(_img_to_stack_subdir, bypass_check=False)
        threads = args.threads if args.threads is not None else cpu_count()
        with Pool(threads) as pool:
            n_steps = len(stack_subdirs)
            with tqdm(total = n_steps) as pbar:
                for i, _ in enumerate(pool.imap_unordered(mappable_partial, stack_subdirs)):
                    pbar.update()
    else:
        for path in tqdm(stack_subdirs):
            _img_to_stack_subdir(path)
    

def _img_to_stack_subdir(subdir_path, bypass_check=False):
    path_name = subdir_path.name + ".tif"
    output_path = subdir_path.joinpath(path_name)
    img_to_stack(subdir_path, output_path, bypass_check=bypass_check)
    
        
if __name__ == "__main__":
    main()