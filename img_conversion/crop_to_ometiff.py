from argparse import ArgumentParser
from pathlib import Path
import numpy as np
import tifffile


def main():
    
    parser = ArgumentParser()
    
    parser.add_argument(
        "-d", "-dir",
        dest="input_dir",
        action="store",
        type=str,
        default=str(Path.cwd()))
    
    args = parser.parse_args()
    input_dir = Path(args.input_dir)
    
    crop_dirs = input_dir.glob("crop*")
    crop_dirs = [path for path in crop_dirs if path.is_dir()]
    
    for crop_dir in crop_dirs:
        
        out_fpath = crop_dir.joinpath(crop_dir.name + ".tiff")
        
        img_dict = _get_img_dict(crop_dir)
        _save_img_dict(img_dict, out_fpath)

def _get_img_dict(crop_dir):
    
    tiff_file_fpaths = list(crop_dir.glob("*.tiff"))
    chan_path_dict = {fpath.name.split("_")[1] : fpath for fpath in tiff_file_fpaths}
    chan_img_dict = {channel : tifffile.imread(str(fpath)) for channel, fpath in chan_path_dict.items()}
    
    return chan_img_dict

def _save_img_dict(img_dict, out_path):
    
    img_list = list(img_dict.values())
    img_array = np.stack(img_list, axis=-1)
    
    channel_metadata =  {
        "Channel": {
            "Name": list(img_dict.keys())}}
    
    tifffile.imwrite(str(out_path), img_array,
                     metadata=channel_metadata)
    

    
if __name__ == "__main__":
    main()
