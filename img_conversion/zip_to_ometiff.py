from argparse import ArgumentParser
from pathlib import Path
import yaml

import pyometiff
import zetastitcher
from pyometiff import OMETIFFWriter
import numpy as np


def main():
    parser = ArgumentParser(description='Convert a JP2 zip to an OME-TIFF stack')
    parser.add_argument('--input', '-i', type=str, required=True, help='Input zip file')
    parser.add_argument("--cfg", "-c", type=str, required=True, help="Path to the config file")
    parser.add_argument("--output", "-o", type=str, required=True, help="Output directory")
    parser.add_argument("--start", "-s", type=int, default=0, help="Starting index")
    parser.add_argument("--end", "-e", type=int, default=0, help="Ending index")

    args = parser.parse_args()
    input_fpath = Path(args.input)
    cfg_fpath = Path(args.cfg)
    output_fpath = Path(args.output)

    if not input_fpath.exists():
        raise FileNotFoundError(f"Input file {input_fpath} does not exist")
    if not cfg_fpath.exists():
        raise FileNotFoundError(f"Config file {cfg_fpath} does not exist")
    if not output_fpath.exists():
        output_fpath.mkdir(parents=True, exist_ok=True)

    zip_to_ometiff = ZipToOmetiff(input_fpath=input_fpath,
                                  cfg_fpath=cfg_fpath,
                                  output_fpath=output_fpath,
                                  start=args.start,
                                  end=args.end)


class ZipToOmetiff:
    """
    Convert a JP2 zip to an OME-TIFF stack
    """

    def __init__(self,
                 input_fpath: Path,
                 cfg_fpath: Path,
                 output_fpath: Path,
                 dimension_order: str = 'ZTCYX',
                 compression: str = 'deflate',
                 start: int = 0,
                 end: int = -1):

        self.input_fpath = input_fpath
        self.cfg_fpath = cfg_fpath
        self.output_dir = output_fpath

        self.dimension_order = dimension_order
        self.compression = compression

        self.inputfile = zetastitcher.InputFile(str(self.input_fpath))
        self.cfg = self.parse_cfg()

        # check if start end end are valid
        if start < 0:
            start = 0
        if end < 0:
            end = self.inputfile.shape[0]
        if end < start:
            raise ValueError("end must be greater than start")
        self.start = start
        self.end = end

        output_name = self.input_fpath.stem if self.start == 0 and self.end == self.inputfile.shape[0] else f"{self.input_fpath.stem}_{self.start}_{self.end}"
        self.output_fpath = output_fpath.joinpath(output_name + '.ome.tiff')

        self.data = self.get_data()
        self.ometiff_config = self.get_ometiff_config()

        self.write_ometiff()

    def get_data(self) -> np.ndarray:
        """
        Get the data, reshape it and return it as a numpy array
        """
        data = self.inputfile[self.start:self.end]
        # data is in the shape (z, y, x) so we need to reshape it to (z, 1, 1, y, x)
        data = np.reshape(data, (self.end - self.start, 1, 1, *data.shape[1:]))

        return data

    def parse_cfg(self) -> dict:
        """
        Parse the config file and return a dict
        """
        with open(self.cfg_fpath, 'r') as f:
            cfg = yaml.safe_load(f)
        return cfg

    def get_ometiff_config(self) -> dict:
        """
        Get the OME-TIFF config as a dict
        """
        return {
            "PhysicalSizeX": self.cfg['PhysicalSizeX'],
            "PhysicalSizeXUnit": self.cfg['PhysicalSizeXUnit'],
            "PhysicalSizeY": self.cfg['PhysicalSizeY'],
            "PhysicalSizeYUnit": self.cfg['PhysicalSizeYUnit'],
            "PhysicalSizeZ": self.cfg['PhysicalSizeZ'],
            "PhysicalSizeZUnit": self.cfg['PhysicalSizeZUnit'],
            "Channels": {
                str(self.cfg['ExcitationWavelength']): {
                    "Name": str(self.cfg['ExcitationWavelength']),
                    "SamplesPerPixel": 1,
                    "ExcitationWavelength": self.cfg['ExcitationWavelength'],
                    "ExcitationWavelengthUnit": self.cfg['ExcitationWavelengthUnit'],
                }
            }
        }

    def write_ometiff(self):
        """
        Write the OME-TIFF stack
        """
        ometiff_writer = OMETIFFWriter(
            fpath=self.output_fpath,
            dimension_order=self.dimension_order,
            array=self.data,
            metadata=self.ometiff_config,
            compression=self.compression,
        )
        ometiff_writer.write()


if __name__ == '__main__':
    main()
