# =============================================================================
# IMG_TO_STACK
#
#
# Filippo Maria Castelli
# castelli@lens.unifi.it
# =============================================================================

from argparse import ArgumentParser
from pathlib import Path
from skimage import io as skio
import numpy as np
from tqdm import tqdm
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def main():
    parser = ArgumentParser()
    
    parser.add_argument(
        "-d",
        type=str,
        default="/mnt/NASone3/castelli/ippocampo_lut_modificata",
        action="store",
        dest="input_path",
        help="Input file path")
    
    parser.add_argument(
        "-o",
        dest="output_path",
        action="store",
        default=str(Path.cwd().joinpath("out")),
        help="output directory path")

    args = parser.parse_args()
    
    out_path = Path(args.output_path)
    out_path.mkdir(exist_ok=True, parents=True)
    input_fpath = Path(args.input_path)
    convert_to_png(input_fpath, out_path)


def _sanitize_extension(ext_str):
    if ext_str[0] != ".":
        ext_str = "."+ext_str
    return ext_str

def convert_to_png(img_path,
                   out_path):
    img_path_list = list(img_path.glob("*.tif"))

    for i, imgpath in enumerate(tqdm(img_path_list)):
        out_fpath = out_path.joinpath(imgpath.stem + ".png")
        img = skio.imread(imgpath, plugin="pil")
        skio.imsave(out_fpath, img) 

    
if __name__ == "__main__":
    main()
