# =============================================================================
# IMG SORT
#
# sort script for chaotic data coming fom Bioretics
#
# Filippo Maria Castelli
# castelli@lens.unifi.it
# =============================================================================

from pathlib import Path
from argparse import ArgumentParser
import shutil

def main():
    
    parser = ArgumentParser()
    parser.add_argument(
        "-d",
        "--input_dir",
        action="store",
        type=str,
        dest="ximage_input_path",
        default="/mnt/NASone3/castelli/2pe_ximages",
        help="Specify images location",
        )
    
    parser.add_argument(
        "-o",
        "--output",
        action="store",
        type=str,
        dest="out_path_str",
        default="/mnt/NASone3/castelli/2pe_ximages/sorted",
        help="Specify output directory",
        )
    
    args = parser.parse_args()
    
    ximage_path = Path(args.ximage_input_path)
    output_path = Path(args.out_path_str)
    
    _, _, _ = img_sort(
        input_path=ximage_path,
        output_path=output_path)


def read_lines(fpath, cdir=None):
    with fpath.open(mode="r") as infile:
        content = [Path(line.strip()) for line in infile.readlines()]
        
    if cdir is not None:
        content = [cdir.joinpath(fname) for fname in content]
        
    return content

def cp_files(filelist, out_dir):
    for fpath in filelist:
        fname = fpath.name
        to_path = out_dir.joinpath(fname)
        shutil.copy(str(fpath),str(to_path))
            
def img_sort(input_path, output_path):
    """
    Find images in unordered Bioretics hell.
    find "rgb" subdirs, find corresponding train.txt, validation.txt, test.txt
    copy images to out_
    Parameters
    ----------
    input_path : TYPE
        DESCRIPTION.
    output_path : TYPE
        DESCRIPTION.

    Returns
    -------
    out_paths : TYPE
        DESCRIPTION.

    """
    
    out_train_path = output_path.joinpath("train")
    out_validation_path = output_path.joinpath("val")
    out_test_path = output_path.joinpath("test")
    
    out_train_path.mkdir(parents=True, exist_ok=True)
    out_validation_path.mkdir(parents=True, exist_ok=True)
    out_test_path.mkdir(parents=True, exist_ok=True)
    
    
    rgb_dirs = [directory for directory in input_path.rglob("*") if directory.is_dir() and directory.name == "rgb"]
    
    for i, rgb_dir in enumerate(rgb_dirs):
        
        parent_dir = rgb_dir.parent
        
        train_txt_path = parent_dir.joinpath("train.txt")
        test_txt_path = parent_dir.joinpath("test.txt")
        validation_txt_path = parent_dir.joinpath("validation.txt")
        
        train_fnames = read_lines(train_txt_path, cdir=rgb_dir)
        val_fnames = read_lines(validation_txt_path, cdir=rgb_dir)
        test_fnames = read_lines(test_txt_path, cdir=rgb_dir)
        
        cp_files(train_fnames, out_train_path)
        cp_files(val_fnames, out_validation_path)
        cp_files(test_fnames, out_test_path)
        
    return out_train_path, out_validation_path, out_test_path
            
        
if __name__ == "__main__":
    main()

