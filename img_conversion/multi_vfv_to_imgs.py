from pathlib import Path
from argparse import ArgumentParser

from tqdm import tqdm
import numpy as np
from zetastitcher import VirtualFusedVolume
import tifffile


def main():
    parser = ArgumentParser(
        description="Unpack two virtualfusedvolumes to single images")

    parser.add_argument(
        "-sf1",
        type=str,
        default="/mnt/ssd1/experiments/results",
        action="store",
        dest="stitchfile_1_path",
        help="stitchfile 1 path")

    parser.add_argument(
        "-sf2",
        type=str,
        default="/mnt/ssd1/experiments/results",
        action="store",
        dest="stitchfile_2_path",
        help="stitchfile 2 path")

    parser.add_argument(
        "-o",
        dest="output_path",
        action="store",
        type=str,
        default=None,
        help="output path")

    args = parser.parse_args()

    sf1_path = Path(args.stitchfile_1_path)
    sf2_path = Path(args.stitchfile_2_path)
    output_path = Path(args.output_path)
    output_path.mkdir(exist_ok=True)
    multivfvtoimgs(sf1_path, sf2_path, output_path)


def multivfvtoimgs(sf1_path: Path,
                   sf2_path: Path,
                   output_path: Path):
    vfv1 = VirtualFusedVolume(str(sf1_path))
    vfv2 = VirtualFusedVolume(str(sf2_path))

    assert vfv1.shape == vfv2.shape, "different vfv shapes"

    for idx, img in enumerate(tqdm(vfv1)):
        img_list = [img, vfv2[idx]]

        out_img = np.stack(img_list, axis=-1)
        out_img_fpath = output_path.joinpath(f"{idx}.tif")

        tifffile.imsave(str(out_img_fpath), out_img)


if __name__ == "__main__":
    main()
