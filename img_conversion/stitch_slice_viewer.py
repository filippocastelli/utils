from pathlib import Path
from zetastitcher import VirtualFusedVolume as vfv
from argparse import ArgumentParser
import tifffile
def main():
    parser = ArgumentParser()
    parser.add_argument("--stitchfile", type=str, action="store", dest="stitch_file_path", help="Stitch File Path")
    parser.add_argument("-s", "--slice", type=int, action="store", dest="slice", help="slice")

    args = parser.parse_args()

    stitch_file_path = Path(args.stitch_file_path)
    vol_slice = args.slice

    virtual_volume = vfv(str(stitch_file_path))

    if virtual_volume.shape[0] < vol_slice:
        raise ValueError("slice outside bounds")
    sliced_volume = virtual_volume[vol_slice]
    out_file_path = stitch_file_path.parent.joinpath("slice_{}.tiff".format(vol_slice))
    tifffile.imsave(str(out_file_path), sliced_volume)

if __name__ == "__main__":
    main()
