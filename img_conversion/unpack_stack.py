# =============================================================================
# UNPACK_STACKS
#
#
# Filippo Maria Castelli
# castelli@lens.unifi.it
# =============================================================================

from argparse import ArgumentParser
from pathlib import Path
from skimage import io as skio
import numpy as np
from tqdm import tqdm
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

SUPPORTED_EXT = ["png", "tif", "tiff", "jpg", "jpeg"]
SUPPORTED_EXT_PT = ["." + ext for ext in SUPPORTED_EXT]


def main():
    parser = ArgumentParser()

    parser.add_argument(
        "-d",
        type=str,
        default="/mnt/NASone3/castelli/test.png",
        action="store",
        dest="input_path",
        help="Input file path")

    parser.add_argument(
        "-o",
        dest="output_path",
        action="store",
        default=str(Path.cwd().joinpath("out")),
        help="output directory path")

    parser.add_argument(
        "-f",
        dest="img_format",
        action="store",
        default=".png",
        help="Extension of output images")

    args = parser.parse_args()

    out_path = Path(args.output_path)
    out_path.mkdir(exist_ok=True, parents=True)

    input_fpath = Path(args.input_path)

    if input_fpath.is_dir():
        files_to_unpack = [file for file in input_fpath.glob("*.*") if file.suffix in SUPPORTED_EXT_PT]
        for fpath in files_to_unpack:
            out_path_file = out_path.joinpath(fpath.stem)
            out_path_file.mkdir(exist_ok=True, parents=True)
            stack_to_img(fpath, out_path_file, img_format=args.img_format)
    elif input_fpath.is_file():
        stack_to_img(input_fpath, out_path, img_format=args.img_format)
    else:
        logger.error("No input file was provided")


def _sanitize_extension(ext_str):
    if ext_str[0] != ".":
        ext_str = "." + ext_str
    return ext_str


def stack_to_img(img_path,
                 out_path,
                 img_format=".png"):
    logging.info("Unpacking {}".format(str(img_path)))
    img_stack = skio.imread(img_path, plugin="pil")
    for i, frame in enumerate(tqdm(img_stack)):
        filename = format(i, '05d') + img_format
        filepath = out_path.joinpath(filename)
        skio.imsave(filepath, frame)


if __name__ == "__main__":
    main()
