from pathlib import Path
from argparse import ArgumentParser

from tqdm import tqdm
from multiprocessing import Pool, cpu_count
from functools import partial
from shutil import copy

def main():
    parser = ArgumentParser(
        description="Look for images in the directory tree with matching extension and copy them to a destination folder")
    parser.add_argument(
        "-d",
        type=str,
        default="/mnt/ssd1/experiments/results",
        action="store",
        dest="results_path",
        help="results_path")
    
    parser.add_argument(
        "-o",
        type=str,
        default="/mnt/ssd1/experiments/results",
        action="store",
        dest="output_path",
        help="output_path")

    parser.add_argument(
        "-e",
        type=str,
        default="tif",
        action="store",
        dest="extension",
        help="extension")
    
    parser.add_argument(
        "-m",
        dest="multithread",
        action="store_true",
        help="enable multithread")
    
    parser.add_argument(
        "-t",
        dest="threads",
        action="store",
        type=int,
        default=None,
        help="number of threads")
    
    args = parser.parse_args()
    results_path = Path(args.results_path)
    output_dir_path = Path(args.output_path)

    files_to_copy = [fpath for fpath in results_path.rglob("*") if fpath.is_file() and _get_extension(fpath) == args.extension]
    mappable_partial = partial(_copy_to_dest, target_dir_path=output_dir_path)
    
    if args.multithread:
        print("starting multiprocessing...")
        threads = args.threads if args.threads is not None else cpu_count()
        with Pool(threads) as pool:
            n_steps = len(files_to_copy)
            with tqdm(total = n_steps) as pbar:
                for i, _ in enumerate(pool.imap_unordered(mappable_partial, files_to_copy)):
                    pbar.update()
    else:
        for path in tqdm(files_to_copy):
            mappable_partial(path)
    
def _get_extension(fpath):
    return fpath.suffix.split(".")[-1]
    
def _copy_to_dest(fpath, target_dir_path):
    output_path = target_dir_path.joinpath(fpath.name)
    copy(str(fpath), str(output_path))
    
    
        
if __name__ == "__main__":
    main()