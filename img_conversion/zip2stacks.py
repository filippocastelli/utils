# =============================================================================
# ZIP2STACKS
#
# combining a folder of images to a single stack
#
# Filippo Maria Castelli
# castelli@lens.unifi.it
# =============================================================================
from pathlib import Path
from argparse import ArgumentParser
import logging
from tqdm import tqdm
import tifffile
from zetastitcher import InputFile

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def main():
    parser = ArgumentParser(
        description="Automatically create tiff stacks from zip archives of JP2 images",
        epilog="Author: Filippo Maria Castelli <castelli@lens.unifi.it>")
    
    parser.add_argument(
        "-d",
        type=str,
        default="/mnt/ssd1/l638_f690_zip",
        action="store",
        dest="input_path",
        help="Directory with .zip archives")
    
    parser.add_argument(
        "-o",
        dest="output_path",
        action="store",
        default="/mnt/ssd1/zip_outs",
        help="Output directory")
        
    args = parser.parse_args()
    
    out_path = Path(args.output_path)
    

    out_path.mkdir(exist_ok=True, parents=True)
        
    in_dir = Path(args.input_path)
    
    archive_pathlist = sorted(list(in_dir.glob("*.zip")))
    
    for i, archive_path in enumerate(tqdm(archive_pathlist)):
        # archive_name = "".join(archive_path.name.split(".")[:-1]).replace(".", "_")
        archive_name = archive_path.stem
        logging.info("Extracting stack {} out of {}: {}".format(i+1, len(archive_pathlist), archive_name))        
        in_file = InputFile(str(archive_path))
        img_stack = in_file.whole()
        logging.info("Writing stack...")
        tifffile.imwrite(out_path.joinpath(archive_name+".tiff"), img_stack)

def _sanitize_extension(ext_str):
    if ext_str[0] != ".":
        ext_str = "."+ext_str
    return ext_str

if __name__ == "__main__":
    main()
