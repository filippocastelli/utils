from pathlib import Path
from argparse import ArgumentParser
import numpy as np
from skimage import io as skio
import matplotlib.pyplot as plt
from image_registration import chi2_shift, cross_correlation_shifts
from tqdm import tqdm

def main():
    parser = ArgumentParser(description="Multichannel xy shift estimation",
                            epilog="Author: Filippo Maria Castelli <castelli@lens.unifi.it>")
    
    parser.add_argument(
        "-d",
        "--crop_dir",
        action="store",
        type=str,
        dest="crop_dir",
        default=None,
        help="crop dir")
    
    parser.add_argument(
        "-p",
        "--pivot",
        action="store",
        type=int,
        dest="pivot_channel",
        default=None,
        help="pivot channel")
    
    parser.add_argument(
        "-m",
        "--maxoff",
        type=int,
        dest="maxoff",
        default=None,
        help="max pixel shift")
    
    parser.add_argument(
        "-c",
        "--channels",
        nargs="+",
        help="channels", 
        default=[405, 488, 561, 638],
        dest="channels",
        action="store")
    
    args = parser.parse_args()
    
    crop_path = Path(args.crop_dir)
    pivot = int(args.pivot_channel)
    maxoff = int(args.maxoff) if args.maxoff is not None else None
    channels = args.channels
    channels = [int(channel) for channel in channels]
    
    OffsetCalculator(crop_path=crop_path,
                     channels=channels,
                     pivot=pivot,
                     maxoff=maxoff)

    
class OffsetCalculator:
    
    def __init__(self,
                 crop_path,
                 channels,
                 pivot,
                 maxoff=None):
        
        self.crop_path = Path(crop_path)
        self.crop_name = self.crop_path.name.split("_")[1]
        self.channels = channels
        self.channels_paths = {channel: self.crop_path.joinpath("crop_{}_{}.tiff".format(channel, self.crop_name)) for channel in self.channels}
        self.channel_imgs = {channel: skio.imread(channel_path, plugin="pil") for channel, channel_path in self.channels_paths.items()}
        
        self.maxoff = int(maxoff)
        
        self.pivot_channel = pivot
        self.non_pivot_channels = set(self.channels)
        self.non_pivot_channels.remove(self.pivot_channel)
        self.non_pivot_channels = list(self.non_pivot_channels)
        
        self.offsets = self.calc_offsets_to_pivot(self.pivot_channel)
        self.multiplot_offsets(to_int=True)
        
    def calc_offsets_to_pivot(self, pivot_channel):
        non_pivot = set(self.channels)
        non_pivot.remove(self.pivot_channel)
        return {channel: self._get_offsets(channel, pivot_channel) for channel in non_pivot}
        
        
    def chi2_offsets(self, channel_0, channel_1, return_errors=False):
        img_0 = np.array(self.channel_imgs[channel_0])
        img_1 = np.array(self.channel_imgs[channel_1])
        
        x_offsets_chi2 = []
        y_offsets_chi2 = []
        x_errors_chi2 = []
        y_errors_chi2 = []
        
        for idx, img in enumerate(img_0):
            xoff, yoff, exoff, eyoff = chi2_shift(img, img_1[idx], return_error=True)
            
            if self.maxoff is not None:
                xoff = np.clip(xoff, -self.maxoff, self.maxoff)
                yoff = np.clip(yoff, -self.maxoff, self.maxoff)
                
            x_offsets_chi2.append(xoff)
            y_offsets_chi2.append(yoff)
            x_errors_chi2.append(exoff)
            y_errors_chi2.append(eyoff)

        if return_errors:
            return x_offsets_chi2, y_offsets_chi2, x_errors_chi2, y_errors_chi2
        else:
            return x_offsets_chi2, y_offsets_chi2
        
    def correlation_offsets(self, channel_0, channel_1):
        img_0 = np.array(self.channel_imgs[channel_0])
        img_1 = np.array(self.channel_imgs[channel_1])
        x_offsets_cc = []
        y_offsets_cc = []
        for idx, img in enumerate(tqdm(img_0)):
            # maxoff is bugged
            try:
                xoff, yoff = cross_correlation_shifts(img, img_1[idx])
            except ValueError:
                xoff = 0
                yoff = 0
            if self.maxoff is not None:
                xoff = np.clip(xoff, -self.maxoff, self.maxoff)
                yoff = np.clip(yoff, -self.maxoff, self.maxoff)
            x_offsets_cc.append(xoff)
            y_offsets_cc.append(yoff)
            
        return x_offsets_cc, y_offsets_cc
    
    def _get_offsets(self, channel_0, channel_1):
        return {"chi2": self.chi2_offsets(channel_0, channel_1),
                "cross_correlation": self.correlation_offsets(channel_0, channel_1)}

    
    def plot_offsets(self, channel, ax_chi2, ax_cc, to_int=True):
        channel_offset_dict = self.offsets[channel]
        
        #plot ch2
        ax_chi2.set_title(r'$\chi^2$ shift of ch. {}nm wrt ch. {}nm '.format(channel, self.pivot_channel))
        x_chi2 = channel_offset_dict["chi2"][0]
        y_chi2 = channel_offset_dict["chi2"][1]
        
        if to_int:
            x_chi2 = self._toint(x_chi2)
            y_chi2 = self._toint(y_chi2)
            
        ax_chi2.plot(x_chi2, color="red", label="estimated shift on x")
        ax_chi2.plot(y_chi2, color="blue", label="estimated shift on y")
        
        ax_chi2.set_xlabel("stack slice [n]")
        ax_chi2.set_ylabel("estimated shift [px]")
        
        ax_chi2.legend()
        
        #plot ch2
        ax_cc.set_title("Cross-correlation shift of ch. {}nm wrt ch. {}nm ".format(channel, self.pivot_channel))
        x_cc = channel_offset_dict["cross_correlation"][0]
        y_cc = channel_offset_dict["cross_correlation"][1]
        
        if to_int:
            x_cc = self._toint(x_cc)
            y_cc = self._toint(y_cc)
            
        ax_cc.plot(x_cc, color="red", label="estimated shift on x")
        ax_cc.plot(y_cc, color="blue", label="estimated shift on y")
        
        ax_cc.set_xlabel("stack slice [n]")
        ax_cc.set_ylabel("estimated shift [px]")
        
        ax_cc.legend()
        
    def multiplot_offsets(self, to_int=True):
        fig, axs = plt.subplots(ncols=len(self.non_pivot_channels), nrows=2, figsize=(20,10))
        
        
        for idx, channel in enumerate(tqdm(self.non_pivot_channels)):
            self.plot_offsets(channel, axs[0][idx], axs[1][idx], to_int=True)
            
        fig.suptitle("Crop n. {}\nEstimated shift wrt ch. {}nm".format(self.crop_name, self.pivot_channel))
        plt.show(block=True)
        
    @staticmethod
    def _toint(list_):
        return [int(elem) for elem in list_]
        


if __name__ == "__main__":
    main()
