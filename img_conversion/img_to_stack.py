# =============================================================================
# IMG_TO_STACK
#
#
# Filippo Maria Castelli
# castelli@lens.unifi.it
# =============================================================================
import sys
from argparse import ArgumentParser
from pathlib import Path
from skimage import io as skio
import tifffile
import numpy as np

SUPPORTED_EXTS = ["png", "tif", "tiff"]
def main():
    parser = ArgumentParser()
    
    parser.add_argument(
        "-d",
        type=str,
        default="/mnt/NASone3/castelli/test.png",
        action="store",
        dest="input_path",
        help="Input file path")
    
    parser.add_argument(
        "-o",
        dest="output_path",
        action="store",
        default=str(Path.cwd().joinpath("out")),
        help="output directory path")

    args = parser.parse_args()
    
    out_path = Path(args.output_path)
    
    if out_path.is_dir():
        out_path.mkdir(exist_ok=True, parents=True)
    else:
        out_dir = out_path.parents[0]
        out_dir.mkdir(exist_ok=True, parents=True)
        
    in_dir = Path(args.input_path)
    
    img_stack = img_to_stack(in_dir, out_path, img_format=args.img_format)


def _sanitize_extension(ext_str):
    if ext_str[0] != ".":
        ext_str = "."+ext_str
    return ext_str
    
def _get_extension(fpath):
    return fpath.suffix.split(".")[-1]

def _ask_confirm(path, bypass=False):
    print("The folder contains images with mixed extensions\n{}\ncontinue?".format(str(path)))
    
    val = ""
    
    while val.lower() not in ["y", "n"]:
        val = input("y/n:")
        
    if val.lower() == "n" or bypass:
        sys.exit()
    elif val.lower() == "y":
        pass
        
    
def img_to_stack(img_path,
                 out_path=None,
                 img_format=".tif",
                 save_stack=True,
                 bypass_check=False):
    """
    Generate an image stack from all elements of a directory
    Save generated file to tif stack

    Parameters
    ----------
    img_path : pathlib Path
        Input directory path.
    img_format : str, optional
        Format of input images. The default is ".tif".

    Returns
    -------
    img_stack : ndarray
        Output Stack.
    """
    
    # extension = _sanitize_extension(img_format)
    # img_path_list = list(img_path.glob("*"+extension))
    
    img_path_list = [path for path in img_path.glob("*.*") if _get_extension(path) in SUPPORTED_EXTS]
    
    extension_list = [_get_extension(path) for path in img_path_list]
    
    if len(set(extension_list)) > 1:
        _ask_confirm(img_path, bypass_check)
        
    img_path_list.sort()
    img_list = []
    for img_path in img_path_list:
        img_list.append(skio.imread(img_path, plugin="pil"))
    
    first_img_shape = img_list[0].shape
    assert all(img.shape == first_img_shape for img in img_list), "Images have different shapes"
    
    img_stack = np.array(img_list)
    
    if save_stack == True:
        tifffile.imwrite(out_path, img_stack)
        
    return img_stack


    
if __name__ == "__main__":
    main()
