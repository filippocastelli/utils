# =============================================================================
# CUT_HALF
#
#
# Filippo Maria Castelli
# castelli@lens.unifi.it
# =============================================================================

from argparse import ArgumentParser
from pathlib import Path
from skimage import io as skio
import numpy as np
from tqdm import tqdm
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def main():
    parser = ArgumentParser()
    
    parser.add_argument(
        "-d",
        type=str,
        default="/mnt/ssd1/stack_laira/rgb_16bit_lut_modificata",
        action="store",
        dest="input_path",
        help="Input file path")
    
    parser.add_argument(
        "-o",
        dest="output_path",
        action="store",
        default=str(Path.cwd().joinpath("out")),
        help="output directory path")
    
    parser.add_argument(
        "-p", "--overlap",
        dest="overlap",
        type=int,
        action="store",
        default=100,
        help="pixel overlap between the two sections")

    args = parser.parse_args()
    
    out_path = Path(args.output_path)
    out_path.mkdir(exist_ok=True, parents=True)
    input_fpath = Path(args.input_path)
    cut_half(input_fpath, out_path)
    
    overlap_px = args.overlap


def _sanitize_extension(ext_str):
    if ext_str[0] != ".":
        ext_str = "."+ext_str
    return ext_str

def cut_half(img_path,out_path, overlap_px=100):
    img_path_list = list(img_path.glob("*.tif"))
    img_path_list_str = list(map(str, img_path_list))
    img_path_list_str = [path + "\n" for path in img_path_list_str]
    print("Found images:")
    print(*img_path_list_str)

    print("Converting images...")
    for i, imgpath in enumerate(tqdm(img_path_list)):
        # print("converting {}".format(imgpath))
        # out_fpath = out_path.joinpath(imgpath.stem + ".png")
        img = skio.imread(imgpath, plugin="pil")
        width, height, channels = img.shape
        cutoff = width // 2
        
        img_upper = img[:cutoff + overlap_px, :, :]
        img_lower = img[cutoff - overlap_px :, :, :]
        
        assert img_lower.shape[0] + img_upper.shape[0] - 2*overlap_px == img.shape[0]
        
        fname = imgpath.stem + "_overlap_{}px".format(overlap_px)
        out_fpath_lower = out_path.joinpath(fname+"_A.png")
        out_fpath_upper = out_path.joinpath(fname+"_B.png")
        skio.imsave(out_fpath_lower, img_upper)
        skio.imsave(out_fpath_upper, img_lower) 

    
if __name__ == "__main__":
    main()
