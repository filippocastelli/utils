from argparse import ArgumentParser
from pathlib import Path

from unpack_stack import stack_to_img


def main():
    parser = ArgumentParser()

    parser.add_argument("-d", "--dataset",
                        help="dataset location",
                        action="store",
                        type=str,
                        dest="dataset_fpath")

    parser.add_argument("-o", "--output",
                        help="output",
                        action="store",
                        type=str,
                        dest="output_fpath")

    args = parser.parse_args()
    dataset_in_path = Path(args.dataset_fpath)
    output_path = Path(args.output_fpath)

    unpack_dataset(in_fpath=dataset_in_path,
                   out_fpath=output_path)


def unpack_dataset(in_fpath: Path,
                   out_fpath: Path,
                   sections: tuple = ("train", "test", "val"),
                   subsections: tuple = ("frames", "masks"),
                   extension: str = ".tif"):

    for section in sections:
        section_path = in_fpath.joinpath(section)
        out_section_path = out_fpath.joinpath(section)
        out_section_path.mkdir(exist_ok=True, parents=True)
        for subsection in subsections:
            subsection_path = section_path.joinpath(subsection)
            out_subsection_path = out_section_path.joinpath(subsection)
            out_subsection_path.mkdir(exist_ok=True, parents=True)
            assert subsection_path.is_dir(), f"{str(subsection_path)}is not a valid directory"
            img_fpath = subsection_path.joinpath(subsection+extension)
            assert img_fpath.is_file(), f"{str(img_fpath)} is not a valid file"

            stack_to_img(img_path=img_fpath,
                         out_path=out_subsection_path,
                         img_format=".tiff")


if __name__ == "__main__":
    main()