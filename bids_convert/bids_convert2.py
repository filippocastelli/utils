from pathlib import Path
import logging
import json
import datetime

from typing import List, Union, Any, Tuple, Dict
from argparse import ArgumentParser

import yaml
from tqdm import tqdm

from pyometiff import OMETIFFWriter, OMETIFFReader
from zetastitcher import InputFile, FileMatrix

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def main():
    parser = ArgumentParser()

    parser.add_argument("-c",
                        "--cfg",
                        type=str,
                        dest="cfg_path",
                        action="store",
                        help="config path")

    args = parser.parse_args()
    cfg_path = Path(args.cfg_path)

    converter = BIDSConverter(
        config_path=cfg_path)
    converter.run()
class BIDSConverter:

    def __init__(self,
                 config_path: Path):

        self.config_path = Path(config_path)

        self.paths_cfg, self.run_cfg, self.meta_cfg, self.channels_cfg, self.yml_dict = self.parse_configs(
            self.config_path)
        self.dataset_path = self.paths_cfg["bids_dataset_path"]

        if not self.dataset_path.exists():
            self.dataset_path.mkdir(parents=True)

    @classmethod
    def parse_configs(cls, config_path: Path) -> Tuple[Dict[str, Path], dict, dict, dict, dict]:
        yml_dict = cls._read_yml(config_path)

        meta_cfg = cls._parse_metadata_cfg(yml_dict["meta_cfg"])

        run_cfg_dict = yml_dict["run_cfg"]
        run_cfg = {
            "write_chunk_json": cls._get_key(run_cfg_dict, "write_chunk_json", required=False, default=True, key_format="bool"),
            "write_chunk_tiff": cls._get_key(run_cfg_dict, "write_chunk_tiff", required=False, default=True, key_format="bool"),
            "use_bigtiff": cls._get_key(run_cfg_dict, "use_bigtiff", required=False, default=False, key_format="bool"),
            "write_chunk_xml": cls._get_key(run_cfg_dict, "write_chunk_xml", required=False, default=False, key_format="bool"),
            "compression": cls._get_key(run_cfg_dict, "compression", required=False, default=None),
        }

        paths_cfg_dict = yml_dict["paths_cfg"]
        paths_cfg = {
            "bids_dataset_path": Path(paths_cfg_dict["bids_dataset_path"])
        }

        channel_cfg_dict = yml_dict["channels_cfg"]
        channels_cfg = cls._parse_channel_cfg(channel_cfg_dict)

        return paths_cfg, run_cfg, meta_cfg, channels_cfg, yml_dict

    @classmethod
    def _parse_metadata_cfg(cls, json_cfg_dict: dict) -> dict:

        # see for referece
        # https://bids-specification--881.org.readthedocs.build/en/881/04-modality-specific-files/10-microscopy.html#microscopy-metadata-sidecar-json

        meta_cfg = {
            # Device Hardware
            "Manufacturer": cls._get_key(json_cfg_dict, "Manufacturer"),
            "ManufacturersModelName": cls._get_key(json_cfg_dict, "ManufacturersModelName"),
            "DeviceSerialNumber": cls._get_key(json_cfg_dict, "DeviceSerialNumber"),
            "StationName": cls._get_key(json_cfg_dict, "StationName"),
            "SoftwareVersions": cls._get_key(json_cfg_dict, "SoftwareVersions"),
            "InstitutionName": cls._get_key(json_cfg_dict, "InstitutionName"),
            "InstitutionAddress": cls._get_key(json_cfg_dict, "InstitutionAddress"),
            "InstitutionalDepartmentName": cls._get_key(json_cfg_dict, "InstitutionalDepartmentName"),
            # Image Acquisition
            "PixelSize": cls._get_key(json_cfg_dict, "PixelSize", key_format="int_list", required=True),
            "PixelSizeUnits": cls._get_key(json_cfg_dict, "PixelSizeUnits", required=True),
            "Immersion": cls._get_key(json_cfg_dict, "Immersion"),
            "NumericalAperture": cls._get_key(json_cfg_dict, "NumericalAperture", key_format="float"),
            "Magnification": cls._get_key(json_cfg_dict, "Magnification", key_format="float"),
            "ImageAcquisitionProtocol": cls._get_key(json_cfg_dict, "ImageAcquisitionProtocol"),
            "OtherAcquisitionParameters": cls._get_key(json_cfg_dict, "OtherAcquisitionParameters"),
            # Sample
            "BodyPart": cls._get_key(json_cfg_dict, "BodyPart"),
            "BodyPartDetails": cls._get_key(json_cfg_dict, "BodyPartDetails"),
            "BodyPartDetailsOntology": cls._get_key(json_cfg_dict, "BodyPartDetailsOntology"),
            "SampleEnvironment": cls._get_key(json_cfg_dict, "SampleEnvironment"),
            "SampleEmbedding": cls._get_key(json_cfg_dict, "SampleEmbedding"),
            "SampleFixation": cls._get_key(json_cfg_dict, "SampleFixation"),
            # "SampleStaining": cls._get_key(json_cfg_dict, "SampleStaining"),
            # "SamplePrimaryAntibody": cls._get_key(json_cfg_dict, "SamplePrimaryAntibody"),
            # "SampleSecondaryAntibody": cls._get_key(json_cfg_dict, "SampleSecondaryAntibody"),
            "SliceThickness": cls._get_key(json_cfg_dict, "SliceThickness", key_format="float"),
            "TissueDeformationScaling": cls._get_key(json_cfg_dict, "TissueDeformationScaling", key_format="float"),
            "SampleExtractionProtocol": cls._get_key(json_cfg_dict, "SampleExtractionProtocol"),
            "SampleExtractionInstitution": cls._get_key(json_cfg_dict, "SampleExtractionInstitution"),
            # Chunk Transformations
            "ChunkTransformationMatrix": cls._get_key(json_cfg_dict, "ChunkTransformationMatrix",
                                                      key_format="int_list_list"),
            "ChunkTransformationMatrixAxis": cls._get_key(json_cfg_dict, "ChunkTransformationMatrixAxis",
                                                          key_format="str_list",
                                                          required=True if "ChunkTransformationMatrix" in json_cfg_dict else False),
            # OME-XML Metadata
            "Species": cls._get_key(json_cfg_dict, "Species", default="Homo sapiens"),
            "AcquisitionDate": cls._get_key(json_cfg_dict, "AcquisitionDate", key_format="datetime", required=True),
            "SubjectID": cls._get_key(json_cfg_dict, "SubjectID", required=True),

            # BIDS Naming Conventions
            "Modality": cls._get_key(json_cfg_dict, "Modality", required=True),
            "SampleID": cls._get_key(json_cfg_dict, "SampleID", required=True),

        }
        return meta_cfg

    @classmethod
    def _parse_channel_cfg(cls, channel_cfg_dict: dict) -> dict:
        channel_cfg = {}
        # channels = list(channel_cfg_dict.keys())
        for key, channel_dict in channel_cfg_dict.items():
            channel_cfg[key] = {
                "Channel": str(key),
                "Name": cls._get_key(channel_dict, "Name", required=True),
                "Fluor": cls._get_key(channel_dict, "Fluor", required=True),
                "path": cls._get_key(channel_dict, "path", required=True, key_format="path"),
                "ExcitationWavelength": cls._get_key(channel_dict, "ExcitationWavelength", key_format="float"),
                "EmissionWavelength": cls._get_key(channel_dict, "EmissionWavelength", key_format="float"),
                "ExcitationWavelengthUnit": cls._get_key(channel_dict, "ExcitationWavelengthUnit", required=False, default="nm"),
                "EmissionWavelengthUnit": cls._get_key(channel_dict, "EmissionWavelengthUnit", required=False, default="nm"),
            }

        return channel_cfg

    @staticmethod
    def _get_key(dictionary: dict, key: str, key_format: str = "str", default=None, required=False) -> Union[
        Union[str, int, float, bool, List[int], List[float], List[str]], Any]:
        """
        Get the value of a key in a dictionary and format it.
        """
        if key in dictionary:
            if key_format == "str":
                return str(dictionary[key])
            elif key_format == "int":
                return int(dictionary[key])
            elif key_format == "float":
                return float(dictionary[key])
            elif key_format == "bool":
                return bool(dictionary[key])
            elif key_format == "int_list":
                return [int(i) for i in dictionary[key]]
            elif key_format == "float_list":
                return [float(i) for i in dictionary[key]]
            elif key_format == "str_list":
                return [str(i) for i in dictionary[key]]
            elif key_format == "int_list_list":
                return [[int(i) for i in j] for j in dictionary[key]]
            elif key_format == "datetime":
                return datetime.datetime.strptime(dictionary[key], "%Y%m%d").isoformat()
            elif key_format == "path":
                return Path(dictionary[key])
            else:
                raise ValueError("Unknown key format: {}".format(key_format))
        else:
            if required:
                raise ValueError(f"Required key {key} not found in dictionary")
            else:
                return default

    @staticmethod
    def _read_yml(yml_fpath: Path) -> dict:
        """read a yml file, return a dict"""
        with yml_fpath.open(mode="r") as rfile:
            yml_dict = yaml.safe_load(rfile)
        return yml_dict

    def run(self):
        for channel_name, channel_dict in tqdm(self.channels_cfg.items()):
            # read stitchfile
            channel_path = channel_dict["path"]
            stitchfile_path = channel_path.joinpath("stitch.yml")
            if not stitchfile_path.exists():
                raise FileNotFoundError(f"Stitchfile {stitchfile_path} not found")

            #inputfile = InputFile(stitchfile_path)
            filematrix = FileMatrix(stitchfile_path)

            #fused_shape = inputfile.shape

            tiff_files = list(channel_path.glob("x_*_y_*_z*_*.tiff"))
            tiff_files.sort()

            # create json metadata file
            exclude_meta_keys = [
                "AcquisitionDate",
                "SubjectID",
                "Modality",
                "SampleID",
            ]

            output_path = self.dataset_path / "rawdata" / "sub-{}".format(self.meta_cfg["SubjectID"]) / "ses-{}".format(
                self.meta_cfg["Modality"]) / "micr"
            if not output_path.exists():
                output_path.mkdir(parents=True)

            json_meta_dict = {key: self.meta_cfg[key] for key in self.meta_cfg.keys() if key not in exclude_meta_keys}
            json_meta_dict["SampleStaining"] = channel_dict["Name"]

            # json filename template
            # sub-<label>[_ses-<label>]_sample-<label>[_acq-<label>][_stain-<label>][_run-<index>][_chunk-<index>]_<suffix>.<extension>
            filename_fields = {
                "subject_id": self.meta_cfg["SubjectID"],
                "session_id": self.meta_cfg["Modality"],
                "sample_id": self.meta_cfg["SampleID"],
                "channel_id": channel_dict["Name"],
                "modality_suffix": self.meta_cfg["Modality"],
            }
            json_filename = "sub-{subject_id}_ses-{session_id}_sample-{sample_id}_stain-{channel_id}_{modality_suffix}.json".format(
                **filename_fields)
            json_fpath = output_path.joinpath(json_filename)
            self._write_dict_to_json(json_meta_dict, json_fpath)

            for idx, tiff_filepath in enumerate(tqdm(tiff_files)):
                # filename template
                # sub-<label>[_ses-<label>]_sample-<label>[_acq-<label>][_stain-<label>][_run-<index>][_chunk-<index>]_<suffix>.<extension>
                filename_fields["idx"] = idx
                filename = "sub-{subject_id}_ses-{session_id}_sample-{sample_id}_stain-{channel_id}_chunk-{idx}_{" \
                           "modality_suffix}.ome.tif".format(**filename_fields)
                tiff_output_path = output_path.joinpath(filename)

                # chunk json
                if self.run_cfg["write_chunk_json"]:
                    chunk_transformation_matrix = self._get_chuntransformationmatrix(chunk_fpath=tiff_filepath,
                                                                                     filematrix=filematrix)
                    chunk_json_dict = json_meta_dict.copy()
                    chunk_json_dict.update({
                        "PixelSize": self.meta_cfg["PixelSize"],
                        "PixelSizeUnits": self.meta_cfg["PixelSizeUnits"],
                        "ChunkTransformationMatrix": chunk_transformation_matrix,
                        "ChunkTransformationMatrixAxis": ["Z", "Y", "X"],
                    })
                    chunk_json_fname = "sub-{subject_id}_ses-{session_id}_sample-{sample_id}_stain-{channel_id}_chunk-{idx}_{modality_suffix}.json".format(**filename_fields)
                    chunk_json_fpath = output_path.joinpath(chunk_json_fname)
                    self._write_dict_to_json(chunk_json_dict, chunk_json_fpath)

                if self.run_cfg["write_chunk_tiff"]:
                    # preparing OMETIFF metadata
                    ome_meta_dict = {
                        "Name": filename,
                        "AcquisitionDate": self.meta_cfg["AcquisitionDate"],
                        "PhysicalSizeX": self.meta_cfg["PixelSize"][2],
                        "PhysicalSizeY": self.meta_cfg["PixelSize"][1],
                        "PhysicalSizeZ": self.meta_cfg["PixelSize"][0],
                        "PhysicalSizeXUnit": "µm",
                        "PhysicalSizeYUnit": "µm",
                        "PhysicalSizeZUnit": "µm",
                        "Channels": {
                            channel_dict["Name"]: {
                                "Name": channel_dict["Name"],
                                "SamplesPerPixel": 1,
                                "ExcitationWavelength": channel_dict["ExcitationWavelength"],
                                "ExcitationWavelengthUnit": channel_dict["ExcitationWavelengthUnit"],
                                "EmissionWavelength": channel_dict["EmissionWavelength"],
                                "EmissionWavelengthUnit": channel_dict["EmissionWavelengthUnit"],
                                "Fluor": channel_dict["Fluor"],
                            }
                        }
                    }

                    # read tiff file
                    omereader = OMETIFFReader(fpath=tiff_filepath)
                    tiff_arr, old_meta_dict, old_meta_xml = omereader.read()

                    # write new tiff file
                    omewriter = OMETIFFWriter(fpath=tiff_output_path,
                                              array=tiff_arr,
                                              metadata=ome_meta_dict,
                                              dimension_order="ZYX",
                                              arr_shape=list(tiff_arr.shape).copy(),
                                              bigtiff=self.run_cfg["use_bigtiff"],
                                              compression=self.run_cfg["compression"])
                    omewriter.write()

                    if self.run_cfg["write_chunk_xml"]:
                        xml_fpath = tiff_output_path.with_suffix(".xml")
                        omewriter.write_xml(xml_fpath=xml_fpath)


    @staticmethod
    def _write_dict_to_json(data: dict, out_path: Path, remove_none_values: bool = True) -> None:
        """dump a dict to json"""
        if remove_none_values:
            data_cpy = {key: value for key, value in data.items() if value is not None}
        else:
            data_cpy = data.copy()
        with out_path.open(mode="w") as json_out:
            json.dump(data_cpy, json_out, indent=2, sort_keys=True)

    @staticmethod
    def _get_chuntransformationmatrix(chunk_fpath: Path,
                                      filematrix: FileMatrix) -> list:

        chunk_key = "./" + chunk_fpath.name
        return [
                [0.0, 0.0, 0.0, filematrix.data_frame.loc[chunk_key]["Xs"]],
                [0.0, 0.0, 0.0, filematrix.data_frame.loc[chunk_key]["Ys"]],
                [0.0, 0.0, 0.0, filematrix.data_frame.loc[chunk_key]["Zs"]],
                [0.0, 0.0, 0.0, 1.0]]


if __name__ == "__main__":
    main(
    )
