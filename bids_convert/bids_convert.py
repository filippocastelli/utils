from pathlib import Path
import logging
import yaml
import json
import datetime
from argparse import ArgumentParser

from tqdm import tqdm
import numpy as np

from pyometiff import OMETIFFReader, OMETIFFWriter
from collections import OrderedDict

logger = logging.getLogger()
logger.setLevel(logging.INFO)

dataset_path = Path("/mnt/DATA/DANDI")


def setup_yaml():
  """add represented for OrderedDicts to pyYAML
  see https://stackoverflow.com/a/8661021 """
  represent_dict_order = lambda self, data:  self.represent_mapping('tag:yaml.org,2002:map', data.items())
  yaml.add_representer(OrderedDict, represent_dict_order)
   
setup_yaml()


def main():
    
    parser = ArgumentParser()
    
    parser.add_argument("-o", "--out",
                        type=str,
                        action="store",
                        dest="out_path",
                        help="Output path")
    
    parser.add_argument("-c", "--cfg",
                        type=str,
                        action="store",
                        dest="cfg_path",
                        help="Config path")
    
    args = parser.parse_args()
    
    out_path = Path(args.out_path)
    cfg_path = Path(args.cfg_path)
    
    yml_config = _load_yaml(cfg_path)
    
    bids_converter = BIDSConverter(yml_config, out_path)
   #test comment
    

class BIDSConverter:
    
    def __init__(self,
                 config,
                 out_path,
                 use_u=True):
        
        self.global_config_dict = config["global_config"]
        self.out_path = Path(out_path)
        
        self._parse_global_config(self.global_config_dict)
        self.channels_dict = self._parse_channels(self.global_config_dict["channels"])
        self.staingroups_dict = config["staingroups"]
        self.slices_dict = self._parse_slices(config["slices"])
        self.slices_paths = [self.dataset_path.joinpath(Path(slice_dict["path"])) for slice_dix, slice_dict in self.slices_dict.items()]
        
        self.use_u = use_u
        for slice_path in self.slices_paths:
            self._check_path_structure(slice_path)
        self._gen_bids()
        
    def _parse_global_config(self, cfgdict):
        
        self.dataset_path = Path(cfgdict["dataset_path"])
        self.reslice = cfgdict["reslice"]
        self.Modality = cfgdict["Modality"]
        
        self.PixelSize = cfgdict["PixelSize"]
        self.PhysicalSizeX, self.PhysicalSizeY, self.PhysicalSizeZ = self.PixelSize
        self.PixelSizeUnit = cfgdict["PixelSizeUnit"]
        self.PhysicalSizeXUnit, self.PhysicalSizeYUnit, self.PhysicalSizeZUnit = self.PixelSizeUnit, self.PixelSizeUnit, self.PixelSizeUnit
        
        self.BitDepth = cfgdict["BitDepth"]
        
        self.Subject = cfgdict["Subject"]
        self.Species = cfgdict["Species"]
        self.BodyPart = cfgdict["BodyPart"]
        self.BodyPartDetails = cfgdict["BodyPartDetails"]
        self.Environment = cfgdict["Environment"]

    
    def _check_path_structure(self, slice_path):
        for n_slice_path in self.slices_paths:
            for channel, channel_dict in self.channels_dict.items():
                channel_dir = n_slice_path.joinpath(self._get_channel_str(channel_dict)).joinpath("left")
                is_dir = channel_dir.is_dir()
                is_not_empty = any(channel_dir.iterdir())
                
                if is_dir and is_not_empty:
                    pass
                else:
                    raise ValueError("channel dir {} is not present".format(channel_dir))

    def _get_channel_str(self, channel_dict):
        if self.use_u:
            pxsizeunit = "um"
        else:
            pxsizeunit = self.PixelSizeUnit
        channel_str = "l{}_f{}_{}{}_resliced".format(
            channel_dict["ExcitationWavelength"],
            channel_dict["EmissionWavelength"],
            self.reslice,
            pxsizeunit
            )
        return channel_str
        
    def _parse_channels(self, channeldict):
        channels_dict = {}
        for ch_id, (excitation_wavelength, emission_wavelength) in channeldict.items():
            channels_dict[ch_id] = {"ExcitationWavelength": excitation_wavelength,
                                    "EmissionWavelength": emission_wavelength,
                                    "ExcitationWavelengthUnit": "nm",
                                    "EmissionWavelengthUnit": "nm"}
            
        return channels_dict
    
    def _parse_slices(self, config_slices):
        for slice_id in config_slices.keys():
            config_slices[slice_id]["path"] = self.dataset_path.joinpath(config_slices[slice_id]["path"])
            config_slices[slice_id]["channel_config"] = self.staingroups_dict[config_slices[slice_id]["channel_config"]]
            config_slices[slice_id]["acquisition_datetime"] = datetime.datetime(*config_slices[slice_id]["acquisition_datetime"]).isoformat()
        self.sample_slices = list(config_slices.keys())
        return config_slices

    def _gen_OMETIFF_dict(self, sample_slice, channel_id):
        
        ometiff_dict = {
            "Name": self._get_namestring(sample_slice, channel_id),
            "PhysicalSizeX": self.PhysicalSizeX,
            "PhysicalSizeXUnit" : self.PhysicalSizeXUnit,
            "PhysicalSizeY": self.PhysicalSizeY,
            "PhysicalSizeYUnit" : self.PhysicalSizeYUnit,
            "PhysicalSizeZ": self.PhysicalSizeZ,
            "PhysicalSizeZUnit" : self.PhysicalSizeZUnit,
            "Channels": self._get_OMETIFF_channel_dict(sample_slice, channel_id),
            "AcquisitionDate": self.slices_dict[sample_slice]["acquisition_datetime"]
            }
        return ometiff_dict
    
    def _get_OMETIFF_channel_dict(self, sample_slice, channel_id):
        channel_config_dict = self.slices_dict[sample_slice]["channel_config"]
        channel_config_dict = channel_config_dict[channel_id]
        ch_dict = self.channels_dict[channel_id]
        ch_name = str(ch_dict["ExcitationWavelength"])+"nm"
        channeldict = {
            # "Name": ch_name,
            "Name": channel_config_dict["SampleStaining"],
            "SamplesPerPixel": 1,
            "ExcitationWavelength": ch_dict["ExcitationWavelength"],
            "EmissionWavelength": ch_dict["EmissionWavelength"],
            "ExcitationWavelengthUnit": "nm",
            "EmissionWavelengthUnit": "nm",
            "Fluor": channel_config_dict["Fluorophore"],
            "AcquisitionMode": self.Modality,
            # "ContrastMethod": "Fluorescence"
        }
        return {ch_name: channeldict}
        
    def _gen_json_dict(self, sample_slice, channel_id):
        channel_config_dict = self.slices_dict[sample_slice]["channel_config"]
        channel_config_dict = channel_config_dict[channel_id]
        json_dict = {
            "PixelSize": self.PixelSize,
            "Species": self.Species,
            "BodyPart": self.BodyPart,
            "BodyPartDetails": self.BodyPartDetails,
            "SampleStaining": channel_config_dict["SampleStaining"],
            "Channels": 1,
            "BitDepth": 16
            }
        return json_dict
    
    def _get_namestring(self, sample_slice, channel_id, chunk_id=None):
        channel_config_dict = self.slices_dict[sample_slice]["channel_config"]
        channel_config_dict = channel_config_dict[channel_id]
        
        subject = "sub-"+self.Subject
        sample = "sample-"+self.BodyPartDetails+"S"+"{:02d}".format(sample_slice)
        stain = "stain-"+channel_config_dict["SampleStaining"]
        if chunk_id is not None:
            chunk_id_str = "{:02d}".format(chunk_id)
            chunk = "chunk-"+ chunk_id_str
        else:
            chunk = None
        modality = self.Modality
        
        name_fields = (subject, sample, stain, chunk, modality)
        name_fields = list(filter(None, name_fields))
        
        #name_str = str.join("-", name_fields)
        name_str = "_".join(name_fields)
        return name_str

    def _gen_bids(self):
        for slice_id in tqdm(self.sample_slices):
            channel_config_dict = self.slices_dict[slice_id]["channel_config"]
            channel_ids = list(channel_config_dict.keys())
            slice_fpath =  self.slices_dict[slice_id]["path"]
            for channel_id in channel_ids:
                channel_dir_str = self._get_channel_str(self.channels_dict[channel_id])
                chunk_dict = self._get_chunk_dict(slice_fpath.joinpath(channel_dir_str).joinpath("left"))
                
                json_dict = self._gen_json_dict(slice_id, channel_id)
                ometiff_dict = self._gen_OMETIFF_dict(slice_id, channel_id)
                
                slice_out_path = self.out_path.joinpath("sub-"+self.Subject).joinpath("microscopy")
                json_namestr = self._get_namestring(slice_id, channel_id) + ".json"
                json_fpath = slice_out_path.joinpath(json_namestr)
                
                json_fpath.parent.mkdir(parents=True, exist_ok=True)
                self._write_json(json_fpath, json_dict)
                
                for chunk_id, chunk_fpath in tqdm(chunk_dict.items()):
                    chunk_namestr = self._get_namestring(slice_id, channel_id, chunk_id) + ".ome.tiff"
                    chunk_out_fpath = slice_out_path.joinpath(chunk_namestr)
                    
                    ometiff_reader = OMETIFFReader(fpath=chunk_fpath)
                    array, old_metadata, old_metadata_xml = ometiff_reader.read()
                    
                    dimension_order = "ZYX"
                    ometiff_writer = OMETIFFWriter(fpath=chunk_out_fpath,
                                                   dimension_order=dimension_order,
                                                   array=array,
                                                   metadata=ometiff_dict)
                    ometiff_writer.write()

    @staticmethod
    def _get_chunk_dict(fpath):
         chunk_fpath_list = sorted(list(fpath.glob("*_l.tiff")))
         return {idx : chunk_fpath_list[idx] for idx in range(len(chunk_fpath_list))}
        
    @staticmethod
    def _read_son(json_path):
        with json_path.open(mode="r") as json_file:
            data = json.load(json_file)
        return data
    
    @staticmethod
    def _write_json(out_path, data):
        with out_path.open(mode="w") as json_out:
            json.dump(data, json_out)

def _load_yaml(yml_path):
    """ load a yml file and return a dict"""
    with yml_path.open(mode="r") as rfile:
        try:
            yml_dict = yaml.safe_load(rfile)
        except yaml.YAMLError as exc:
            print(exc)
            raise ValueError(exc)
            
    return yml_dict


def _save_yaml(data, yml_path):
    """save a dict-like to yml"""
    with yml_path.open(mode="w") as dumpfile:
        yaml.dump(data, dumpfile,
                  default_flow_style=None,
                  line_break="\n")



if __name__ == "__main__":
    main()
    
