from argparse import ArgumentParser
from pathlib import Path
import yaml
import os
import re

from tqdm import tqdm


def main():
    parser = ArgumentParser(description='Organize marker gt to BIDS format')

    parser.add_argument("-c", "--config", help="config file", required=True)
    parser.add_argument("-o", "--output", help="output directory", required=True)
    parser.add_argument("-g", "--gt", help="ground truth directory", required=True)

    args = parser.parse_args()
    config_path = Path(args.config)
    output_path = Path(args.output)
    gt_path = Path(args.gt)

    bgtc = BIDSGTConverter(config_path, output_path, gt_path)
    bgtc.symlink_gt()

class BIDSGTConverter:
    def __init__(self,
                 config_path: Path,
                 output_path: Path,
                 gt_path: Path):
        self.config_path = Path(config_path)
        self.output_path = Path(output_path)
        self.gt_path = Path(gt_path)

        self.config = self.load_config()
        self.gt_list = self.get_gt_list()

    def load_config(self):
        with self.config_path.open(mode='r') as f:
            config = yaml.load(f, Loader=yaml.FullLoader)
        return config

    def get_gt_list(self):
        paths = []
        for dirpath, dirs, files in os.walk(self.gt_path):
            for filename in files:
                fpath = Path(dirpath).joinpath(filename)
                if fpath.suffix == '.marker':
                    paths.append(Path(fpath))
        return paths

    def symlink_gt(self):
        for gt_path in tqdm(self.gt_list):
            tif_path = gt_path.parent.joinpath(gt_path.stem)
            assert tif_path.exists(), f'{tif_path} does not exist'

            gt_path_str = str(gt_path)
            gt_slice = int(re.search( "(?<=slice_)\d+", gt_path_str)[0])
            _, pos_z, pos_y, pos_x, _, _, _, _ = gt_path.stem.split('_')

            pos_offset = 32

            pos_z = int(pos_z) + pos_offset
            pos_y = int(pos_y) + pos_offset
            pos_x = int(pos_x) + pos_offset

            pos_str = f"{str(pos_z)}-{str(pos_y)}-{str(pos_x)}"

            subject = self.config["Subject"]
            session = self.config["Modality"]
            sample = self.config["BodyPartDetails"]+"S"+"{:02d}".format(gt_slice)
            stain = "NeuN"
            base_fname = f"sub-{subject}_ses-{session}_sample-{sample}_stain-{stain}_{session}_pos-{pos_str}"
            marker_out_fname = f"{base_fname}.marker"
            tiff_out_fname = f"{base_fname}.tif"

            marker_out_path = self.output_path.joinpath(marker_out_fname)
            tiff_out_path = self.output_path.joinpath(tiff_out_fname)

            if marker_out_path.exists():
                # remove existing symlink
                os.remove(marker_out_path)

            if tiff_out_path.exists():
                # remove existing symlink
                os.remove(tiff_out_path)

            os.symlink(str(gt_path), str(marker_out_path))
            os.symlink(str(tif_path), str(tiff_out_path))


if __name__ == '__main__':
    main()
