from pathlib import Path
import logging
import json
import datetime
import os
import shutil

from typing import List
from argparse import ArgumentParser
# from multiprocessing import Pool, cpu_count
# from functools import partial
import subprocess

import yaml
import pandas as pd
from tqdm import tqdm

from pyometiff import OMETIFFWriter
from zetastitcher import InputFile, FileMatrix

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def main():
    parser = ArgumentParser()

    parser.add_argument("-o",
                        "--out",
                        type=str,
                        dest="out_path",
                        action="store",
                        help="output path")

    parser.add_argument("-c",
                        "--cfg",
                        type=str,
                        dest="cfg_path",
                        action="store",
                        help="config path")

    parser.add_argument("-t",
                        "--threads",
                        dest="threads",
                        type=int,
                        action="store",
                        default=None,
                        help="multiproc threads")

    args = parser.parse_args()

    out_path = Path(args.out_path)
    cfg_path = Path(args.cfg_path)
    threads = args.threads

    converter = BIDSConverter2(
        config_path=cfg_path,
        out_path=out_path,
        threads=threads)
    converter.run()


class BIDSConverter2:

    def __init__(self,
                 config_path: Path,
                 out_path: Path,
                 threads: int = None):

        self.config_path = Path(config_path)
        self.out_path = Path(out_path)

        self.threads = threads

        self.config_dict = self.parse_config(self.config_path)
        self.subject = self.config_dict["Subject"]

        self.slices = self.config_dict["run_cfg"]["slices"] if "slices" in self.config_dict["run_cfg"] else None

        self.acquisition_dirs = self.get_acquisition_dirs()
        self.acquisition_df = self.get_acquisition_df()

        self.write_json = self.config_dict["run_cfg"]["write_json"]
        self.create_symlinks_chunks = self.config_dict["run_cfg"]["create_symlinks_chunks"]
        self.create_symlinks_mips = self.config_dict["run_cfg"]["create_symlinks_mips"]
        self.create_symlinks_fused = self.config_dict["run_cfg"]["create_symlinks_fused"]
        self.write_ome_xml_chunks = self.config_dict["run_cfg"]["write_ome_xml_chunks"]
        self.write_ome_xml_mips = self.config_dict["run_cfg"]["write_ome_xml_mips"]
        self.write_ome_xml_fused = self.config_dict["run_cfg"]["write_ome_xml_fused"]
        self.inject_ome_xml_chunks = self.config_dict["run_cfg"]["inject_ome_xml_chunks"]
        self.inject_ome_xml_mips = self.config_dict["run_cfg"]["inject_ome_xml_mips"]
        self.inject_ome_xml_fused = self.config_dict["run_cfg"]["inject_ome_xml_fused"]
        self.ignore_existing_xml = self.config_dict["run_cfg"]["ignore_existing_xml"]
        # self.rename_chunks = self.config_dict["run_cfg"]["rename_chunks"]
        # self.rename_mips = self.config_dict["run_cfg"]["rename_mips"]
        # self.rename_fused = self.config_dict["run_cfg"]["rename_fused"]

        self.bftools_path = Path(self.config_dict["paths_cfg"]["bftools_path"])

        session = self.config_dict["Modality"]
        self.derivatives_path = self.out_path.joinpath(f"derivatives/sub-{self.subject}/ses-{session}/micr")
        self.derivatives_path.mkdir(exist_ok=True, parents=True)

        self.rawdata_path = self.out_path.joinpath(f"rawdata/sub-{self.subject}/ses-{session}/micr")
        self.rawdata_path.mkdir(parents=True, exist_ok=True)

    @classmethod
    def parse_config(cls, config_path: Path) -> dict:
        yml_dict = cls._read_yml(config_path)
        config_dict = {
            "dataset_path": Path(yml_dict["paths_cfg"]["dataset_path"]),
            "Modality": yml_dict["Modality"],
            "PixelSize": yml_dict["PixelSize"],
            "PixelSizeUnit": str(yml_dict["PixelSizeUnit"]),
            "BitDepth": int(yml_dict["BitDepth"]),
            "Subject": str(yml_dict["Subject"]),
            "Species": str(yml_dict["Species"]),
            "BodyPart": str(yml_dict["BodyPart"]),
            "BodyPartDetails": str(yml_dict["BodyPartDetails"]),
            "Environment": str(yml_dict["Environment"]),
            "Channels": yml_dict["Channels"],
            "bftools_path": Path(yml_dict["paths_cfg"]["bftools_path"]),
            "json_common": yml_dict["json_common"],
            "run_cfg": yml_dict["run_cfg"],
            "paths_cfg": yml_dict["paths_cfg"],
        }
        return config_dict

    @staticmethod
    def _read_yml(yml_fpath: Path) -> dict:
        """read a yml file, return a dict"""
        with yml_fpath.open(mode="r") as rfile:
            yml_dict = yaml.safe_load(rfile)
        return yml_dict

    def get_acquisition_dirs(self) -> List[Path]:
        """returns a list of acquisition dirs"""
        paths = list(fpath for fpath in self.config_dict["dataset_path"].glob("*_*_*_*_*_*_*") if fpath.is_dir())
        paths = [dirpath for dirpath in paths if self.subject in dirpath.name]
        if self.slices is not None:
            slices_str_list = ["_"+str(slice_)+"_" for slice_ in self.slices]
            paths = [dirpath for dirpath in paths if any(slice_str in dirpath.name for slice_str in slices_str_list)]
        return sorted(paths)

    def get_acquisition_df(self) -> pd.DataFrame:
        """returns acquisition dataframe"""
        dir_df = pd.DataFrame()

        for idx, acquisition_dir_path in enumerate(self.acquisition_dirs):
            dir_name = acquisition_dir_path.name

            date_str, subject, sample_str, _, left_channel_str, _, right_channel_str = dir_name.split("_")

            acquisition_date = datetime.datetime.strptime(date_str, "%Y%m%d").isoformat()
            left_channel_wavelength = int(left_channel_str)
            right_channel_wavelength = int(right_channel_str)

            tiff_left_path = acquisition_dir_path.joinpath("tiff_left")
            tiff_right_path = acquisition_dir_path.joinpath("tiff_right")

            zip_left_path = acquisition_dir_path.joinpath("zip_left")
            zip_right_path = acquisition_dir_path.joinpath("zip_right")

            stitchfile_left_path = tiff_left_path.joinpath("stitch.yml")
            stitchfile_right_path = tiff_right_path.joinpath("stitch.yml")

            mip_left_path = tiff_left_path.joinpath("mip.tiff")
            mip_right_path = tiff_left_path.joinpath("mip.tiff")

            fused_left_path = tiff_left_path.joinpath("fused.tiff")
            fused_right_path = tiff_right_path.joinpath("fused.tiff")

            entry_common = {
                "acquisition_path": acquisition_dir_path,
                "acquisition_path_str": str(acquisition_dir_path),
                "date": acquisition_date,
                "date_str": date_str,
                "subject": subject,
                "sample_str": sample_str,
                "sample_idx": int(sample_str)
            }

            entry_left = entry_common.copy()
            entry_left.update({
                "channel_str": left_channel_str,
                "channel_wavelength": left_channel_wavelength,
                "tiff_path": tiff_left_path,
                "zip_path": zip_left_path,
                "stitchfile_path": stitchfile_left_path,
                "mip_path": mip_left_path,
                "fused_path": fused_left_path,
                "camera": "left"
            })

            entry_right = entry_common.copy()
            entry_right.update({
                "channel_str": right_channel_str,
                "channel_wavelength": right_channel_wavelength,
                "tiff_path": tiff_right_path,
                "zip_path": zip_right_path,
                "stitchfile_path": stitchfile_right_path,
                "mip_path": mip_right_path,
                "fused_path": fused_right_path,
                "camera": "right"
            })

            dir_df = dir_df.append(entry_left, ignore_index=True)
            dir_df = dir_df.append(entry_right, ignore_index=True)

        return dir_df

    @staticmethod
    def _write_dict_to_json(data: dict, out_path: Path):
        """dump a dict to json"""
        with out_path.open(mode="w") as json_out:
            json.dump(data, json_out, indent=2, sort_keys=True)

    def get_base_namestring(self, row: pd.Series):
        """get a BIDS formatted name"""
        # sub-I48_ses-SPIM_sample-BrocaAreaS01_stain-NeuN_chunk-00_SPIM.ome.tiff

        subject = self.config_dict["Subject"]
        session = self.config_dict["Modality"]

        partdetails = self.config_dict["BodyPartDetails"]
        sample_idx = int(row["sample_idx"])
        sample = f"{partdetails}S{sample_idx:02d}"

        channel = row["channel_wavelength"]
        stain = self.config_dict["Channels"][channel]["Name"]
        name_str = f"sub-{subject}_ses-{session}_sample-{sample}_stain-{stain}_{session}"

        return name_str

    @staticmethod
    def namestring_to_chunk_namestring(namestring: str, chunk_idx: int = 0):
        split_namestr = namestring.split("_")
        split_namestr.insert(-1, f"chunk-{chunk_idx:02d}")
        return "_".join(split_namestr)

    @staticmethod
    def get_chunk_fpaths(dir_fpath: Path) -> List[Path]:
        """returns an ordered list of chunk fpaths"""
        return sorted(list(fpath for fpath in dir_fpath.glob("x_*_y_*_z_*_cam_*.ti*")))

    @staticmethod
    def get_tif_fpath(dir_fpath: Path, tif_name: str):
        return list(fpath for fpath in dir_fpath.glob(tif_name + "*"))[0]

    @staticmethod
    def filematrix_to_shape_dict(filematrix: list):
        shapedict = {
            Path(entry["filename"]).stem: [entry["nfrms"], entry["ysize"], entry["xsize"]] for entry in filematrix
        }
        return shapedict

    @staticmethod
    def _fix_stitchfile_extensions(stitchfile_path: Path):
        stitchfile_backup_path = stitchfile_path.parent.joinpath("stitch.yml.backup")

        if stitchfile_backup_path.is_file():
            stitchfile_backup_path = stitchfile_path.parent.joinpath(
                f"stitch_{str(datetime.datetime.now())}.yml.backup")

        shutil.copyfile(str(stitchfile_path), str(stitchfile_backup_path))

        with stitchfile_path.open(mode="r") as in_stitchfile:
            stitchfile_data = in_stitchfile.read()

        stitchfile_data = stitchfile_data.replace("cam_l.tiff", "cam_l.ome.tif")
        stitchfile_data = stitchfile_data.replace("cam_r.tiff", "cam_r.ome.tif")
        os.remove(str(stitchfile_path))
        with stitchfile_path.open(mode="w") as out_stitchfile:
            out_stitchfile.write(stitchfile_data)

    @staticmethod
    def create_tif_symlink(in_fpath: Path,
                           link_fpath: Path):
        if link_fpath.is_file():
            assert str(
                link_fpath.is_symlink()), f"{str(link_fpath)} seems to be a real file, not a symlink"
            os.remove(str(link_fpath))
            logger.info(f"removing {str(link_fpath)}")
        os.symlink(str(in_fpath), str(link_fpath))

    def run(self):
        # iterate over acquisition dirs
        for idx, row in tqdm(self.acquisition_df.iterrows(), total=self.acquisition_df.shape[0]):
            channel_config_dict = self.config_dict["Channels"][row["channel_wavelength"]]
            json_common_dict = self.config_dict["json_common"]
            acquisition_date = row["date"]
            namestring = self.get_base_namestring(row)

            fused_path = Path(row["fused_path"])
            mip_path = Path(row["mip_path"])
            stitchfile_path = Path(row["stitchfile_path"])

            # if self.rename_chunks:
            #    self._fix_stitchfile_extensions(stitchfile_path)

            filematrix = FileMatrix(str(stitchfile_path))

            json_dict = {
                "PixelSize": self.config_dict["PixelSize"],
                "PixelSizeUnits": "um",
                # "Species": self.config_dict["Species"],
                "BodyPart": self.config_dict["BodyPart"],
                "BodyPartDetails": self.config_dict["BodyPartDetails"],
                "Environment": self.config_dict["Environment"],
                "SampleStaining": channel_config_dict["Name"],
                # "Channels": 1,
                # "BitDepth": 16
            }
            json_dict.update(json_common_dict)

            chunk_fpaths = self.get_chunk_fpaths(row["tiff_path"])
            chunk_id_list = [(idx, chunk_fpath) for idx, chunk_fpath in enumerate(chunk_fpaths)]

            for chunk_id_list_elem in tqdm(chunk_id_list):
                chunk_idx, chunk_in_fpath = chunk_id_list_elem
                xml_fpath = chunk_in_fpath.with_suffix(".xml")

                if self.ignore_existing_xml and xml_fpath.is_file():
                    continue
                chunk_array_shape = self._get_stack_shape(chunk_in_fpath)

                # renamed_fpath = self._rename_to_ometiff(in_fpath=chunk_in_fpath,
                #                                        rename=self.rename_chunks)

                chunk_namestring = self.namestring_to_chunk_namestring(namestring, chunk_idx=chunk_idx)
                chunk_json_fpath = self.rawdata_path.joinpath(chunk_namestring + ".json")

                chunk_transofrm_matrix = self._get_chuntransformationmatrix(chunk_in_fpath, filematrix)

                chunk_json_dict = json_dict.copy()
                chunk_json_dict.update(chunk_transofrm_matrix)

                if self.write_json:
                    self._write_dict_to_json(chunk_json_dict, chunk_json_fpath)

                chunk_symlink_fpath = self.rawdata_path.joinpath(chunk_namestring + ".ome.tif")
                if self.create_symlinks_chunks:
                    self.create_tif_symlink(in_fpath=chunk_in_fpath,
                                            link_fpath=chunk_symlink_fpath)

                self._inject_ometiff_tags(out_path=chunk_in_fpath,
                                          channel_config_dict=channel_config_dict,
                                          bftools_path=self.bftools_path,
                                          array_shape=list(chunk_array_shape),
                                          dimension_order="ZYX",
                                          acquisition_date=acquisition_date,
                                          inject_tags=self.inject_ome_xml_chunks,
                                          write_ome_xml=self.write_ome_xml_chunks)

            # MIPS HANDLING
            # renamed_mip_fpath = self._rename_to_ometiff(in_fpath=mip_path, rename=self.rename_mips)
            mip_array_shape = self._get_stack_shape(mip_path)
            mip_out_path = self.derivatives_path.joinpath("mip")
            mip_out_path.mkdir(exist_ok=True, parents=True)
            mip_symlink_fpath = mip_out_path.joinpath(namestring + "_mip.ome.tif")
            if self.create_symlinks_mips:
                self.create_tif_symlink(in_fpath=mip_path,
                                        link_fpath=mip_symlink_fpath)

            self._inject_ometiff_tags(out_path=mip_path,
                                      channel_config_dict=channel_config_dict,
                                      bftools_path=self.bftools_path,
                                      array_shape=list(mip_array_shape),
                                      dimension_order="ZYX",
                                      acquisition_date=acquisition_date,
                                      inject_tags=self.inject_ome_xml_mips,
                                      write_ome_xml=self.write_ome_xml_mips)

            # FUSED HANDLING
            # renamed_fused_fpath = self._rename_to_ometiff(in_fpath=fused_path, rename=self.rename_fused)
            fused_array_shape = self._get_stack_shape(fused_path)
            fused_out_path = self.derivatives_path.joinpath("fused")
            fused_out_path.mkdir(exist_ok=True, parents=True)
            fused_symlink_fpath = fused_out_path.joinpath(namestring + "_fused.ome.tif")

            if self.create_symlinks_fused:
                self.create_tif_symlink(in_fpath=fused_path,
                                        link_fpath=fused_symlink_fpath)

            self._inject_ometiff_tags(out_path=fused_path,
                                      channel_config_dict=channel_config_dict,
                                      bftools_path=self.bftools_path,
                                      array_shape=list(fused_array_shape),
                                      dimension_order="ZYX",
                                      acquisition_date=acquisition_date,
                                      inject_tags=self.inject_ome_xml_fused,
                                      write_ome_xml=self.write_ome_xml_fused)

    # def write_ome_stacks(self):
    #     """write ometiff stacks"""
    #
    #     # iterate over acquisition folders
    #     for index, row in tqdm(self.acquisition_df.iterrows(), total=self.acquisition_df.shape[0]):
    #         channel_config_dict = self.config_dict["Channels"][row["channel_wavelength"]]
    #         json_common_dict = self.config_dict["json_common"]
    #         acquisition_date = row["date"]
    #
    #         #  writing acqusition BIDS json
    #         namestring = self.get_base_namestring(row)
    #         json_fpath = self.out_path.joinpath(namestring + ".json")
    #         json_dict = {
    #             "PixelSize": self.config_dict["PixelSize"],
    #             "PixelSizeUnits": "um",
    #             # "Species": self.config_dict["Species"],
    #             "BodyPart": self.config_dict["BodyPart"],
    #             "BodyPartDetails": self.config_dict["BodyPartDetails"],
    #             "Environment": self.config_dict["Environment"],
    #             "SampleStaining": channel_config_dict["Name"],
    #             # "Channels": 1,
    #             # "BitDepth": 16
    #         }
    #         json_dict.update(json_common_dict)
    #         self._write_dict_to_json(json_dict, out_path=json_fpath)
    #
    #         # stitch_path = row["tiff_path"].joinpath("stitch.yml")
    #         # stitch_dict = self._read_yml(stitch_path)
    #         # filematrix = stitch_dict["filematrix"]
    #         # shapedict = self.filematrix_to_shape_dict(filematrix)
    #
    #         chunk_fpaths = self.get_chunk_fpaths(row["tiff_path"])
    #
    #         # chunk_id_list = [(idx, chunk_fpath, shapedict[chunk_fpath.stem.replace(".ome", "")]) for idx,
    #         # chunk_fpath in enumerate(chunk_fpaths)]
    #
    #         chunk_id_list = [(idx, chunk_fpath) for idx, chunk_fpath in enumerate(chunk_fpaths)]
    #
    #         if self.threads is None:
    #             for chunk_id_list_elem in tqdm(chunk_id_list):
    #                 self.write_chunk(chunk_id_list_elem,
    #                                  namestring=namestring,
    #                                  channel_config_dict=channel_config_dict,
    #                                  acquisition_date=acquisition_date,
    #                                  bftools_path=self.config_dict["bftools_path"])
    #         else:
    #             if self.threads == -1:
    #                 threads = cpu_count()
    #             else:
    #                 threads = self.threads
    #
    #             mappable_partial = partial(self.write_chunk,
    #                                        namestring=namestring,
    #                                        channel_config_dict=channel_config_dict,
    #                                        acquisition_date=acquisition_date,
    #                                        bftools_path=self.config_dict["bftools_path"])
    #
    #             with Pool(threads) as pool:
    #                 n_steps = len(chunk_id_list)
    #                 with tqdm(total=n_steps) as pbar:
    #                     for i, _ in enumerate(pool.imap_unordered(mappable_partial, chunk_id_list)):
    #                         pbar.update()

    @staticmethod
    def _get_fname_no_extension(fpath: Path) -> str:
        fname_str = fpath.stem
        for ext in [
            ".ome",
            ".tif",
            ".tiff"
        ]:
            fname_str = fname_str.replace(ext, "")
            return fname_str

    @classmethod
    def _rename_to_ometiff(cls, in_fpath: Path, rename: bool = True) -> Path:
        """renames files to ".ome.tif"""
        if str(in_fpath).endswith(".ome.tif"):
            return in_fpath

        fname = cls._get_fname_no_extension(in_fpath)

        new_fname = fname + ".ome.tif"
        new_fpath = in_fpath.parent.joinpath(new_fname)

        if not in_fpath.is_file():
            logging.info(f"{in_fpath} does not exist")
            if new_fpath.is_file():
                return new_fpath
            else:
                raise ValueError(f"{in_fpath} is not a valid file")

        if rename:
            os.rename(str(in_fpath), str(new_fpath))

        return new_fpath

    def _inject_ometiff_tags(self,
                             out_path: Path,
                             channel_config_dict: dict,
                             bftools_path: Path,
                             array_shape: list,
                             dimension_order: str = "ZYX",
                             acquisition_date: str = None,
                             inject_tags: bool = True,
                             write_ome_xml: bool = True
                             ):
        ome_config_dict = {
            "Name": out_path.name,
            "AcquisitionDate": acquisition_date,
            "PhysicalSizeX": self.config_dict["PixelSize"][2],
            "PhysicalSizeXUnit": self.config_dict["PixelSizeUnit"],
            "PhysicalSizeY": self.config_dict["PixelSize"][1],
            "PhysicalSizeYUnit": self.config_dict["PixelSizeUnit"],
            "PhysicalSizeZ": self.config_dict["PixelSize"][0],
            "PhysicalSizeZUnit": self.config_dict["PixelSizeUnit"],
            "Channels": {
                channel_config_dict["Name"]: {
                    "Name": channel_config_dict["Name"],
                    "SamplesPerPixel": 1,
                    "ExcitationWavelength": channel_config_dict["ExcitationWavelength"],
                    "ExcitationWavelengthUnit": channel_config_dict["PhysicalUnit"],
                    "EmissionWavelength": channel_config_dict["EmissionWavelength"],
                    "EmissionWavelengthUnit": channel_config_dict["PhysicalUnit"],
                    "Fluor": channel_config_dict["Fluor"]
                }
            }
        }

        if inject_tags:
            write_ome_xml = True

        if write_ome_xml:
            ometiff_writer = OMETIFFWriter(fpath=out_path,
                                           dimension_order=dimension_order,
                                           array=None,
                                           metadata=ome_config_dict,
                                           arr_shape=array_shape.copy())
            ometiff_writer.write_xml()

        xml_fpath = out_path.parent.joinpath(out_path.stem + ".xml")

        # injecting ometiff tags
        if inject_tags:
            return subprocess.run([f"{str(bftools_path)}/tiffcomment", "-set", str(xml_fpath), str(out_path)],
                                  capture_output=True)

    @staticmethod
    def _get_stack_shape(stack_fpath: Path):
        inputfile = InputFile(stack_fpath)
        return inputfile.shape

    @staticmethod
    def _get_chuntransformationmatrix(chunk_fpath: Path,
                                      filematrix: FileMatrix):

        chunk_key = "./" + chunk_fpath.name
        return {
            "ChunkTransformationMatrix": [
                [0.0, 0.0, 0.0, filematrix.data_frame.loc[chunk_key]["Xs"]],
                [0.0, 0.0, 0.0, filematrix.data_frame.loc[chunk_key]["Ys"]],
                [0.0, 0.0, 0.0, filematrix.data_frame.loc[chunk_key]["Zs"]],
                [0.0, 0.0, 0.0, 1.0],
            ]
        }

    # def write_chunk(self,
    #                 chunk_id_list_elem: list,
    #                 namestring: str,
    #                 channel_config_dict: dict,
    #                 bftools_path: Path,
    #                 acquisition_date: str):
    #
    #     chunk_idx, chunk_in_fpath = chunk_id_list_elem
    #     array_shape = self._get_stack_shape(chunk_in_fpath)
    #     # rename_files
    #
    #     # renaming original files to .ome.tif
    #     renamed_fpath = self._rename_chunk(chunk_in_fpath=chunk_in_fpath)
    #
    #     chunk_namestring = self.namestring_to_chunk_namestring(namestring, chunk_idx=chunk_idx)
    #     symlink_fpath = self.out_path.joinpath(chunk_namestring + ".ome.tif")
    #
    #     # if symlink already exists skip
    #     if symlink_fpath.is_file() and self.skip_ometiff:
    #         pass
    #     else:
    #         if not self.write_xml_only:
    #             if symlink_fpath.is_file():
    #                 assert str(
    #                     symlink_fpath.is_symlink()), f"{str(symlink_fpath)} seems to be a real file, not a symlink"
    #                 os.remove(str(symlink_fpath))
    #                 logger.info(f"removing {str(symlink_fpath)}")
    #
    #         self._inject_ometiff_tags(out_path=symlink_fpath,
    #                                   channel_config_dict=channel_config_dict,
    #                                   bftools_path=bftools_path,
    #                                   array_shape=list(array_shape),
    #                                   dimension_order="ZYX",
    #                                   acquisition_date=acquisition_date)
    #
    #         # creating symbolic link
    #         if not self.write_xml_only:
    #             os.symlink(str(renamed_fpath), str(symlink_fpath))


if __name__ == "__main__":
    main(
    )
