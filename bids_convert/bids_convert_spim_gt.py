from pathlib import Path
import tifffile
import numpy as np
from skimage import io as skio
import pyometiff

dataset_fpath = Path("/mnt/ssd1/datasets/dandi_gt/masks")
dataset_dict = {subdir: dataset_fpath.joinpath(subdir) for subdir in ["left", "right"]}
segmentation_tiff_dict = {subdir: sorted(list(dataset_dict[subdir].glob("*.png"))) for subdir
                          in ["left", "right"]}


metadata_dict = {
    "PhysicalSizeX": "0.88",
    "PhysicalSizeXUnit": "µm",
    "PhysicalSizeY": "0.88",
    "PhysicalSizeYUnit": "µm",
    "PhysicalSizeZ": "3.3",
    "PhysicalSizeZUnit": "µm",
}

for subdir, segmentation_tiff_list in segmentation_tiff_dict.items():
    for idx, img_fpath in enumerate(segmentation_tiff_list):
        # img = tifffile.imread(str(img_fpath))
        img = skio.imread(str(img_fpath), plugin="pil")
        output_fname = f"sub-I46_ses-SPIM_sample-BrocaAreaS07_{subdir}_{idx:02}_segmentation.ome.tiff"
        output_fpath = img_fpath.parent.joinpath(output_fname)
        writer = pyometiff.OMETIFFWriter(
            fpath=output_fpath,
            dimension_order="ZTCYX",
            array=np.reshape(img, (1, 1, 1, 2048, 2048)),
            metadata=metadata_dict,
            explicit_tiffdata=False
        )
        writer.write()

