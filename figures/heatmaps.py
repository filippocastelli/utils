from pathlib import Path
from argparse import ArgumentParser

import matplotlib.pyplot as plt
import numpy as np
from skimage import io as skio
from tqdm import tqdm


def main():
    parser = ArgumentParser()
    
    parser.add_argument(
        "-d",
        type=str,
        default="/mnt/hdd1/validation_stack/20200320_val_stack/heatmaps/",
        action="store",
        dest="input_path",
        help="Input directory path")
    
    parser.add_argument(
        "-o",
        dest="output_path",
        action="store",
        default=str(Path.cwd().joinpath("out")),
        help="output directory path")
    
    parser.add_argument(
        "-c",
        dest="colormap",
        action="store",
        default="jet",
        help="Colormap name")

    args = parser.parse_args()
    
    out_path = Path(args.output_path)
    out_path.mkdir(exist_ok=True, parents=True)
    in_path = Path(args.input_path)
    cmap = args.colormap
    
    conv_colormap(in_path, out_path, cmap)
    
    
def conv_colormap(in_path, out_path, cmap):
    cm = plt.get_cmap('jet')
    file_list = in_path.glob("*.*")
    file_list = [file for file in file_list if file.is_file()]
    
    for fpath in tqdm(file_list):
        img = skio.imread(fpath, plugin="pil")
        img_name = fpath.name
        out_fpath = out_path.joinpath(img_name)
        colored_img = (cm(img)*255).astype(np.uint8)
        skio.imsave(out_fpath, colored_img)
        
    
if __name__ == "__main__":
    main()
    
    